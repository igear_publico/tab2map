var minValueForColor = new Array();
var maxValueForColor = new Array();
var minIntervalForColor = new Array();
var maxIntervalForColor = new Array();

function sendDataTbl2MapWS() {
	//var fileType = $( "#outputFormat option:selected" ).text();
	var fileType = formatSelected;
	if (fileType == "GML") {
		generateRequest(fileType);
	} else if (fileType == "GeoJSON") {
		generateRequest(fileType);
	}else if (fileType == "KML") {
		generateRequest(fileType);
	} else if (fileType == "SHP") {
		generateRequest(fileType);
	} else if (fileType == "PDF") {
		generateRequest(fileType);
	} else if (fileType == "JPG") {
		generateRequest(fileType);
	} else {
		alert("Formato no soportado");
	}
}

function getFechaActual() {
	var fecha=new Date();
	return fecha.getDate() + "/" + (fecha.getMonth() +1) + "/" + fecha.getFullYear();
}

function generateRequest(outputFormat) {
	$("#resultMapDiv").css("display","block");
	resultMapConfig = false;
	minIntervalForColor = new Array();
	maxIntervalForColor = new Array();
	if ((outputFormat == "JPG") || (outputFormat == "PDF")) {
		if (fileType != "GPX") {
			if (($("#colorNature").val()!='qualitative') && ($("#colorNature").val()!='boolean') && checkNumericValues()) {
				// construir el intervalo para rangos num�ricos
				resultMapConfig = generateMapConfig();
			} else {
				// no num�ricos => cualitativos => s�lo permitimos tantos valores distintos como clases haya
				resultMapConfig = generateMapConfigNonNumeric();
			}
			if (! resultMapConfig) {
				alert("No hay valores para a ser representados. No se muestra mapa");
				return;
			}
		}
	}

	var extent = getExtent();

	if (fileType == "GPX") {
		var typeToLink = "GPX";
		var reqXML = getBeginRequestXML(outputFormat, typeToLink, extent);

		var iniciadaLista = false;

		for (var i = 0; i < grid.getDataLength(); i++) {
			var valorBuscado = normalizaCadenaSpanish(grid.getDataItem(i)["column0"]);
			if (! iniciadaLista) {
				reqXML += "<itemList>";
				iniciadaLista = true;
			}
			reqXML += getItemRequestGPX(valorBuscado, "#FFFF00", grid.getDataItem(i));
		}

		if (iniciadaLista) {
			reqXML += "</itemList>";
		}

		reqXML += getEndRequestXML();
	} else {
		var typeToLink = $("#typeToLink").val();

		var reqXML = getBeginRequestXML(outputFormat, typeToLink, extent);

		var iniciadaLista = false;

		if ((outputFormat == "JPG") || (outputFormat == "PDF")) {
			var valoresOrdenados = new Array();
			var valoresOrdenadosReverse = new Array();
			var selectedFieldColumn = $("select[id='fieldToGenerateMapConfig'] option:selected").index();
			for (var currentColor in rowsPerColor) {
				var i = rowsPerColor[currentColor][0];
				var val = grid.getDataItem(i)["column" + selectedFieldColumn];
				var ultimoValor;
				if (isNaN(val)) {
					ultimoValor = {'id': i, 'value': val, 'color': currentColor};
				} else {
					ultimoValor = {'id': i, 'value': parseFloat(val), 'color': currentColor};
				}
			
				valoresOrdenados.push(ultimoValor);
				valoresOrdenadosReverse.push(ultimoValor);
			}
			valoresOrdenados.sort(function(a,b) {return a.value-b.value;});
			if (typeof minIntervalForColor[currentColor]=='undefined') {
				valoresOrdenadosReverse.sort(function(a,b) {return b.value-a.value;});

				minValueForColor = new Array();
				maxValueForColor = new Array();

				calculateMinMaxValuesForColor(valoresOrdenadosReverse);
			}
			//for (var currentColor in rowsPerColor) {
			for (var currentColor in valoresOrdenados) {
				var	aux = getItemRequestJPG(valoresOrdenados[currentColor].color, extent);

				if (aux != "") {
					if (! iniciadaLista) {
						reqXML += "<itemList>";
						iniciadaLista = true;
					}
					reqXML += aux;
				}
			}
		} else {
			for (var i = 0; i < grid.getDataLength(); i++) {
				if ((grid.getDataItem(i)["column"+columnCount] > 0) || (grid.getDataItem(i)["column"+columnCount] == "S")) {
					var valorBuscado = getValueToFindNormalizado(i);
					var cuantosHay = cuantosHayDelTipo(valorBuscado, typeToLink);
					if (cuantosHay == 1) {
						if (! iniciadaLista) {
							reqXML += "<itemList>";
							iniciadaLista = true;
						}

						// buscar la posicion para este tipo
						var idx = -1;
						for (j in linkedPosibilities[valorBuscado]) {
							if (linkedPosibilities[valorBuscado][j]["type"] == typeToLink) {
								idx = j;
							}
						}

						if (idx != -1) {
							reqXML += getItemRequest(valorBuscado, idx, colorsPerRow[i], grid.getDataItem(i));
						}
					} else if (cuantosHay > 1) {
						if (multipleOptionsSelection[i]) {
							if (! iniciadaLista) {
								reqXML += "<itemList>";
								iniciadaLista = true;
							}
							idx = multipleOptionsSelection[i];
							reqXML += getItemRequest(valorBuscado, idx, colorsPerRow[i], grid.getDataItem(i));
						}
					}
				}
			}
		}

		if (iniciadaLista) {
			reqXML += "</itemList>";
		}

		reqXML += getEndRequestXML();
	}
	if($("#mapCount").is(":checked")){ //lote
		$("#cancel").css("display","block");
		$("#resultMapDiv").css("display","none");
		solicitado=true;
		$.ajax({
			type: "POST",
			url: tab2mapWS_url,
			data: reqXML,
			success: function(data) {
				jobid = $.trim(data);
				
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(xhr.status);
				alert(thrownError);
			} ,
			//	contentType: "application/zip",
			//	dataType: "binary"

			contentType: "text/xml",
			dataType: "text"
		});
		showProgress();
	}
	else{
	if (outputFormat == "JPG") {
		muestraLayerLoading();
		$.ajax({
			type: "POST",
			url: tab2mapWS_url,
			data: reqXML,
			success: function(data) {
				$("#generatedMap").removeClass("oculto");
				$("#generatedLegend").removeClass("oculto");
				$("#descargarBtn").removeClass("oculto");

				aux = data.split("@");
				$("#generatedMap").attr("src", aux[0]);
				$("#generatedLegend").attr("src", aux[1]);
				ocultaLayerLoading();
				/*
				ww=window.open();
				ww.document.write(data);
				ww.document.close();
				*/
			},
			error: function (xhr, ajaxOptions, thrownError) {
				alert(xhr.status);
				alert(thrownError);
			} ,
			//	contentType: "application/zip",
			//	dataType: "binary"

			contentType: "text/xml",
			dataType: "text"
		});
		
	} else if (outputFormat == "PDF") {
		muestraLayerLoading();
		document.printForm.datos.value = makeXMLsafeAcentos(reqXML);
		document.printForm.action = tab2mapWS_url;
		document.printForm.target="resultForm";
		document.printForm.submit();
		ocultaLayerLoading();
		$("#resultForm").removeClass("oculto");
		$("#descargarPDFBtn").removeClass("oculto");
	} else {
		url = tab2mapWS_url;
		if (outputFormat == "SHP") {
			target = "hiddenForm";
		} else {
			target = "hiddenForm";
			//target = "_blank";
		}

		$('body').append('<form action="'+url+'" method="post" target="'+target+'" id="postToIframe"></form>');
		$('#postToIframe').append('<input type=\'hidden\' name=\'datos\' value=\''+reqXML+'\' />');
		$('#postToIframe').submit().remove();
	}
	}
}

function descargar() {
	url = tab2mapWS_url;
	target = "hiddenForm";
	var str = '<?xml version="1.0" encoding="UTF-8"?>';
	str += "<tbl2mapRequest>";
	str += "<outputFormat>ZIP</outputFormat>";
	str += "<url1>" + $("#generatedMap").attr("src") + "</url1>";
	str += "<url2>" + $("#generatedLegend").attr("src") + "</url2>";
	str += "</tbl2mapRequest>";
	$('body').append('<form action="'+url+'" method="post" target="'+target+'" id="postToIframe"></form>');
	$('#postToIframe').append('<input type=\'hidden\' name=\'datos\' value=\''+str+'\' />');
	$('#postToIframe').submit().remove();
}

function descargarPDF() {
	document.printForm.target="_blank";
	document.printForm.submit();
}
function calculateMinMaxValuesForColor(valoresOrdenados) {
	var selectedFieldColumn = $("select[id='fieldToGenerateMapConfig'] option:selected").index();
	for (var idx in valoresOrdenados) {
		var currentColor = valoresOrdenados[idx].color;
		var minValue = "";
		var maxValue = "";
		for (var fila in rowsPerColor[currentColor]) {
			var i = rowsPerColor[currentColor][fila];
			var ultimoValorEsteticaBuscado = normalizaCadenaSpanish(grid.getDataItem(i)["column" + selectedFieldColumn]);
			var valorUltimo = ultimoValorEsteticaBuscado;

			if (! isNaN(ultimoValorEsteticaBuscado)) {
				valorUltimo = parseFloat(ultimoValorEsteticaBuscado);
			}

			if (minValue == "") {
				minValue = valorUltimo;
				minValueForColor[currentColor] = minValue;
			} else {
				if (minValue > valorUltimo) {
					minValue = valorUltimo;
					minValueForColor[currentColor] = minValue;
				}
			}
			if (maxValue == "") {
				maxValue = valorUltimo;
				maxValueForColor[currentColor] = maxValue;
			} else {
				if (maxValue < valorUltimo) {
					maxValue = valorUltimo;
					maxValueForColor[currentColor] = maxValue;
				}
			}
		}
	}
	for (var idx in valoresOrdenados) {
		var currentColor = valoresOrdenados[idx].color;
		minIntervalForColor[currentColor] = minValueForColor[currentColor];
		maxIntervalForColor[currentColor] = maxValueForColor[currentColor];
	}

	for (var idx in valoresOrdenados) {
		var currentColor = valoresOrdenados[idx].color;
		//if (minIntervalForColor[currentColor] != maxIntervalForColor[currentColor]) {
		var pos = parseInt(idx);
		if (pos < (valoresOrdenados.length-1)) {
			var nextColor = valoresOrdenados[pos+1].color;
			var maxValue = maxIntervalForColor[currentColor];
			var minValueNext = minIntervalForColor[nextColor];

			if (($("#colorNature").val()!='qualitative') && ($("#colorNature").val()!='boolean') && (! isNaN(parseFloat(minValueNext))) && (! isNaN(parseFloat(maxValue)))) {
				if ((isInt(minValueNext)) && (isInt(maxValue))) {
					maxIntervalForColor[currentColor] = minValueNext-1;
				} else {
					/*
					var differ = minValueNext - maxValue;
					// 0.1 < diferencia => restar 0.1 al m�ximo del siguiente
					// 0.01 < diferencia < 0.1 => restar 0.01 al m�ximo del siguiente
					// 0.001 < diferencia < 0.01 => restar 0.001 al m�ximo del siguiente
					// lo dejo como est�
					if (differ > 0.1) {
					maxIntervalForColor[currentColor] = RoundDecimal(minValueNext-0.1, 2);
					} else if (differ > 0.01) {
					maxIntervalForColor[currentColor] = RoundDecimal(minValueNext-0.01, 3);
					} else if (differ > 0.001) {
					maxIntervalForColor[currentColor] = RoundDecimal(minValueNext-0.001, 4);
					} else if (differ > 0.0001) {
					maxIntervalForColor[currentColor] = RoundDecimal(minValueNext-0.0001, 5);
					} else {
					// nop
					}*/

					var nextFutureValue = RoundDecimal(minValueNext-0.001, 3);
					if (nextFutureValue > minIntervalForColor[currentColor]) {
						maxIntervalForColor[currentColor] = nextFutureValue;
					}
				}
			}
		}
		//}
	}
}

function isInt(n) {
	return n % 1 === 0;
}

// Funcion para redondear un numero decimal a d decimales
function RoundDecimal(num,d) {
	var m = Math.pow(10,d);
	var num2 = num * m;
	return (Math.round(num2) / m);
}

function getItemRequestJPG(currentColor, extent) {
	var str = "";
	var strVal = "";
	var fieldToGenerate = "";
	var typeToLink = $("#typeToLink").val();
	var selectedFieldColumn = $("select[id='fieldToGenerateMapConfig'] option:selected").index();

	for (var fila in rowsPerColor[currentColor]) {
		var i = rowsPerColor[currentColor][fila];
		if ((grid.getDataItem(i)["column"+columnCount] > 0) || (grid.getDataItem(i)["column"+columnCount] == "S")) {
			var valorBuscado = getValueToFindNormalizado(i);

			var cuantosHay = cuantosHayDelTipo(valorBuscado, typeToLink);
			var idx = -1;
			if (cuantosHay == 1) {
				// buscar la posicion para este tipo
				for (j in linkedPosibilities[valorBuscado]) {
					if ((linkedPosibilities[valorBuscado][j]["type"] == typeToLink)||
						(linkedPosibilities[valorBuscado][j]["type"] == tipos[typeToLink])) {
						idx = j;
					}
				}
			} else if (cuantosHay > 1) {
				if (multipleOptionsSelection[i]) {
					idx = multipleOptionsSelection[i];
				} else {
					return "";
				}
			}

			if (typeToLink == "Toponimo") {
					//strVal = linkedPosibilities[valorBuscado][idx]["xmin"] + " " + linkedPosibilities[valorBuscado][idx]["ymin"];
					var coordX = linkedPosibilities[valorBuscado][idx]["idValue"].split(":")[0];
					var coordY = linkedPosibilities[valorBuscado][idx]["idValue"].split(":")[1];
					var wh = getWidthHeight();
					var coords = coord2px(wh[0], coordX, coordY, extent);
					strVal = coords[0] + " " + coords[1]+":"+coordX + " " + coordY;
			} else if (typeToLink == "Coordenadas") {
				//strVal = linkedPosibilities[valorBuscado][idx]["xmin"] + " " + linkedPosibilities[valorBuscado][idx]["ymin"];
				var coordX = valorBuscado.split(",")[0];
				var coordY = valorBuscado.split(",")[1];
				var wh = getWidthHeight();
				var coords = coord2px(wh[0], coordX, coordY, extent);
				strVal = coords[0] + " " + coords[1]+":"+coordX + " " + coordY;
			}

		/*else if (typeToLink == "Portal") {
				/!*
				if (strVal != "") {
				strVal += ';' + (linkedPosibilities[valorBuscado][idx]["xmax"]-250) + " " + (linkedPosibilities[valorBuscado][idx]["ymax"]-250);
				} else {
				strVal = (linkedPosibilities[valorBuscado][idx]["xmax"]-250) + " " + (linkedPosibilities[valorBuscado][idx]["ymax"]-250);
				}*!/
				if (strVal != "") {
					strVal += ";" + (linkedPosibilities[valorBuscado][idx]["xmax"]) + " " + (linkedPosibilities[valorBuscado][idx]["ymax"]);
				} else {
					strVal = (linkedPosibilities[valorBuscado][idx]["xmax"]) + " " + (linkedPosibilities[valorBuscado][idx]["ymax"]);
				}
			} */
			else {
				if (fieldToGenerate == "") {
					fieldToGenerate = "OBJECTID";
				}

				if (strVal != "") {
					strVal += ',' + linkedPosibilities[valorBuscado][idx]["idValue"];
				} else {
					strVal = linkedPosibilities[valorBuscado][idx]["idValue"];
				}
			}
		}
	}
	var minValue = minIntervalForColor[currentColor];
	var maxValue = maxIntervalForColor[currentColor];

	str += "<it>";
	str += "<_fld>" + (prefixIdFieldList[typeToLink] ? prefixIdFieldList[typeToLink] : "") + fieldToGenerate + "</_fld>";
	str += "<_val>" + strVal + "</_val>";

	if (minValue != maxValue) {
		str += "<_tit>" + minValue + " - " + maxValue + "</_tit>";
	} else {
		str += "<_tit>" + minValue + "</_tit>";
	}
	str += "<_rgb>" + currentColor + "</_rgb>";
	str += "</it>";

	return str;
}

function getItemRequest(value, idxSelected, color, dataItem) {
	var typeToLink = $("#typeToLink").val();
	var str = "<it>";
	var columns = grid.getColumns();
	str += "<_fld>" + (prefixIdFieldList[typeToLink] ? prefixIdFieldList[typeToLink] : "") + "OBJECTID" + "</_fld>";

	if ((typeToLink.value == "Coordenadas") || (typeToLink.value == "Toponimo")) {
		str += "<_val>" + linkedPosibilities[value][idxSelected]["xmin"] + " " + linkedPosibilities[value][idxSelected]["ymin"] + "</_val>";


	} else if (typeToLink.value == "Portal") {
		//str += "<_val>" + (linkedPosibilities[value][idxSelected]["xmax"]-250) + " " + (linkedPosibilities[value][idxSelected]["ymin"]) + "</_val>";
		str += "<_val>" + (linkedPosibilities[value][idxSelected]["xmax"]) + " " + (linkedPosibilities[value][idxSelected]["ymax"]) + "</_val>";
	} else {
		str += "<_val>" + linkedPosibilities[value][idxSelected]["idValue"] + "</_val>";
	}

	str += "<_rgb>" + color + "</_rgb>";

	for (var kk = 0; kk < columnCount; kk++) {
		var columnName = columns[kk]["name"].trim();
		if (columnName != '') {
			columnName = columnName.replace(/ /g, "_")
		} else {
			columnName = 'field' + columnCount;
		}
		str += "<" + columnName	+ ">";
		str += dataItem["column" + kk];
		str += "</" + columnName + ">";
	}

	str += "</it>";
	return str;
}

function getItemRequestGPX(value, color, dataItem) {
	var str = "<it>";
	var columns = grid.getColumns();
	str += "<_val>" + value + "</_val>";
	str += "<_rgb>" + color + "</_rgb>";

	for (var kk = 1; kk < columnCount; kk++) {
		var columnName = columns[kk]["name"].trim();
		if (columnName != '') {
			columnName = columnName.replace(/ /g, "_")
		} else {
			columnName = 'field' + columnCount;
		}
		str += "<" + columnName	+ ">";
		str += dataItem["column" + kk];
		str += "</" + columnName + ">";
	}

	str += "</it>";
	return str;
}

function getBeginRequestXML(outputFormat, selectedColumnType, extent) {
	var str = '<?xml version="1.0" encoding="UTF-8"?>';
	str += "<tbl2mapRequest>";
	str += "<outputFormat>" + outputFormat + "</outputFormat>";
	str += "<type>" + selectedColumnType + "</type>";
	//str += "<mapa>" + $("#mapa").val() + "</mapa>";
	var layers="";
	var leyendas="";
	$(".listaCapas input:checked").each(function() {
		if (layers.length>0) {
			layers +="#";
			leyendas+="#";
		}
		var layerIdx = $(this)[0].name.replace("capa_","");
		layers += eval("CAPA_WMS_"+layerIdx)+"LAYERS="+eval("CAPA_LAYERS_"+layerIdx);
		leyendas += eval("CAPA_LABELS_"+layerIdx);
	});
	if (layers.length>0) {
	str += "<capas>" + layers + "</capas>";
	str += "<leyendas>" + leyendas + "</leyendas>";
	}
	if($("#mapCount").is(":checked")){
		str += "<lote>" + $("#ambito").val() + "</lote>";
		str += "<tamano>" + $('input[name=tamano]:checked').val() + "</tamano>";
	}
	str +="<columna>" +$("select[id='fieldToGenerateMapConfig'] option:selected").text()+"</columna>";
	str += "<layer>" + layerFieldList[selectedColumnType] + "</layer>";
	// idealmente, no deber�a pasarse filtro, o pasarse vac�o
	// este filtro, como ahora contiene comilla simple, se hace mal el escape del env�o del formulario
	// pero es innecesario (aparentemente) para formatos vectoriales de salida
	if ((outputFormat == "JPG") || (outputFormat == "PDF")) {
		str += "<filter>" + filterFieldList[selectedColumnType] + "</filter>";
	} else {
		str += "<filter>1=1</filter>";
	}

	//var radioButOrient = $('input[name=orientacion]:checked').val();
	var wh = getWidthHeight();
	/*var theWidth = 500;
	var theHeight = 800;
	if (radioButOrient == "Vertical") {
	theWidth = 500;
	theHeight = 800;
	} else if (radioButOrient == "Horizontal") {
	theWidth = 800;
	theHeight = 500;
	}*/


	str += "<extent>" + extent + "</extent>";
	str += "<w>" + wh[0] + "</w>";
	str += "<h>" + wh[1] + "</h>";
	/*str += "<w>" + theWidth + "</w>";
	str += "<h>" + theHeight + "</h>";*/
	if (outputFormat == "PDF") {
			str += "<orientacion>" + $('input[name=orientacion]:checked').val() + "</orientacion>";
		str += "<titulo>" + $("#tituloMapa").val() + "</titulo>";
		str += "<fecha>" + getFechaActual() + "</fecha>";
	}
	return str;
}

function getEndRequestXML() {
	var str = "</tbl2mapRequest>";
	return str;
}

function getExtentMinimumForAllData(mwidth, mheight, gap) {
	var minx = 9999999;
	var miny = 9999999;
	var maxx = -9999999;
	var maxy = -9999999;
	var typeToLink = $("#typeToLink").val();
	for (var i = 0; i < grid.getDataLength(); i++) {
		if ((grid.getDataItem(i)["column"+columnCount] > 0) || (grid.getDataItem(i)["column"+columnCount] == "S")) {
			var valorBuscado = getValueToFindNormalizado(i);
			var cuantosHay = cuantosHayDelTipo(valorBuscado, typeToLink);
			if (cuantosHay == 1) {
				// buscar la posicion para este tipo
				var idx = -1;
				for (j in linkedPosibilities[valorBuscado]) {
					if (linkedPosibilities[valorBuscado][j]["type"] == typeToLink) {
						idx = j;
					}
				}

				if (idx != -1) {
					minx = Math.min(minx, linkedPosibilities[valorBuscado][idx]["xmin"]);
					miny = Math.min(miny, linkedPosibilities[valorBuscado][idx]["ymin"]);
					maxx = Math.max(maxx, linkedPosibilities[valorBuscado][idx]["xmax"]);
					maxy = Math.max(maxy, linkedPosibilities[valorBuscado][idx]["ymax"]);
				}
			} else if (cuantosHay > 1) {
				if (multipleOptionsSelection[i]) {
					idx = multipleOptionsSelection[i];
					if (idx != -1) {
						minx = Math.min(minx, linkedPosibilities[valorBuscado][idx]["xmin"]);
						miny = Math.min(miny, linkedPosibilities[valorBuscado][idx]["ymin"]);
						maxx = Math.max(maxx, linkedPosibilities[valorBuscado][idx]["xmax"]);
						maxy = Math.max(maxy, linkedPosibilities[valorBuscado][idx]["ymax"]);
					}
				}
			}
		}
	}
	var bbox = checkDeformation(minx-gap, miny-gap, maxx+gap, maxy+gap, mwidth, mheight);
	return bbox.minx + ":" + bbox.miny + ":" + bbox.maxx + ":" + bbox.maxy;
}

function coord2px(anchuraPx, xMetros, yMetros, bbox) {
	var partes = bbox.split(":");
	var anchuraMetros = parseFloat(partes[2]) - parseFloat(partes[0]); //xmax - xmin
	var metrosXpx = anchuraMetros / anchuraPx;
	var offsetX = xMetros - parseFloat(partes[0]);
	var offsetY = yMetros - parseFloat(partes[1]);
	var xPx = Math.round(offsetX / metrosXpx);
	var yPx = Math.round(offsetY/ metrosXpx);
	//Comprobacion
	return [xPx, yPx];

}

function getExtent() {
	var wh = getWidthHeight();
	var extent = "";
//	var extentType = $("#extent").val();
	var extentType= $('input[name=extent]:checked').val();
	if (extentType == "Global") {
		extent = "569301:4413376:812739:4755088";
	} else if (extentType == "Provincia") {
		var txt = $("#provincias").val().toUpperCase();
		var codProv = cProvList[txt];
		extent = xminProv[codProv] + ":" + yminProv[codProv] + ":" + xmaxProv[codProv] + ":" + ymaxProv[codProv];
	} else if (extentType == "Comarca") {
		var txt = normalizaCadena($("#textoComarca").val()).toUpperCase();
		var codCom = cComList[txt];
		extent = xminCom[codCom] + ":" + yminCom[codCom] + ":" + xmaxCom[codCom] + ":" + ymaxCom[codCom];
	} else if (extentType == "Municipio") {
		var txt = normalizaCadena($("#textoMuni").val()).toUpperCase();
		var codMuni = cMuniList[txt];
		extent = xminMuni[codMuni] + ":" + yminMuni[codMuni] + ":" + xmaxMuni[codMuni] + ":" + ymaxMuni[codMuni];
	} else if (extentType == "Localidad") {
		var txt = $("#textoLocalidad").val();
		var codLocali = cLocaliHint[txt];
		extent = xminLocali[codLocali] + ":" + yminLocali[codLocali] + ":" + xmaxLocali[codLocali] + ":" + ymaxLocali[codLocali];
	} else if (extentType == "scale") {
		var xMidPoint = $("#scaleXExtent").val();
		var yMidPoint = $("#scaleYExtent").val();
		var newScale = $("#scaleExtent").val();
		extent = getExtentForScale(xMidPoint, yMidPoint, newScale, wh[0], wh[1]);
	} else if (extentType == "custom") {
		var _minx = parseFloat($("#userExtentW").val());
		var _miny = parseFloat($("#userExtentS").val());
		var _maxx = parseFloat($("#userExtentE").val());
		var _maxy = parseFloat($("#userExtentN").val());
		var bbox = checkDeformation(_minx, _miny, _maxx, _maxy, wh[0], wh[1]);
		extent = bbox.minx + ":" + bbox.miny + ":" + bbox.maxx + ":" + bbox.maxy;
	} else if (extentType == "minimum") {
		var minimumGap = parseFloat($("#minimumGap").val());
		if (minimumGap != "") {
			if (isNaN(minimumGap)) {
				minimumGap = 0;
			}
		} else {
			minimumGap = 0;
		}
		extent = getExtentMinimumForAllData(wh[0], wh[1], minimumGap);
	}

	var partesbbox = extent.split(":");
	var bbox = checkDeformation(parseFloat(partesbbox[0]), parseFloat(partesbbox[1]), parseFloat(partesbbox[2]), parseFloat(partesbbox[3]), wh[0], wh[1]);
	extent = bbox.minx + ":" + bbox.miny + ":" + bbox.maxx + ":" + bbox.maxy;
	return extent;
}

function getWidthHeight() {
	var radioButOrient = $('input[name=orientacion]:checked').val();
	var theWidth = 500;
	var theHeight = 800;
	if (radioButOrient == "Vertical") {
		theWidth = 500;
		theHeight = 800;
	} else if (radioButOrient == "Horizontal") {
		theWidth = 800;
		theHeight = 500;
	}
	return [theWidth, theHeight];
}
