﻿function getParameter(paramName) {
	var searchString = window.location.search.substring(1),
			i, val, params = searchString.split("&");

	for (i=0; i<params.length; i++) {
		val = params[i].split("=");
		if (val[0] == paramName) {
			return unescape(val[1]);
		}
	}
	return null;
}

//Esto es para que funcione el hover en explorer 6
$(document).ready(function() {
	image_change();

	if (getParameter("q")) {
		//$("#busqBanner").attr('value', getParameter("q"));
	}
	//precacheImagesMenu();

	$("#flechaNext").click(function () { next(); });
	$("#flechaBack").click(function () { back(); });
});

//replace the five problem characters for the server's XML parser
function makeXMLsafeAcentos(oldString) {
	var aux = oldString;
	aux = aux.replace(/&nbsp;/g, " ");

	aux = aux.replace(/&/g, "&amp;");

	aux = aux.replace(/<link href=".*page.css" rel="StyleSheet" type="text\/css">/g, "");

	aux = aux.replace(/&amp;ordm;/g, "&#186;");

	if (aux.indexOf("src='") != -1) {
		if (aux.indexOf("src='http://") == -1) {
			aux = aux.replace(/src='/g, "src='" + urlApp);
		}
	}

	if (aux.indexOf("src=\"") != -1) {
		if (aux.indexOf("src=\"http://") == -1) {
			aux = aux.replace(/src="/g, "src=\"" + urlApp);
		}
	}

	aux = aux.replace(/&amp;ordm;/g, "&#186;");
	aux = aux.replace(/&amp;ordf;/g, "&#170;");
	aux = aux.replace(/&amp;quot;/g, "&#34;");
	aux = aux.replace(/&amp;aacute;/g, "&#225;");
	aux = aux.replace(/&amp;eacute;/g, "&#233;");
	aux = aux.replace(/&amp;iacute;/g, "&#237;");
	aux = aux.replace(/&amp;oacute;/g, "&#243;");
	aux = aux.replace(/&amp;uacute;/g, "&#250;");
	aux = aux.replace(/&amp;ntilde;/g, "&#241;");
	aux = aux.replace(/&amp;uuml;/g, "&#252;");
	aux = aux.replace(/&amp;Aacute;/g, "&#193;");
	aux = aux.replace(/&amp;Eacute;/g, "&#201;");
	aux = aux.replace(/&amp;Iacute;/g, "&#205;");
	aux = aux.replace(/&amp;Oacute;/g, "&#211;");
	aux = aux.replace(/&amp;Uacute;/g, "&#218;");
	aux = aux.replace(/&amp;Ntilde;/g, "&#209;");
	aux = aux.replace(/&amp;Uuml;/g, "&#220;");


	aux = aux.replace(/á/g, "&#225;");
	aux = aux.replace(/é/g, "&#233;");
	aux = aux.replace(/í/g, "&#237;");
	aux = aux.replace(/ó/g, "&#243;");
	aux = aux.replace(/ú/g, "&#250;");
	aux = aux.replace(/ñ/g, "&#241;");
	aux = aux.replace(/ü/g, "&#252;");
	aux = aux.replace(/Á/g, "&#193;");
	aux = aux.replace(/É/g, "&#201;");
	aux = aux.replace(/Í/g, "&#205;");
	aux = aux.replace(/Ó/g, "&#211;");
	aux = aux.replace(/Ú/g, "&#218;");
	aux = aux.replace(/Ñ/g, "&#209;");
	aux = aux.replace(/Ü/g, "&#220;");
	aux = aux.replace(/º/g, "&#186;");
	return aux;
}

function boundingBox(x1, y1, x2, y2) {
	this.minx = x1;
	this.maxx = x2;
	this.miny = y1;
	this.maxy = y2;
}

function checkDeformation(_minx, _miny, _maxx, _maxy, mwidth, mheight) {
	var newBBOX = new boundingBox(_minx, _miny, _maxx, _maxy);

	var ratioTamano = mwidth / mheight;
	var diffX = _maxx - _minx;
	var diffY = _maxy - _miny;
	var ratioCoord = diffX / diffY;
//alert("ratioT" + ratioTamano + "," + ratioCoord)
	if (ratioCoord > ratioTamano) {
		// estirar la Y
	//	alert("estirar la Y")

		var newDiffY = diffX / ratioTamano;

		var gapY = newDiffY - diffY;

		if (gapY > 0) {
			newBBOX.miny = _miny - (gapY/2);
			newBBOX.maxy = _maxy + (gapY/2);
		} else {
			newBBOX.maxy = _miny - (gapY/2);
			newBBOX.miny = _maxy + (gapY/2);
		}
	} else if (ratioCoord < ratioTamano){
		// estirar la X
		//alert("estirar la X")

		var newDiffX = ratioTamano * diffY;

		var gapX = newDiffX - diffX;
		if (gapX > 0) {
			newBBOX.minx = _minx - (gapX/2);
			newBBOX.maxx = _maxx + (gapX/2);
		} else {
			newBBOX.maxx = _minx - (gapX/2);
			newBBOX.minx = _maxx + (gapX/2);
		}
	}
	return newBBOX;
}

var tempFactor = 96;
var inchPerMeter = 39.4;
function getExtentForScale(xMidPoint, yMidPoint, newScale, mwidth, mheight) {
	var distanceInches = (newScale) / tempFactor;
	var distanceOnePixel = distanceInches / inchPerMeter;
	xDistance = distanceOnePixel * mwidth;
	yDistance = distanceOnePixel * mheight;

	newLeft = xMidPoint - (xDistance/2);
	newRight = parseFloat(xMidPoint) + (xDistance/2);
	newTop = parseFloat(yMidPoint) + (yDistance/2);
	newBottom = yMidPoint - (yDistance/2);
	return newLeft + ":" + newBottom + ":" + newRight + ":" + newTop;
}
