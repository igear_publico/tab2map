var grid;
var columnCount = -1;
var fileType = "";
var MAX_NUM_ROWS = 3001;
var alreadyNotifiedError = false;

var originalFirstRow = null;

var maxWidthEmparejamiento = 140;

var solicitadoEmparejamiento = "";

function showLoadDialog(type) {
    var alreadyNotifiedError = false;

    $('#firstRowAsHeader').prop('checked', false);

    fileType = type;
    var url = fileForwarder_url;
    if (fileType == "XLS") {
        url = XLS2CSV_url;
    }

    var dialog= $('<div></div>')
        .html('<div class="ui-widget"><p>Seleccione un fichero de su disco local:</p><br/><form method="post" id="formId" enctype="multipart/form-data" action="' + url + '"><input type="file" name="file"/></form><p>En funci&oacute;n del tama&ntilde;o del fichero esta operaci&oacute;n puede ser muy lenta. En ese caso, pruebe con un fichero de menor tama&ntilde;o</div>')
        .dialog({
            autoOpen: true,
            buttons: {
                Aceptar: function() {
                    muestraLayerLoading();
                    pararEmparejamiento = true;
                    $("#cuantosLlevoTxt").html("");
                    if (grid) {
                        grid.invalidate();
                        grid.render();
                    }

                    $('#formId').ajaxForm({
                        contentType: "charset=utf-8",
                        beforeSubmit: function(a,f,o) {
                        },
                        success: function(data) {
                            dataLoaded(data);
                        },
                        error: function(data) {
                            ocultaLayerLoading();
                            alert("Error cargando el fichero");
                            alert(data.responseText);
                        }
                    });

                    $('#formId').submit();
                    next();

                    $( this ).dialog( "close" );
                }
            },
            close: function(ev, ui) {
                $( this ).dialog( "destroy" );
            },
            width: 600,
            title: 'Carga de tabla',
            modal: true
        });
}

function muestraLayerLoading() {
    //$("#loading").removeClass("oculto");
    $("#loading").css("visibility", "visible");
}

function ocultaLayerLoading() {
    //$("#loading").addClass("oculto");
    $("#loading").css("visibility", "hidden");
}

var patternBlank = new RegExp(/^[ 	]*$/);

function esLineaVacia(theLine) {
    return patternBlank.test(theLine);
}

function formatter(row, cell, value, columnDef, dataContext) {
    if (typeof value=='undefined') {
        return "";
    } else if (value == "1") {
        return "<img src='images/singleLink.png' alt='OK' title='Emparejamiento único'>";
    } else if (value == "0") {
        return "<img src='images/noLink.png' alt='X' title='Sin emparejamiento'>";
    } else if (value == "S") {
        return "<img id='multipleLink" + row + "' alt='OK (R)' title='Emparejamiento múltiple con selección anterior' onclick='javascript:solveMultiple(" + row + ")' style='cursor:pointer;' src='images/multipleLinkSelected.png'></img>";
    } else if (parseInt(value) >= 0) {
        return "<img id='multipleLink" + row + "' alt='R' title='Emparejamiento múltiple' onclick='javascript:solveMultiple(" + row + ")' style='cursor:pointer;' src='images/multipleLink.png'></img>";
    } else if (parseInt(value) < 0) {
        return "<img alt='-' title='Valor descartado (repetido)' src='images/repeatedLink.png'></img>";
    } else {
        return value;
    }
}

// Obtener formato de datos del usuario y procesarlo
function dataLoaded(data) {
    colorsPerRow = new Array();
    solicitadoEmparejamiento = "";

    //fileType = $( "#formatType option:selected" ).text();

    if (fileType == "CSV") {
        processCSVloaded(data);
    } else if (fileType == "GPX") {
        processGPXloaded(data);
    } else if (fileType == "JSON") {
        processJSONloaded(data);
    } else if (fileType == "XML") {
        processXMLloaded(data);
    } else if (fileType == "XLS") {
        processCSVloaded(data);
        //processXLSloaded(data);
    } else {
        alert("No soportado todavía");
    }

    if (fileType != "GPX") {
        //emparejarDatos();
    }
    ocultaLayerLoading();
    //$('#optionsZone').removeClass("oculto");
    showFlechaNext();
}
function processGPXloaded(data) {
    var dataAux = deleteExtraChars(data);

    //var mapOL = new OpenLayers.Map();
    var gmlLayer = new OpenLayers.Layer.Vector("MyGMLLayer");
    var gmlOptions = {
        //featureType: "REDNAT_APPE",
        //geometryName: "geometryProperty",
        //collectionName:"FeatureCollection",
        //featureNS: "http://ogr.maptools.org/"
    };
    var gmlOptionsIn = OpenLayers.Util.extend(
        OpenLayers.Util.extend({}, gmlOptions)
    );

    var format = new OpenLayers.Format.GPX(gmlOptionsIn);

    var features = format.read(dataAux);

    if (features.length > 0) {
        // Averiguar las columnas que tiene y cargar los datos
        var idxRow = 0;
        var data = [];
        var columns =[];
        var numRows = -1;

        // la primera fila marca el n�mero de columnas que habr�

        var col = 1;

        // Construir la lista de columnas que hay
        columns[-1] = {id: "id", name: "id", field: "id" };
        // la primera columna ser� la geometr�a
        columns[0] = {id: "column0", name: "Geometría", field: "column0" };
        for (var field in features[0]["data"]) {
            columns[col] = {id: "column"+col, name: field, field: "column"+col, editor: Slick.Editors.Text };
            col++;
        }
        columnCount = col-1;

        // Crear la columna de emparejamiento
        //columns["column"+columnCount] = {id: "column"+columnCount, name: "Emparejamiento", field: "column"+fields.length, formatter: formatter, maxWidth:maxWidthEmparejamiento};

        // Rellenar las filas de la tabla con los datos que ha proporcionado el usuario
        for (var fila in features) {
            data[idxRow] = {};
            data[idxRow]["id"] = idxRow;
            var col = 0;

            data[idxRow]["column"+col] = features[fila]["geometry"].toString();
            col++;

            for (var field in features[fila]["data"]) {
                data[idxRow]["column"+col] = adjustDecimalComma(features[fila]["data"][field]);
                col++;
            }
            //data[idxRow]["column" + columnCount] = "";
            idxRow++;
        }

        createTableSlickGrid(data, columns);

        // actualizar la lista de campos por el que enlazar
        updateFieldsToLink();
        $('#fieldToLinkDiv').removeClass("oculto");
        $('#outputFormatDiv').removeClass("oculto");
        if ($( "#outputFormat option:selected" ).text() == "JPG-geo") {
            $('#fieldToGenerateMapConfigDiv').removeClass("oculto");
        }
        $('#generateBtn').removeClass("oculto");
    } else {
        alert("No se encontraron geometrías");
    }

}
function deleteExtraChars(data) {
    var linesFile = data.split("\n");
    var dataAux = "";

    for (var jj = 0; jj < linesFile.length; jj++) {
        var theLine = linesFile[jj].replace(/\r/g, "");
        if (! esLineaVacia(theLine)) {
            // pongo un espacio en blanco por si es XML y hay etiqueta abierta en l�nea anterior
            dataAux += " " + theLine;
        }
    }
    return dataAux;
}

function processXMLloaded(data) {
    // eliminar los espacios en blanco al principio
    var dataAux = deleteExtraChars(data).replace(/^[ ]*/g, "");

    var obj = jQuery.parseXML(dataAux);
    var items = obj["children"][0]["children"];

    // Averiguar las columnas que tiene y cargar los datos
    var idxRow = 0;
    var data = [];
    var columns =[];
    var numRows = -1;

    // la primera fila marca el n�mero de columnas que habr�
    columnCount = items[0]["children"].length;

    // Construir la lista de columnas que hay
    columns[-1] = {id: "id", name: "id", field: "id" };
    for (var col=0; col < columnCount; col++) {
        var field = items[0]["children"][col].nodeName;
        columns[col] = {id: "column"+col, name: field, field: "column"+col, editor: Slick.Editors.Text };
    }

    // Crear la columna de emparejamiento
    columns[columnCount] = {id: "columnEmparejamiento", name: "Emparejamiento", field: "column"+columnCount, formatter: formatter, maxWidth:maxWidthEmparejamiento};

    var salir = false;
    for (var fila=0; fila < items.length; fila++) {
        if (! salir) {
            // Rellenar las filas de la tabla con los datos que ha proporcionado el usuario
            data[idxRow] = {};
            data[idxRow]["id"] = idxRow;
            for (var col=0; col < columnCount; col++) {
                data[idxRow]["column"+col] = adjustDecimalComma(items[fila]["children"][col].textContent);
            }
            data[idxRow]["column" + columnCount] = "";
            idxRow++;

            if (idxRow >= MAX_NUM_ROWS) {
                salir = true;
            }
        }
    }

    createTableSlickGrid(data, columns);

    // actualizar la lista de campos por el que enlazar
    updateFieldsToLink();
    $('#fieldToLinkDiv').removeClass("oculto");
    $('#outputFormatDiv').removeClass("oculto");
    if ($( "#outputFormat option:selected" ).text() == "JPG-geo") {
        $('#fieldToGenerateMapConfigDiv').removeClass("oculto");
    }
    $('#generateBtn').removeClass("oculto");
}

function processJSONloaded(data) {
    var obj = jQuery.parseJSON(data);

    // Averiguar las columnas que tiene y cargar los datos
    var idxRow = 0;
    var data = [];

    columnCount = 0;
    var columns =[];
    var numRows = -1;
    columns[-1] = {id: "id", name: "id", field: "id" };
    for (field in obj) {
        // Construir la lista de columnas que hay
        columns[columnCount] = {id: "column"+columnCount, name: field, field: "column"+columnCount, editor: Slick.Editors.Text };
        columnCount++;

        if (numRows == -1) {
            // la primera columna decide el n�mero de filas que habr�
            numRows = obj[field].length;
        }
    }

    columns[columnCount] = {id: "columnEmparejamiento", name: "Emparejamiento", field: "column"+columnCount, formatter: formatter, maxWidth:maxWidthEmparejamiento};

    // rellenar la columna emparejamiento
    for (var idxRow = 0; idxRow < Math.min(MAX_NUM_ROWS, numRows); idxRow++) {
        data[idxRow] = {};
        data[idxRow]["id"] = idxRow;
        data[idxRow]["column"+columnCount] = "";
    }

    var kk = 0;
    var salir = false;
    for (field in obj) {
        if (! salir) {
            // avisar si no coincide el numero de columnas
            if (columnCount != obj[field].length) {
                if (! alreadyNotifiedError) {
                    alert("La columna denominada " + field + "tiene " + obj[field].length + " filas y no coincide con la de la primera columna que tiene " + numRows + ". Es posible que haya seleccionado como tipo de archivo uno incorrecto (Sucesivos errores serán omitidos)");
                    alreadyNotifiedError = true;
                }
                continue;
            } else {
                // Rellenar las filas de la tabla con los datos que ha proporcionado el usuario
                var idxRow = 0;
                for (value in obj[field]) {
                    if (! salir) {
                        data[idxRow]["column"+kk] = adjustDecimalComma(obj[field][value]);
                        idxRow++;

                        if (idxRow >= MAX_NUM_ROWS) {
                            salir = true;
                        }
                    }
                }
                kk++;
            }
        }
    }

    createTableSlickGrid(data, columns);

    // actualizar la lista de campos por el que enlazar
    updateFieldsToLink();

    $('#fieldToLinkDiv').removeClass("oculto");
    $('#outputFormatDiv').removeClass("oculto");
    if ($( "#outputFormat option:selected" ).text() == "JPG-geo") {
        $('#fieldToGenerateMapConfigDiv').removeClass("oculto");
    }
    $('#generateBtn').removeClass("oculto");
}

function processCSVloaded(data) {
    linesFile = data.split("\n");

    // Averiguar las columnas que tiene y cargar los datos
    var idxRow = 0;
    var data = [];
    var columns = [];
    columnCount = -1;

    var salir = false;
    for (var jj = 0; jj < linesFile.length; jj++) {
        if (! salir) {
            var theLine = linesFile[jj].replace(/\r/g, "");
            if (! esLineaVacia(theLine)) {
                var fields = theLine.split(";");
                if (columnCount == -1) {
                    columnCount = fields.length;
                    // Construir la lista de columnas que hay
                    var columns =[];
                    columns[-1] = {id: "id", name: "id", field: "id" };
                    for (var kk = 0; kk < fields.length; kk++) {
                        columns[kk] = {id: "column"+kk, name: "Columna "+(kk+1), field: "column"+kk, editor: Slick.Editors.Text };
                    }
                    columns[fields.length] = {id: "column"+fields.length, name: "Emparejamiento", field: "column"+fields.length, formatter: formatter, maxWidth:maxWidthEmparejamiento};
                } else {
                    if (columnCount != fields.length) {
                        if (! alreadyNotifiedError) {
                            alert("La fila " + (jj+1) + "tiene " + fields.length + " columnas y no coincide con la de la primera fila que tiene " + columnCount + ". Es posible que haya seleccionado como tipo de archivo uno incorrecto (Sucesivos errores serán omitidos)");
                            alreadyNotifiedError = true;
                        }
                        continue;
                    }
                }

                // Rellenar las filas de la tabla con los datos que ha proporcionado el usuario
                data[idxRow] = {};
                data[idxRow]["id"] = idxRow;
                for (var kk = 0; kk < fields.length; kk++) {
                    data[idxRow]["column"+kk] = adjustDecimalComma(fields[kk]);
                }
                data[idxRow]["column"+fields.length] = "";
                idxRow++;

                if (idxRow >= MAX_NUM_ROWS) {
                    salir = true;
                }
            }
        }
    }

    createTableSlickGrid(data, columns);

    // actualizar la lista de campos por el que enlazar
    updateFieldsToLink();

    $('#firstRowAsHeaderDiv').removeClass("oculto");
    $('#fieldToLinkDiv').removeClass("oculto");
    $('#outputFormatDiv').removeClass("oculto");
    if ($( "#outputFormat option:selected" ).text() == "JPG-geo") {
        $('#fieldToGenerateMapConfigDiv').removeClass("oculto");
    }
    $('#generateBtn').removeClass("oculto");
}

function createTableSlickGrid(data, columns) {
    originalFirstRow = data[0];

    var dataView = new Slick.Data.DataView();

    var options = {
        editable: true,
        enableCellNavigation: true,
        forceFitColumns:true,
        //multiColumnSort: true,
        explicitInitialization: true,
        enableColumnReorder: false
    };

    // Mostrar la tabla para que compruebe si es correcto
    grid = new Slick.Grid("#resultados_tabla_cargada", dataView, columns, options);
	/*
	 grid.onSort.subscribe(function (e, args) {
	 var comparer = function(a, b) {
	 if (args.sortAsc) {
	 return a[args.sortCol.field] > b[args.sortCol.field];
	 } else {
	 return a[args.sortCol.field] < b[args.sortCol.field];
	 }
	 };

	 view = grid.getData();

	 view.sort(comparer);
	 grid.invalidate();
	 grid.render();
	 });
	 */

    grid.onCellChange.subscribe(function(e, args) {
        if (solicitadoEmparejamiento != "") {
            var typeToLink = $("#typeToLink").val();
            if (args.row != "") {
                // si ya la tengo, no vuelvo a consultar
                if (! linkedPosibilities[args.row]) {
                    llevoUnoMenos();
                    updateOnlyOneColumnEmparejamientosFixedValue(args.row, "", false);
                    muestraLayerLoading();
                    emparejaUnDato(args.row, numRows, typeToLink, false, true);
                } else {
                    updateRepeatedValues(typeToLink);
                }
            } else {
                updateOnlyOneColumnEmparejamientosFixedValue(args.row, "", false);
            }
        }
    });

    dataView.onRowCountChanged.subscribe(function (e, args) {
        grid.updateRowCount();
        grid.render();
    });

    dataView.onRowsChanged.subscribe(function (e, args) {
        grid.invalidateRows(args.rows);
        grid.render();
    });

    dataView.beginUpdate();
    dataView.setItems(data, "id");
    dataView.setFilter(filter);
    dataView.endUpdate();

    var optionsHF = {
        buttonImage: "images/down.png",
        filterImage: "images/filter.png",
        sortAscImage: "/lib/SlickGrid-2.0.2/images/sort-asc.gif",
        sortDescImage: "/lib/SlickGrid-2.0.2/images/sort-desc.gif"
    };

    var filterPlugin = new Ext.Plugins.HeaderFilter(optionsHF);

    // This event is fired when a filter is selected
    filterPlugin.onFilterApplied.subscribe(function () {
        dataView.refresh();
        grid.resetActiveCell();
        updateFilterStatus();
    });

    // Event fired when a menu option is selected
    filterPlugin.onCommand.subscribe(function (e, args) {
        dataView.fastSort(args.column.field, args.command === "sort-asc");
        // TODO: Marcar en la cabecera como que est� ordenado por esa columna
    });

    grid.registerPlugin(filterPlugin);
    grid.init();

    // Filter the data (using userscore's _.contains)
    function filter(item) {
        var columns = grid.getColumns();

        var value = true;

        for (var i = 0; i < columns.length; i++) {
            var col = columns[i];
            var filterValues = col.filterValues;

            if (filterValues && filterValues.length > 0) {
                value = value & _.contains(filterValues, item[col.field]);
            }
        }
        return value;
    }

}

function setFirstRowAsHeader() {
    //var data = grid.getData()["0"];
    var data = originalFirstRow;
    for (var i = 0; i < columnCount; i++) {
        grid.updateColumnHeader("column"+i, data["column"+i]);
    }
    //grid.getData().splice(0,1);
    grid.getData().deleteItem(0);
    grid.invalidate();
    grid.render();
    updateFilterStatus();
    //emparejarDatos();
}

function setDefaultHeader() {
    var columns = grid.getColumns();

    for (var kk = 0; kk < columnCount; kk++) {
        grid.updateColumnHeader("column"+kk, "Columna "+(kk+1));
    }

    grid.getData().insertItem(0, originalFirstRow);
    grid.invalidate();
    grid.render();
    updateFilterStatus();
    //emparejarDatos();
}

function updateFieldsToLink() {
    var columns = grid.getColumns();
    var combo = $(".fieldToLink");
    var combo2 = $("#fieldToGenerateMapConfig");

    combo.find('option').remove().end();
    combo2.find('option').remove().end();

    for (var i = 0; i < columnCount; i++) {
        var o = new Option(columns[i]["name"], columns[i]["value"]);
        /// jquerify the DOM object 'o' so we can use the html method
        $(o).html(columns[i]["value"]);
        combo.append(o);
        // hay que crear dos objetos independientes para cada combo
        var o2 = new Option(columns[i]["name"], columns[i]["value"]);
        $(o2).html(columns[i]["value"]);
        combo2.append(o2);
    }
}

function adjustDecimalComma(val) {
	/*
	 var aux = val.replace(',', '.');
	 if (! isNaN(aux)) {
	 return aux;
	 } else {
	 return val;
	 }
	 */

    // esto no funciona porque si se hace, no funciona los pares de coordenadas
    return val;
}

function getValueToFindNormalizado(rowIndex) {
    //var selectedColumnType = $("select[id='typeToLink'] option:selected").index();
    var type = $("#typeToLink").val();
    var str = "";
    if (type == "Calle") {
        var subtype = $("#patternToLink_" + type).val();
        var numFields = 1;
        if (subtype == 2) {
            numFields = 2;
        }
        for (var j = 1; j <= numFields; j++) {
            var selectedColumn = $("select[id='fieldToLink_" + type + "_" + subtype + "_" + j + "'] option:selected").index();
            var separador = " ";
            if (j==1) {
                separador = "";
            } else if ((subtype==2) && (j==2)) {
                separador = ",";
            }
            str += separador + normalizaCadenaSpanish(grid.getDataItem(rowIndex)["column" + selectedColumn]);
        }
    } else if (type == "Portal") {
        var subtype = $("#patternToLink_" + type).val();
        var numFields = 1;
        if ((subtype == 2) || ( subtype == 3)) {
            numFields = 2;
        } else if (subtype == 4) {
            numFields = 3;
        }
        for (var j = 1; j <= numFields; j++) {
            var selectedColumn = $("select[id='fieldToLink_" + type + "_" + subtype + "_" + j + "'] option:selected").index();
            var separador = " ";
            if (j==1) {
                separador = "";
            } else if ((subtype==3) && (j==2)) {
                separador = ",";
            } else if ((subtype==4) && (j==3)) {
                separador = ",";
            }
            str += separador + normalizaCadenaSpanish(grid.getDataItem(rowIndex)["column" + selectedColumn]);
        }
    } else if (type == "Cuad5k") {
        var subtype = $("#patternToLink_" + type).val();
        var numFields = 1;
        if (subtype == 2) {
            numFields = 2;
        }
        for (var j = 1; j <= numFields; j++) {
            var selectedColumn = $("select[id='fieldToLink_" + type + "_" + subtype + "_" + j + "'] option:selected").index();
            var separador = " ";
            var currentValue = normalizaCadenaSpanish(grid.getDataItem(rowIndex)["column" + selectedColumn]);
            if (j==1) {
                separador = "";
            } else if ((subtype==2) && (j==2)) {
                separador = "-";
                if (! isNaN(parseInt(currentValue))) {
                    if (parseInt(currentValue) < 10) {
                        // a�adirle por si acaso el 0 para los menores a 10
                        currentValue = "0" + parseInt(currentValue);
                    }
                }
            }
            str += separador + currentValue;
        }
    } else if (type == "Coordenadas") {
        var subtype = $("#patternToLink_" + type).val();
        var numFields = 1;
        if (subtype == 2) {
            numFields = 2;
        }
        for (var j = 1; j <= numFields; j++) {
            var selectedColumn = $("select[id='fieldToLink_" + type + "_" + subtype + "_" + j + "'] option:selected").index();
            var separador = " ";
            if (j==1) {
                separador = "";
            } else if ((subtype==2) && (j==2)) {
                separador = ",";
            }
            str += separador + normalizaCadenaSpanish(grid.getDataItem(rowIndex)["column" + selectedColumn]);
        }
    } else if (type == "Toponimo") {
        var subtype = $("#patternToLink_" + type).val();

        var selectedColumn = $("select[id='fieldToLink_" + type + "_" + subtype + "_1'] option:selected").index();
        var str = /*normalizaCadenaSpanish(*/grid.getDataItem(rowIndex)["column" + selectedColumn]/*)*/;

    

    } else if (type == "BusSIGPAC"){
        var subtype = $("#patternToLink_" + type).val();
        var numFields = 1;
        if (subtype == 2) {
            numFields = 6;
        }
        for (var j = 1; j <= numFields; j++) {
            var selectedColumn = $("select[id='fieldToLink_" + type + "_" + subtype + "_" + j + "'] option:selected").index();
            var separador = "";
            var valor=normalizaCadenaSpanish(grid.getDataItem(rowIndex)["column" + selectedColumn]);
            if ((j==2)||(j==3)||(j==5)) {
                valor = stuff(valor,3,'0');
            } else if (j==4) {
            	valor = stuff(valor,2,'0');
            }
            else if (j==6) {
            	valor = stuff(valor,5,'0');
            }
            str += separador + valor;
        }
    }  else {
        var selectedColumn = $("select[id='fieldToLink_Unique'] option:selected").index();
        str = normalizaCadenaSpanish(grid.getDataItem(rowIndex)["column" + selectedColumn]);
    }
    return str;
}

function stuff(texto, long, char){
	var res = texto;
	while (res.length<long){
		res = char+res;
	}
	return res;
}
function updateFilterStatus() {
    // Excel like status bar at the bottom
    var status = "";
    var totalCount = grid.getData().getItems().length;
    var currentCount = grid.getData().getLength();

    if (currentCount === totalCount) {
        status = "";
    } else {
        if (currentCount == 1) {
            status = 'Seleccionada 1 fila de ' + totalCount + ' filas';
        } else {
            status = 'Seleccionadas ' + currentCount + ' filas de ' + totalCount;
        }
    }
    $('#status-label').text(status);
	/*
	 if (status == "") {
	 $("#firstRowAsHeaderDiv").removeClass("oculto");
	 } else {
	 $("#firstRowAsHeaderDiv").addClass("oculto");
	 }*/
}

