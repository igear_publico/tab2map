var mapContentModal = null;

$(document).ready(function() {
	/*image_change();
	$("#flechaNext").click(function () { next(); });
	$("#flechaBack").click(function () { back(); });*/
	changeAmbitoTooltip();
	changeMapa();
});

function showMapContentModal() {
	if (mapContentModal == null) {
		mapContentModal = $('[data-remodal-id=mapContentModal]').remodal();
	}
	mapContentModal.open();
}

$(document).on('closed', '.remodal', function (e) {
});

/*
var currentActivePage = 0;
var maxPagesActived = 1;
*/
var jobid;
var solicitado = false;
/*
function showFlechaBack() {
	$("#flechaBack").css("visibility", "visible");
}
function hideFlechaNext() {
	$("#flechaNext").css("visibility", "hidden");
}
function showFlechaNext() {
	$("#flechaNext").css("visibility", "visible");
}

function back() {
	hidePage(currentActivePage);

	currentActivePage--;

	showFlechaNext();

	showPage(currentActivePage);

	if (currentActivePage == 0) {
		$("#flechaBack").css("visibility", "hidden");
	}
}

function next() {
	hidePage(currentActivePage);
	currentActivePage++;
	showPage(currentActivePage);
	showFlechaBack();
	if (currentActivePage == 2) {
	hideFlechaNext();
	}
}
*/

function clickFormat(format) {
	if (solicitado) {
		alert("Hay un lote en curso. Espere a que finalice.");
		return;
	}
	solicitado=true;
	var ambito_val = document.getElementById("ambito").value;
	jobid=null;
	var layers ="";
	$(".listaCapas input:not(:checked)").each(function() {
		if (layers.length>0) {
			layers +=",";
		}
		layers += $(this)[0].name;
	});

	$.ajax({
		url: service_url+"?REQUEST=initJob&MAPA="+document.getElementById("mapa").value
					+"&AMBITO="+ambito_val
					+"&FORMATO="+format+"&ORIENTACION="+$('input[name="orientacion"]:checked').val()
					+"&TAMANO="+$('input[name="tamano"]:checked').val()+(layers.length>0?"&CAPASNO="+layers:""),
		type: 'GET',

		//async:false,
		success: function(data) {
			jobid = $.trim(data);
			$("#cancel").css("display","block");
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Error al iniciar el trabajo "+textStatus +"\n "+errorThrown);
			jobid=-1;
			solicitado=false;
		},
	});

	var total_mapas = getTotalMapas(ambito_val);

	if (total_mapas>0) {
		document.getElementById("terminados").innerHTML = 0;
		document.getElementById("total").innerHTML = total_mapas;
		$("#cuantosMapasLlevoTxt").css("display","block");
		$("#tiempoEstimado").css("display","block");
	}
	setTimeout(function() {refreshProgress(0);}, PROGRESS_REQUEST_INTERVAL);
}

function showProgress(){
	var ambito_val = document.getElementById("ambito").value;
	var total_mapas = getTotalMapas(ambito_val);

	if (total_mapas>0) {
		document.getElementById("terminados").innerHTML = 0;
		document.getElementById("total").innerHTML = total_mapas;
		$("#cuantosMapasLlevoTxt").css("display","block");
		$("#tiempoEstimado").css("display","block");
	}
	setTimeout(function() {refreshProgress(0);}, PROGRESS_REQUEST_INTERVAL);
}

function getTotalMapas(ambito_val) {
	if (ambito_val=="CCAA") {
		return 1;
	} else if (ambito_val=="PROV") {
		return 3;
	} else if (ambito_val=="COMARCA") {
		return 33;
	}	else if (ambito_val=="TTMM") {
		return 731;
	} else if (ambito_val=="HOJA50") {
		return 122;
	}
	return 0;
}

function cancelJob() {
	if (!solicitado) {
		alert("No hay ninguna ejecuci�n en curso.");
		return;
	}
	if (jobid==null) {
		setTimeout(cancelJob,1000);
		return;
	}
	alert("Proceso abortado. Se descargar�n los mapas elaborados hasta el momento");

	$.ajax({
		url: tab2mapWS_url+"?REQUEST=cancelJob&JOBID="+jobid,
		type: 'GET',
		//async:false,
		success: function(data) {
		},
		error: function(jqXHR, textStatus, errorThrown) {
			alert("Error al cancelar el trabajo "+textStatus +"\n "+errorThrown);
		}
	});
}

function refreshProgress(iteracion) {
	if ((jobid==null)||(jobid=="")) {
		setTimeout(function() {refreshProgress(iteracion+1);}, PROGRESS_REQUEST_INTERVAL);
	} else if (jobid>0) {
		$.ajax({
			url: tab2mapWS_url+"?REQUEST=getJobProgress&JOBID="+jobid,
			type: 'GET',
			//async:false,
			success: function(data) {
				var terminados = $.trim(data);
				if (terminados=="NOJOB") {
					alert("Error en la generaci�n del lote o lote ya entregado");
				} else if (terminados=="CANCELLED") {
					window.location.href = tab2mapWS_url+"?REQUEST=getJob&JOBID="+jobid;
					solicitado=false;
				} else if (terminados!="ALL") {
					var antes = document.getElementById("terminados").innerHTML;
					if (terminados>antes) {
					//	alert(" antes "+antes+" ahora "+terminados+ " total "+document.getElementById("total").innerHTML+" iteracion "+iteracion);
						var segundos = ((document.getElementById("total").innerHTML-terminados)*((iteracion*PROGRESS_REQUEST_INTERVAL)/terminados))/PROGRESS_REQUEST_INTERVAL;
						//alert(segundos);
						if (segundos >60) {
							var minutos = segundos/60;
							//alert(minutos);
							if (minutos>60) {
								var horas = Math.floor(minutos/60);
								minutos =minutos-(horas*60);
								document.getElementById("tiempo").innerHTML = horas+" horas "+Math.round(minutos)+" minutos";
							} else {
								minutos = Math.floor(minutos);
								segundos = segundos -(minutos*60);
								document.getElementById("tiempo").innerHTML =Math.floor(minutos)+" minutos "+Math.round(segundos)+" segundos";
							}
						} else {
							document.getElementById("tiempo").innerHTML = Math.round(segundos)+" segundos";
						}
					}
					document.getElementById("terminados").innerHTML = terminados;
					setTimeout(function() {refreshProgress(iteracion+1);}, PROGRESS_REQUEST_INTERVAL);
				} else {
					document.getElementById("terminados").innerHTML = document.getElementById("total").innerHTML ;
					document.getElementById("tiempo").innerHTML = "0 segundos";
					window.location.href = tab2mapWS_url+"?REQUEST=getJob&JOBID="+jobid;
					solicitado=false;
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				solicitado=false;
			}
		});
	}
}
/*
function showPage(currentActivePage) {
	$("#page"+currentActivePage).fadeIn(300);
}

function hidePage(currentActivePage) {
	$("#page"+currentActivePage).fadeOut(300);
}
*/
function changeAmbitoTooltip() {
	var ambito = $("#ambito").val();
	var tooltip="";
	if (ambito=="CCAA") {
		tooltip="Un mapa de la Comunidad Aut�noma de Arag�n";
	}
	else if (ambito=="PROV") {
		tooltip="Un mapa para cada provincia de la Comunidad Aut�noma de Arag�n (3 mapas)";
	}
	else if (ambito=="COMARCA") {
		tooltip="Un mapa para cada comarca de la Comunidad Aut�noma de Arag�n (33 mapas)";
	}
	else if (ambito=="TTMM") {
		tooltip="Un mapa para cada municipio de la Comunidad Aut�noma de Arag�n (731 mapas)";
	}
	else if (ambito=="HOJA50") {
		tooltip="Un mapa para cada hoja 50.000 de la Comunidad Aut�noma de Arag�n (122 mapas)";
	}
	//document.getElementById("infoAmbito").title = tooltip;
	$("#infoAmbito").tooltip();
}


function changeMapa() {
	
	var count = CAPAS_COUNT;
	var lista = ""
	for (var i=1; i<=count; i++) {
		lista += '<li><input type="checkbox" name="capa_'+i+'"> '+eval("CAPA_NAME_"+i) + '<img src="'+eval("CAPA_ICON_"+i)+'"></li>';
	}
	$("#listaCapas").html(lista);
	$("#seleccionCapas").fadeIn(300);
}
