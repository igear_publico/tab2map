var pararEmparejamiento = false;
var timeoutEmpareja;
var cuantosLlevo = 0;

	// Hacer lo que corresponda una vez cargada la p�gina
$(document).ready(function() {
	$('#firstRowAsHeader').prop('checked', false);

	$('#firstRowAsHeader').change(function() {
		//clearTimeout(timeoutEmpareja);
		if($(this).is(":checked")) {
			setFirstRowAsHeader();
		} else {
			setDefaultHeader();
		}
		updateFieldsToLink();
	});

	clearPreviousValues();

	$("#fieldToLink").change(function() {
		//emparejarDatos();
		pararEmparejamiento = true;
		$('#emparejar').prop('disabled', false);
	});

	$(".fieldToLink").change(function() {
		//emparejarDatos();
		pararEmparejamiento = true;
		$('#emparejar').prop('disabled', false);
	});

	$("#typeToLink").change(function() {
		//updateColumnaEmparejamientos()
		pararEmparejamiento = true;
		$('#emparejar').prop('disabled', false);
		updatePatterns();
	});

	$(".patternToLink").change(function() {
		//updateColumnaEmparejamientos()
		pararEmparejamiento = true;
		$('#emparejar').prop('disabled', false);
		updatePatterns();
	});

	initAutoUpdateRadioExtentAfterChange();
	initComboMunis();
	initComboComarcas();
	initComboLocalidad();

	$("#emparejarBtn")
		.button()
		.click(function( event ) {
			$('#emparejar').prop('disabled', true);
			//$('#parar').prop('disabled', false);
			emparejarDatos();
			//event.preventDefault();
		});
		$('#emparejar').prop('disabled', false);
/*
	$("#parar")
		.button()
		.click(function( event ) {
			pararEmparejamiento = true;
			clearTimeout(timeoutEmpareja);
			$('#emparejar').prop('disabled', false);
			$('#parar').prop('disabled', true);
			//event.preventDefault();
		});

		$('#emparejar').prop('disabled', true);
		$('#parar').prop('disabled', false);
*/
/*
	$("#extent").change(function() {
		var extentType = $("#extent").val();
		if (extentType == "Global") {
			$("#provincias").addClass("oculto");
			$("#textoComarca").addClass("oculto");
			$("#textoMuni").addClass("oculto");
		} else if (extentType == "Provincia") {
			$("#provincias").removeClass("oculto");
			$("#textoComarca").addClass("oculto");
			$("#textoMuni").addClass("oculto");
		} else if (extentType == "Comarca") {
			$("#provincias").addClass("oculto");
			$("#textoComarca").removeClass("oculto");
			$("#textoMuni").addClass("oculto");
		} else if (extentType == "Municipio") {
			$("#provincias").addClass("oculto");
			$("#textoComarca").addClass("oculto");
			$("#textoMuni").removeClass("oculto");
		}
	});*/


	$("#extentType").switchButton({
		checked: false,
		width: 40,
		height: 18,
		button_width: 20,
		off_label: 'Predefinida',
		on_label: 'Personalizada'
	});
	$('#extentType').change(function() {
		if($(this).is(":checked")) {
			deactivateExtentDefault();
		} else {
			activateExtentDefault();
		}
	});

	$("#mapCount").switchButton({
		checked: false,
		width: 40,
		height: 18,
		button_width: 20,
		off_label: 'Mapa único',
		on_label: 'Lote de mapas'
	});
	$('#mapCount').change(function() {
		if($(this).is(":checked")) {
			activateBatchMap();
		} else {
			deactivateBatchMap();
		}
	});

	function activateBatchMap() {
		$("#extentSwitch").hide();
		$("#defaultExtent").hide();
		$("#defaultExtentBatch").show();
		$("#batchInfoProcess").show();
	}
	function deactivateBatchMap() {
		$("#defaultExtentBatch").hide();
		$("#batchInfoProcess").hide();
		$("#extentSwitch").show();
		$("#defaultExtent").show();
	}
	function activateExtentDefault() {
		$("#customExtent").fadeOut(250);
		setTimeout('$("#defaultExtent").fadeIn(250);', 260);
	}

	function deactivateExtentDefault() {
		$("#defaultExtent").fadeOut(250);
		setTimeout('$("#customExtent").fadeIn(250);', 260);
	}
});

function clearPreviousValues() {
	$("select").prop('selectedIndex', 0);
	$( ":text" ).val('');
	$("#tituloMapa").val('Mapa realizado con la aplicación geográfica Tabla a Mapa de IDEAragon');
}

function initAutoUpdateRadioExtentAfterChange() {
	$("#provincias").change(function() {
		$("input[name=extent][value=Provincia]").prop('checked', true);
	});
	$("#textoComarca").change(function() {
		$("input[name=extent][value=Comarca]").prop('checked', true);
	});
	$("#textoMuni").change(function() {
		$("input[name=extent][value=Municipio]").prop('checked', true);
	});
	$("#textoLocalidad").change(function() {
		$("input[name=extent][value=Localidad]").prop('checked', true);
	});
	$("#scaleExtent").change(function() {
		$("input[name=extent][value=scale]").prop('checked', true);
	});
	$("#scaleXExtent").change(function() {
		$("input[name=extent][value=scale]").prop('checked', true);
	});
	$("#scaleYExtent").change(function() {
		$("input[name=extent][value=scale]").prop('checked', true);
	});
	$("#minimumGap").change(function() {
		$("input[name=extent][value=minimum]").prop('checked', true);
	});
}

function updatePatterns() {
	$(".pattern").addClass("oculto");

	var type = $("#typeToLink").val();
	$(".patternToLink_row_" + type).removeClass("oculto");

	if ((type == "Calle") || (type == "Portal") || (type == "Cuad5k") || (type == "Coordenadas")|| (type == "Toponimo")|| (type == "BusSIGPAC")) {
		var subtype = $("#patternToLink_" + type).val();
		$(".fieldToLink_" + type + "_" + subtype).removeClass("oculto");
	} else {
		$(".fieldToLink_Unique").removeClass("oculto");
	}
}

var map;
function showHelpViewer(){
	var proporcion = 500/800;
	var viewer_width= $(window).width()-50;
	var viewer_height= $(window).height()-50;
	if ($('input[name=orientacion]:checked').val() == "Vertical") {
		if (viewer_width > viewer_height*proporcion){
			viewer_width = viewer_height * proporcion;
		}
		else{
			viewer_height = viewer_width / proporcion;
		}
	}
	else{
		if (viewer_height > viewer_width * proporcion){
			viewer_height = viewer_width * proporcion;
		}
		else{
			viewer_width = viewer_height / proporcion;
		}
	}
	var map_height = viewer_height-80;
	var fullExtent_left = viewer_width - 70;
	var viewer = $('<div></div>')
								.html('<div id="geoselection">'+
			'<div>Centrar en el municipio:<input type="text" id="area" class="areaClass" placeholder="Municipio" />'+
			'</div>'+
			'<div id="mapPanel" class="mapPanelClass">'+
			'<div id="map" class="mapClass" style="height:'+map_height+'px;">'+
			'<div id="northDiv">'+
			'<img id="northLabel2" class="northLabel" class="olControlNoSelect" src="/lib/IDEAragon/themes/default/images/north.png" border="0" ></img>'+
			'						</div>'+
			'<div class="zoom olControlZoom" align="right" style="left:'+fullExtent_left+'px;">'+

			'						<div class="mq-zoombuttons ui-widget ui-helper-clearfix ">'+
			'							<div id="iconoInicio" class="icono zoomInicio"></div>'+
			'						</div>'+
			'					</div>'+
			'</div>'+

			'</div>'+
			'</div>')
						.dialog({
								autoOpen: false,
								modal: true,
								width: viewer_width,
								height: viewer_height,
								close: function () {
									if (boxLayer.features.length<=0){
										if (confirm("Usted no ha marcado ninguna extensión sobre el mapa ¿Desea que la extensión sea la visualizada en el mapa?")) {
											var extent = map.getExtent();
												$("#userExtentW").val(extent.left);
												$("#userExtentS").val(extent.bottom);
												$("#userExtentE").val(extent.right);
												$("#userExtentN").val(extent.top);
											}
									}

									$( this ).dialog( "destroy" );
								},
								title: "Mapa interactivo para establecer extensión geográfica" 
						});
	initAutocomplete(); 

	viewer.dialog("open");
	initMap();
}

var $dialog_XYscale = null;
function showHelpScale() {
	var page = "ext/BuscadorCajaUnica/indexEmbed.html";
	$dialog_XYscale = $('<div></div>')
							.html('<iframe style="border: 0px; " src="' + page + '" width="100%" height="100%"></iframe>')
							.dialog({
									autoOpen: true,
									modal: true,
									width: $(window).width()-50,
									height: $(window).height()-50,
									close: function () {
											$dialog_XYscale = null;
									},
									title: "Buscador de nombres geográficos para localizar un punto" 
							});
}

function updateScaleXY(x, y) {
	$("input[name=extent][value=scale]").prop('checked', true);
	$("#scaleXExtent").val(x);
	$("#scaleYExtent").val(y);
	$dialog_XYscale.dialog("close");
}
