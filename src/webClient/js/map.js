 var boxLayer;

function initAutocomplete() {
	cUnitList = new Array();
	dUnitList = new Array();
	bUnitList = new Array();
	customarrayUnitJQ = new Array();
	var index=0;
	var lista = muniDefault.split("\n");

	for (var i=0; i<lista.length; i++) {
		var unidad = lista[i].split(";");
		if (unidad.length > 2) {
			dUnitList[unidad[1]] = unidad[0];
			cUnitList[unidad[0]] = unidad[1];

			bUnitList[unidad[0]] = unidad[2];

			customarrayUnitJQ[index] = unidad[0];
			index++;
		}
	}

	$( "#area" ).autocomplete({
		select: function( event, ui ) {
			zoomTo(bUnitList[ui.item.value],map);
		},
		source: function(request, response) {
			var results = $.ui.autocomplete.filter(customarrayUnitJQ, request.term);
			response(results.slice(0, 10));
		}
	});
}

function initMap() {
	var options = {
			projection : WMS_SRS,
			maxExtent : new OpenLayers.Bounds(WMS_BBOX_X_MIN, WMS_BBOX_Y_MIN, WMS_BBOX_X_MAX, WMS_BBOX_Y_MAX),//551580, 4402223, 787351, 4756639),
			center : new OpenLayers.LonLat(675533, 4589000),
			units : "m",
			scales : [ 1750000, 1000000, 500000,300000,200000,100000,25000,10000,5000, 1000 ],
			controls : [ new OpenLayers.Control.ArgParser(),
									new OpenLayers.Control.Attribution()/*,
									new OpenLayers.Control.PanZoom(),
									new OpenLayers.Control.Navigation()*/]
	};

	map = new OpenLayers.Map('map', options);
	map.layerContainerDiv.style.left="0";
	map.layerContainerDiv.style.top="0";

	// capas base
	var wmscURL = [ WMS_URL ];
	var baseLayer = new OpenLayers.Layer.WMS('Mapa base', wmscURL, {
		layers : WMS_LAYERS,
		format : 'image/png'
	}, {
		singleTile: true, ratio: 1,
		buffer : 0,
		isBaseLayer : true//,
		// escala, fondo y norte
	//	attribution: '<div><img border="0" id="north" src="images/norte.png" alt="norte"><div id="escalaMapa">Escala = 1:3.000.000</div><div id="fondo">Fondo:'+FONDO+'</div>'
	});
	map.addLayer(baseLayer);
	boxLayer = new OpenLayers.Layer.Vector("Box layer");

	map.addLayer(boxLayer);
	var drawControl = new OpenLayers.Control.DrawFeature(boxLayer,
					OpenLayers.Handler.RegularPolygon, {
						handlerOptions: {
								sides: 4,
								irregular: true
						}
				});
	map.addControl(drawControl);
	drawControl.activate();
	boxLayer.events.register('beforefeatureadded', boxLayer, onBoxDrawing);
	boxLayer.events.register('featureadded', boxLayer, onBoxDrawed);

	$("#iconoInicio").click(function() {
		zoomToExtent(map);
	});

	map.addControl(new OpenLayers.Control.PanZoom());
/*	map.addControl(new OpenLayers.Control.Navigation());*/


	// visualizar todo Aragón
	zoomToExtent(map);

	initZoombar();
}

function onBoxDrawing(evt) {
	evt.feature.layer.destroyFeatures();
}

function onBoxDrawed(evt) {
	var extent = evt.feature.geometry.bounds;
		$("#userExtentW").val(extent.left);
		$("#userExtentS").val(extent.bottom);
		$("#userExtentE").val(extent.right);
		$("#userExtentN").val(extent.top);
}

function zoomToExtent(mapa) {
	mapa.zoomToExtent(new OpenLayers.Bounds(571580, 4412223, 812351, 4756639));
}

function zoomTo(bbox,mapa) {
	mapa.zoomToExtent(new OpenLayers.Bounds(bbox.split(",")));
	//$("#searchAreaButton").button( "enable" );
}

function initZoombar() {
	$(".olControlPanZoom *").each(function() {
		if (this.id.indexOf("innerImage")>=0) {
			$(this).addClass("panZoomBar_innerImage");
		}
		else if (this.id.indexOf("panup")>=0) {
			$(this).addClass("panZoomBar_panup");
		}
		else if (this.id.indexOf("pandown")>=0) {
			$(this).addClass("panZoomBar_pandown");
		}
		else if (this.id.indexOf("panleft")>=0) {
			$(this).addClass("panZoomBar_panleft");
		}
		else if (this.id.indexOf("panright")>=0) {
			$(this).addClass("panZoomBar_panright");
		}
		else if (this.id.indexOf("zoomin")>=0) {
			$(this).addClass("panZoomBar_zoomin");
		}
		else if (this.id.indexOf("zoomout")>=0) {
			$(this).addClass("panZoomBar_zoomout");
		}
		else if (this.id.indexOf("zoomworld")>=0) {
			$(this).addClass("panZoomBar_zoomworld");
		}
	});
}

