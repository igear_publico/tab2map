var currentActivePage = 0;

function showFlechaBack() {
	$("#flechaBack").css("visibility", "visible");
}
function hideFlechaNext() {
	$("#flechaNext").css("visibility", "hidden");
}
function showFlechaNext() {
	$("#flechaNext").css("visibility", "visible");
}

function back() {
	showFlechaNext();

	pararEmparejamiento = true;

	if ($("#generatedMap").attr("src") != "") {
		$("#generatedMap").attr("src", "");
		$("#generatedMap").addClass("oculto");
		$("#generatedLegend").attr("src", "");
		$("#generatedLegend").addClass("oculto");
		$("#descargarBtn").addClass("oculto");
		$("#descargarPDFBtn").addClass("oculto");
	}
	if ($("#resultForm").css("visibility") == "visible") {
		$("#resultForm").addClass("oculto");
	}

	hidePage(currentActivePage);

	currentActivePage--;

	if (fileType == "GPX") {
		if ((currentActivePage == 1) || (currentActivePage == 5)) {
			currentActivePage--;
		}
	}

	showPage(currentActivePage);

	if (currentActivePage == 0) {
		$("#flechaBack").css("visibility", "hidden");
		$('#resultadosDiv').css("visibility", "hidden");
	} else if (currentActivePage == 5) {
		$('#resultadosDiv').css("visibility", "visible");
	}
}

function next() {
	pararEmparejamiento = true;
				/*
	if (currentActivePage == 5) {
		if (! checkNumericValues()) {
			alert("El campo seleccionado para generar la estética debe contener exclusivamente valores numéricos. Seleccione otro campo");
			return;
		}
	}*/

	if (currentActivePage == 1) {
		hideFlechaNext();
	}

	showFlechaBack();

	hidePage(currentActivePage);
	setTimeout("next2()", 400);
}

var formatSelected = "";
function clickFormat(format) {
	formatSelected = format;
	if ((formatSelected == "JPG") || (formatSelected == "PDF")) {
		//$("#resultHiddenForm").addClass("oculto");
		showFlechaNext();
		next();
	} else {
		sendDataTbl2MapWS();
	}
}

function next2() {
	if (currentActivePage == 0) {
		if (cuantosLlevo == 0) {
			$("#flechaNext").css("visibility", "hidden");
		}
	}

	currentActivePage++;

	if (fileType == "GPX") {
		if ((currentActivePage == 1) || (currentActivePage == 5)) {
			currentActivePage++;
		}
	}

	showPage(currentActivePage);

	if (currentActivePage == 6) {
		$('#resultadosDiv').css("visibility", "hidden");

		if ($('input[name=orientacion]:checked').val() == "Vertical") {
			$('#resultMapDiv').css("width", "660");
		} else {
			$('#resultMapDiv').css("width", "980");
		}
		if (document.body && document.body.offsetHeight) {
			winH = document.body.offsetHeight;
			winW = document.body.offsetWidth;
		}
		if (document.compatMode == 'CSS1Compat' && document.documentElement
				&& document.documentElement.offsetWidth) {
			winH = document.documentElement.offsetHeight;
			winW = document.documentElement.offsetWidth;
		}
		if (window.innerWidth && window.innerHeight) {
			winH = window.innerHeight;
			winW = window.innerWidth;
		}
		$('#resultMapDiv').css("height", winH -320);
		sendDataTbl2MapWS();
		$("#flechaNext").css("visibility", "hidden");
	} else if (currentActivePage == 0) {
		$('#resultadosDiv').css("visibility", "hidden");
	}
	else if(currentActivePage == 5) {
		showPalette($("#colorNature")[0].value);
	}
	else {
		$('#resultadosDiv').css("visibility", "visible");
	}
}

function showPage(currentActivePage) {
	if (currentActivePage == 0) {
		$("#page0").fadeIn(300);
	} else if (currentActivePage == 1) {
		//$('#optionsZone').removeClass("oculto");
		$("#page1").fadeIn(300);
	} else if (currentActivePage == 2) {
		$("#page2").fadeIn(300);
	} else if (currentActivePage == 3) {
		$("#page3").fadeIn(300);
	} else if (currentActivePage == 4) {
		$("#page4").fadeIn(300);
	} else if (currentActivePage == 5) {
		$("#page5").fadeIn(300);
	} else if (currentActivePage == 6) {
		$("#page6").fadeIn(300);
		/*
	} else if (currentActivePage == 7) {
		$("#page7").fadeIn(300);
	} else if (currentActivePage == 8) {
		$("#page8").fadeIn(300);*/
	} else {
		// NOP
	}
}

function hidePage(currentActivePage) {
	if (currentActivePage == 0) {
		$("#page0").fadeOut(300);
	} else if (currentActivePage == 1) {
		//$('#optionsZone').addClass("oculto");
		$('#page1').fadeOut(300);
	} else if (currentActivePage == 2) {
		$('#page2').fadeOut(300);
	} else if (currentActivePage == 3) {
		$('#page3').fadeOut(300);
	} else if (currentActivePage == 4) {
		$('#page4').fadeOut(300);
	} else if (currentActivePage == 5) {
		$('#page5').fadeOut(300);
	} else if (currentActivePage == 6) {
		$('#page6').fadeOut(300);
		/*
	} else if (currentActivePage == 7) {
		$('#page7').fadeOut(300);
	} else if (currentActivePage == 8) {
		$('#page8').fadeOut(300);*/
	} else {
		// NOP
	}
}

function checkNumericValues() {
	var typeToLink = $("#typeToLink").val();
	var selectedFieldColumn = $("select[id='fieldToGenerateMapConfig'] option:selected").index();

	for (var i = 0; i < grid.getDataLength(); i++) {
		if ((grid.getDataItem(i)["column"+columnCount] > 0) || (grid.getDataItem(i)["column"+columnCount] == "S")) {
			var valorBuscado = getValueToFindNormalizado(i);
			var cuantosHay = cuantosHayDelTipo(valorBuscado, typeToLink);
			if (cuantosHay == 1) {
					// buscar la posicion para este tipo
				var idx = -1;
				for (j in linkedPosibilities[valorBuscado]) {
					if (linkedPosibilities[valorBuscado][j]["type"] == typeToLink) {
						idx = j;
					}
				}

				if (idx != -1) {
					if (isNaN(grid.getDataItem(i)["column" + selectedFieldColumn])) {
						return false;
					}
				}
			} else if (cuantosHay > 1) {
				if (multipleOptionsSelection[i]) {
					if (isNaN(grid.getDataItem(i)["column" + selectedFieldColumn])) {
						return false;
					}
				}
			}
		}
	}
	return true;
}


function changeLegendMethod(){
	if ($("#colorClassification").val()=="manual"){
		changeClasses();
		$( "#intervals" ).show();
	}
	else{
		$( "#intervals" ).hide();
		
	}
}

function changeClasses(){
	if ($("#colorClassification").val()=="manual"){
	$("#manualIntervals").empty();
	for (var i=0; i<$("#colorClasses").val();i++){
		if (i==0){
			$("#manualIntervals").append("<li>&lt<input id='class0' class='claseManual' type='number' onchange='setNextClass("+i+")'></input></li>");
		}
		else if (i==$("#colorClasses").val()-1){
			$("#manualIntervals").append("<li>&gt=<input id='class"+(i-1)+"bis' disabled type='number'></input></li>");
		}
		else{
			$("#manualIntervals").append("<li>&gt=<input id='class"+(i-1)+"bis' disabled type='number'></input> y &lt<input onchange='setNextClass("+i+")' class='claseManual' id='class"+i+"'  type='number'></input></li>");
		}
	}
	}
}

function setNextClass(clase){
	$("#class"+clase+"bis").val($("#class"+clase).val());
}