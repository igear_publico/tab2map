﻿function normalizaCadenaSpanish(entry) {
	var result = entry;
	result = replaceChar(result, 'ñ', 'ny');
	result = replaceChar(result, 'Ñ', 'NY');
	result = normalizaCadena(result);
	return result;
}

function doSpecialSearch(nomCapa, theValue, typeToFind, funcCallBack) {
	if (theValue != "") {
		if (isNaN(theValue)) {
			sendOneQuery(nomCapa, theValue, typeToFind, funcCallBack, txtFieldList[nomCapa]);
		} else {
			sendOneQuery(nomCapa, theValue, typeToFind, funcCallBack, codeFieldList[nomCapa]);
		}
	} else {
		alert("Introduzca un valor");
	}
}

function sendOneQuery(activeLyr, theValue, typeToFind, funcCallBack, nomField) {
	$.ajax({
		url: SimpleSearchServiceUrl + "type=" + tiposInverso[typeToFind] + "&texto=" + theValue,
		type: 'GET',
		success: function(data) { /*try{*/
			processResultQuery(data, theValue, activeLyr, typeToFind, nomField, funcCallBack);
			/*}
			 catch(err) {
			 showSearching(false);
			 alert("No se pudo completar la búsqueda "+err);
			 }*/},
		error: function() { console.log("error " + SimpleSearchServiceUrl + "type=" + tiposInverso[typeToFind] + "&texto=" + theValue);},
		processData: false,
		contentType: "text/xml; charset=\"utf-8\"",
		async: true
	});
}

function processResultQuery(theReply, valorBuscado, activeLayer, typeToFind, nomField, funcCallBack) {
	var json = $.xmlToJSON(theReply);
	if ((json == null) || (typeof json.Body[0].SearchResponse[0].SearchResult=='undefined')) {
		return;
	}

	for (var j=0; j < json.Body[0].SearchResponse[0].SearchResult.length; j++) {
		var contenidoElemento = json.Body[0].SearchResponse[0].SearchResult[j];
		var resultados = "";
		if (contenidoElemento.Count[0].Text == "0") {
			//buscarSOAP(1, valorBuscado, true, funcCallBack);
			eval(funcCallBack);
		}
		else if (contenidoElemento.Count[0].Text == "1") {
			num_results = num_results + parseInt(contenidoElemento.Count[0].Text);
			if (typeof contenidoElemento.List != 'undefined') {
				resultados = contenidoElemento.List[0].Text;
				tipo = contenidoElemento.Type[0].Text;

				extractInfo(resultados, valorBuscado, activeLayer, typeToFind, nomField, false);
				eval(funcCallBack);
			}
		}
		else if (contenidoElemento.Count[0].Text > "1") {
			var exito = false;
			resultados = contenidoElemento.List[0].Text;
			var lineas = resultados.split("\n");
			for (var i = 0; i < lineas.length; i++) {
				valores = lineas[i];
				var valorColumna = valores.split("#")[1]; //Tomamos nombre y comparamos
				if (valorColumna == valorBuscado) {
					exito = true;
					break;
				}
			}
			if (exito) {  //Ha encontrado el nombre exacto
				extractInfo(valores, valorBuscado, activeLayer, typeToFind, nomField, false);
				eval(funcCallBack);
			}
			else {
				for (var i = 0; i < lineas.length; i++) {
					valores = lineas[i];
					extractInfo(valores, valorBuscado, activeLayer, typeToFind, nomField, false);
				}
				eval(funcCallBack);

			}
		}
	}
}

function extractInfo(valores, valorBuscado, activeLayer, typeToFind, nomField, includeExtended) {
	var partes = valores.split("#");
	var nombre = partes[0];
	if (includeExtended) {
		nombre = nombre + ' (' + partes[1] + ')';
	}
	var objectid = partes[3];

	saveData(valorBuscado, nombre, typeToFind, objectid);
}
