﻿var rowsPerColor = null;
var colorsPerRow = new Array();
var palette;
function generateMapConfig() {
	rowsPerColor = new Object();
	var selectedFieldColumn = $("select[id='fieldToGenerateMapConfig'] option:selected").index();
	var typeToLink = $("#typeToLink").val();

	var legend = $("#styleMapConfig").val();
	var num_classes;
	//$("#resultados *").remove();

	var valores = new Array();
	for (var i = 0; i < grid.getDataLength(); i++) {
		if ((grid.getDataItem(i)["column"+columnCount] > 0) || (grid.getDataItem(i)["column"+columnCount] == "S")) {
			var valorBuscado = getValueToFindNormalizado(i);

			var cuantosHay = cuantosHayDelTipo(valorBuscado, typeToLink);
			if (cuantosHay == 1) {
					// buscar la posicion para este tipo
				var idx = -1;
				for (j in linkedPosibilities[valorBuscado]) {
					if ((linkedPosibilities[valorBuscado][j]["type"] == typeToLink)||
							(linkedPosibilities[valorBuscado][j]["type"] == tipos[typeToLink])) {
						idx = j;
					}
				}

				if (idx != -1) {
					var valor = grid.getDataItem(i)["column" + selectedFieldColumn];
					if (!isNaN(valor)) { // es numérico si no no lo añado
						valores.push(parseInt(valor));
					}
				}
			} else if (cuantosHay > 1) {
				if (multipleOptionsSelection[i]) {
					var valor = grid.getDataItem(i)["column" + selectedFieldColumn];
					if (!isNaN(valor)) { // es numérico si no no lo añado
						valores.push(parseInt(valor));
					}
				}
			}
		}
	}

		// si no hay datos, salgo y no genero intervalos
	if (valores.length == 0) {
		return false;
	}

	valores.sort(function(a,b) {return a-b;});

	var intervalos = new Array();

	num_classes = $("#colorClasses")[0].value;
	intervalos = getLegendCategories(valores, num_classes);
	// }

	// for (var i =1; i<intervalos.length; i++) {

		//$("#resultados").append('<div class="colorPicker" style="background-color:'+colors[i-1]+
			//';"></div><span>menor que '+intervalos[i]+'</span>');
	// }

	//$("#resultados").append("<h3>valores</h3>");


	for (var i = 0; i < grid.getDataLength(); i++) {
		if ((grid.getDataItem(i)["column"+columnCount] > 0) || (grid.getDataItem(i)["column"+columnCount] == "S")) {
			var valorBuscado = getValueToFindNormalizado(i);

			var cuantosHay = cuantosHayDelTipo(valorBuscado, typeToLink);
			if (cuantosHay == 1) {
					// buscar la posicion para este tipo
				var idx = -1;
				for (j in linkedPosibilities[valorBuscado]) {
					if ((linkedPosibilities[valorBuscado][j]["type"] == typeToLink)||
					(linkedPosibilities[valorBuscado][j]["type"] == tipos[typeToLink])) {
						idx = j;
					}
				}

				if (idx != -1) {
					var valor = grid.getDataItem(i)["column" + selectedFieldColumn];
					if (!isNaN(valor)) { // es numérico si no no lo añado
						for ( var j = 0; j < intervalos.length-1; j++) {
								// buscar la categoría en la que se encuentra
							if ((valor >= intervalos[j])
								&& ((valor < intervalos[j+1]) || (j == intervalos.length - 1))) {
								colorsPerRow[i] = colors[j];
								var paletteColor = palette[num_classes-1][j];
								if(!rowsPerColor[paletteColor]) {
									rowsPerColor[paletteColor]=new Array();
								}
								rowsPerColor[paletteColor].push(i);
								minIntervalForColor[paletteColor]=intervalos[j];
								maxIntervalForColor[paletteColor]=intervalos[j+1]-1;
								//$("#resultados").append('<div class="colorPicker" style="background-color:'+colors[j]+
								//	';"></div><span>'+valor+'</span>');
								break;
							}
						}
					}
				}
			} else if (cuantosHay > 1) {
				if (multipleOptionsSelection[i]) {
					var valor = grid.getDataItem(i)["column" + selectedFieldColumn];
					if (!isNaN(valor)) { // es numérico si no no lo añado
						for ( var j = 0; j < intervalos.length-1; j++) {
								// buscar la categoría en la que se encuentra
							if ((valor >= intervalos[j])
								&& ((valor < intervalos[j+1]) || (j == intervalos.length - 1))) {
								colorsPerRow[i] = colors[j];
								var paletteColor = palette[num_classes-1][j];
								if(!rowsPerColor[paletteColor]) {
									rowsPerColor[paletteColor]=new Array();
								}
								rowsPerColor[paletteColor].push(i);
								minIntervalForColor[paletteColor]=intervalos[j];
								maxIntervalForColor[paletteColor]=intervalos[j+1]-1;
								//$("#resultados").append('<div class="colorPicker" style="background-color:'+colors[j]+
								//	';"></div><span>'+valor+'</span>');
								break;
							}
						}
					}
				}
			}
		}
	}
		// notifico que hay valores y, por tanto, intervalos => Se puede construir el mapa
	return true;
}

function componentToHex(c) {
	var hex = parseInt(c).toString(16);
	return hex.length == 1 ? "0" + hex : hex;
}

function rgbToHex(rgb) {
	colors = rgb.replace("rgb(","").replace(")","").replace(" ","").split(",");
	/*	alert(rgb+" -> #"+componentToHex(colors[0]) + componentToHex(colors[1]) + componentToHex(colors[2])+
				" ->"+parseInt(componentToHex(colors[0]) + componentToHex(colors[1]) + componentToHex(colors[2]),16));*/
	return componentToHex(colors[0]) + componentToHex(colors[1]) + componentToHex(colors[2]);
}
function rgbToInt(rgb) {

/*	alert(rgb+" -> #"+componentToHex(colors[0]) + componentToHex(colors[1]) + componentToHex(colors[2])+
			" ->"+parseInt(componentToHex(colors[0]) + componentToHex(colors[1]) + componentToHex(colors[2]),16));*/
	return parseInt(rgbToHex(rgb),16);
}

/**
 * devuelve los límites de las categorías de la leyenda, en función de los valores pasados como parámetro y el metodo seleccionado
 * por el usuario
 * @param values valores que se van a representar
 * @returns vector que contiene en la primera posición el límite inferior de la primera clase y en el resto los límites superiores
 * para todas las categorías
 */
function getLegendCategories(values, num_classes) {
	switch (document.getElementById("colorClassification").selectedIndex) {
	case 0: return getJenksBreaks(values, num_classes);
	break;
	case 1: return getPercentiles( values, num_classes);
	break;
	case 2: return getStdDevRanges( values, num_classes);
	break;
	case 3: return getUserRanges(values, num_classes);
	break;
	}
}

function getUserRanges(values, num_classes){

	// calcular los límites
	var userValues = new Array();
	// poner en la primera posición el valor mínimo -1
	userValues.push(values[0]-1);
	// userValues los límites correspondientes a las desviaciones establecidas en
	// el vector
	for (var i=0; i<num_classes-1; i++){
		userValues.push(parseFloat($("#class"+i).val()));
	}
	// poner en la ultima posición el valor máximo + 1
	userValues.push(values[values.length-1]+1);

	return userValues;


}
function getBreaks(inicio, fin, num_classes) {
	var range = new Array();

	var mod = Math.round((fin -inicio)/(num_classes));
	for (var i=0; i<(num_classes-1);i++) {
		range.push(inicio+(mod*(i+1)));
	}
	return range;
}

function showStyleOptions() {
	var sel = $("#styleMapConfig").val();
	$("#colors .colors").css("display","none");
	$("#colors_"+sel).css("display","block");
}

/**
 * devuelve un vector con los percentiles definidos en el vector percentiles
 * para el conjunto de valores del vector values. En la primera posición del
 * resultado devuelve el valor mínimo (menos 1 por posibles redondeos) del
 * vector values y en la última el máximo (más 1 por posibles redondeos)
 *
 * @param percentiles
 *            vector con los percentiles que hay que calcular
 * @param values
 *            vector con los valores sobre los que hay que calcular los
 *            percentiles
 * @returns {Array} vector con los valores de percentil indicados. Además en la
 *          primera posición se incluye el valor mínimo -1 y en la última el
 *          máximo +1
 */
function getPercentiles( values, num_classes) {
	var percentiles =getBreaks(0, 100, num_classes);
	var percValues = new Array();
	// ordenar los valores de menor a mayor
	//values = values.sort(function(a,b) {return a-b;});
	// poner en la primera posición el valor mínimo -1
	percValues.push(values[0]-1);
	// calcular los percentiles
	for (var i=0; i<percentiles.length; i++) {
		// calcular la posición del vector correspondiente al percentil
		var index = ((values.length-1)*percentiles[i])/100;

		if (index == Math.floor(index)) { // si el valor es un entero, ese es
			// el índice a buscar
			index = Math.floor(index);
		}
		else{ // si el valor no es entero cojo el primer entero mayor
			index = Math.floor(index)+1;
		}
		percValues[i+1] = values[index];
	}
	// poner en la ultima posición el valor máximo + 1
	percValues.push(values[values.length-1]+1);

	return percValues;
}

/**
 * devuelve un vector con la desviación estandar esablecida en el vector de
 * desviaciones para el conjunto de valores del vector values. En la primera
 * posición del resultado devuelve el valor mínimo (menos 1 por posibles
 * redondeos) del vector values y en la última el máximo (más 1 por posibles
 * redondeos)
 *
 * @param range
 *            vector con las desviaciones que hay que calcular
 * @param values
 *            vector con los valores sobre los que hay que calcular las
 *            desviacones
 * @returns {Array} vector con los valores de correspondientes a las
 *          desviaciones indicadas. Además en la primera posición se incluye el
 *          valor mínimo -1 y en la última el máximo +1
 */
function getStdDevRanges(values, num_classes) {
	var range =getBreaks(-100, 100, num_classes);
	// calcular la media de la serie
	avg = getAvg(values);
	// calcular la desviación típica de la serie
	stdDev = getStdDev(values, avg);

	// ordenar los valores de menor a mayor
	//values = values.sort(function(a,b) {return a-b;});

	// calcular los límites
	var stdDevValues = new Array();
	// poner en la primera posición el valor mínimo -1
	stdDevValues.push(values[0]-1);
	// calcular los límites correspondientes a las desviaciones establecidas en
	// el vector
	for (var i=0; i<range.length; i++) {
		stdDevValues[i+1] = Math.round(avg + ((stdDev * range[i] )/100));
	}
	// poner en la ultima posición el valor máximo + 1
	stdDevValues.push(values[values.length-1]+1);

	return stdDevValues;
}

/**
 * obtiene la media de la serie
 * @param values serie
 * @returns {Number} media
 */
function getAvg(values) {
	var avg = 0;
	for (var i = 0; i<values.length; i++) {
		avg = avg + values[i];
	}
	return avg / values.length;
}

/**
 * obtiene la desviación tipica de la serie
 * @param values serie
 * @param avg media de la serie. Si es nula, se calcula.
 * @returns desviación típica
 */
function getStdDev(values, avg) {
	if (avg == null) {
		avg = getAvg(values);
	}

	// calcular la desviación típica de la serie
	var stdDev = 0;
	for (var i = 0; i<values.length; i++) {
		stdDev = stdDev + Math.pow(values[i]-avg,2);
	}

	return Math.sqrt(stdDev /( values.length-1));
}

/**
 * obtiene los límtes de clases para la leyenda aplicacndo el algorítmo de rotura natural de Jenks
 * @param values serie
 * @returns {Array} vector con los límites de clases (en la primera posición se incluye el límite inferior de la primera clase, el resto
 * serán los límites superiores de cada clase).
 */
function getJenksBreaks( values, num_classes)
{
	var numdata = values.length;

	// ordenar la serie ascendentemente
//	values.sort(function(a,b) {return a-b;});

	// inicialización
	var pResult = new Array();
	for (var i = 0; i < num_classes; i++) {
		pResult.push(0);
	}

	var mat1 = new Array(); // servirá para guardar los índices en la serie de los límites de clases ?
	var mat2 = new Array(); // servirá para guardar la varianza de cada clase ?
	for (var i = 1; i <= num_classes; i++)
	{
		if (mat1[1]== null) {
			mat1[1] = new Array();
		}
		mat1[1][i] = 1;

		if (mat2[1]== null) {
			mat2[1] = new Array();
		}
		mat2[1][i] = 0;

		for (var j = 2; j <= numdata; j++)
		{
			if (mat1[j]== null)
				mat1[j] = new Array();
			if (mat2[j]== null)
				mat2[j] = new Array();
			mat2[j][i] = Infinity;
		}
	}
	// mat 1 queda con la fila 1 con todo 1 y mat2 con la fila 1 con todo 0
	// y el resto infinito


	var ssd = 0; //stdev
	for (var rangeEnd = 2; rangeEnd <= numdata; rangeEnd++)
	{
		var sumX = 0; // suma de los valores
		var sumX2 = 0; // suma de los cuadrados de los valores
		var w = 0;
		var dataId;
		for (var m = 1; m <= rangeEnd; m++)// recorro los anteriores
		{
			dataId = rangeEnd - m + 1;// orden inverso
			var val = values[dataId - 1];
			sumX2 += val * val;
			sumX += val;
			w++;
			ssd = sumX2 - (sumX * sumX) / w;
			for (var j = 2; j <= num_classes; j++)
			{
				// alert(dataId +" "+mat2[dataId - 1]);
				if (dataId >1) {
					if((mat2[rangeEnd][j] >= (ssd + mat2[dataId - 1][j - 1])))// incluyendo el valor actual, la varianza es menor
					{
						mat1[rangeEnd][j] = dataId;
						mat2[rangeEnd][j] = ssd + mat2[dataId - 1][j - 1];
					}
					/*else if (typeof mat1[rangeEnd][j]=='undefined') {
						mat1[rangeEnd][j] = (j==2?1:mat1[rangeEnd][j-1]);
					}*/
				}
			}
		}
		mat1[rangeEnd][1] = 1;
		/*for (var j = num_classes-1; j > 1; j--)
		{
			if (!mat1[rangeEnd][j]) {
				mat1[rangeEnd][j] = mat1[rangeEnd][j+1] ;
			}
		}*/
		mat2[rangeEnd][1] = ssd;
	}
	// mat1 contendrá en la última fila los índices del vector de valores
	// que deben representar los límites superiores de cada clase

	// en la primera posición ponemos el límite inferior de la primera clase
	// (menor valor -1 por posibles redondeos)
	pResult[0] = values[0]-1;
	// en la última posición ponemos el límite superior de la ultima clase
	// (mayor valor +1 por posibles redondeos)
	pResult[num_classes] = values[numdata - 1]+1;

	// establecer los límites superiores de cada clase (salvo la última que
	// ya está metido)
	var k = numdata;
	for (var j = num_classes; j >= 2; j--)
	{
		/*if (typeof mat1[k] =="undefined") {
			alert(k);
		}*/
		var id = Math.round((mat1[k][j]) - 2);
		if (id>=0) {
			pResult[j - 1] = values[id]+1; // sumamos 1 pq nosotros
		// consideramos el límite superior
		// no incluido en la clase
		}
		else{
			pResult[j - 1] = values[0]+1;
		}
		k = Math.round(mat1[k][ j] - 1);
		if (k==0) {
			k=1;
		}
	}
	return pResult;
}

function showPalette(id) {
	if ((id=='qualitative')||(id=="boolean")){
		$(".quantitativeSels").hide();
	}
	else{
		$(".quantitativeSels").show();
	}
	$("#colorsPalette img").css("display","none");
	$("#"+id).css("display","block");
	document.getElementById(id).src="images/"+id+"0.png";
	selectedColor(0);
}

function selectedColor(idx) {
	var dataNature = $("#colorNature")[0].value;

	var paletteColors = eval(dataNature.toUpperCase()+"_PALETTE");
	document.getElementById(dataNature).src="images/"+dataNature+idx+".png";
	palette = paletteColors[idx];
}



//////
//
function generateMapConfigNonNumeric() {
	var valueForColor = new Array();
	var informedForError = false;
	var nextIdxValueForColor = 0;

	rowsPerColor = new Object();
	var selectedFieldColumn = $("select[id='fieldToGenerateMapConfig'] option:selected").index();
	var typeToLink = $("#typeToLink").val();

	var legend = $("#styleMapConfig").val();
	var num_classes = Math.min(palette.length,grid.getDataLength());
	//$("#resultados *").remove();

	var alMenosHayUno = false;

	for (var i = 0; i < grid.getDataLength(); i++) {
		if ((grid.getDataItem(i)["column"+columnCount] > 0) || (grid.getDataItem(i)["column"+columnCount] == "S")) {
			var valorBuscado = getValueToFindNormalizado(i);

			var cuantosHay = cuantosHayDelTipo(valorBuscado, typeToLink);
			var valor = "";
			if (cuantosHay == 1) {
				alMenosHayUno = true;
					// buscar la posicion para este tipo
				var idx = -1;
				for (j in linkedPosibilities[valorBuscado]) {
					if (linkedPosibilities[valorBuscado][j]["type"] == typeToLink) {
						idx = j;
					}
				}

				if (idx != -1) {
					valor = (grid.getDataItem(i)["column" + selectedFieldColumn]);
				}
			} else if (cuantosHay > 1) {
				if (multipleOptionsSelection[i]) {
					valor = (grid.getDataItem(i)["column" + selectedFieldColumn]);
				}
			}
			if(($("#colorNature").val()=='boolean') && (valor.trim().length<=0)){
				console.log("Cadena vacía, no lo considero");
			}
			else{
			if (cuantosHay >= 1) {
				if (! valueForColor[valor]) {
					alMenosHayUno = true;
					var paletteColor = "";
					if (nextIdxValueForColor >= num_classes){
						nextIdxValueForColor=0;
					}
					//if (nextIdxValueForColor < num_classes) {
						paletteColor = palette[num_classes-1][nextIdxValueForColor];
						valueForColor[valor] = paletteColor;
						nextIdxValueForColor++;
					/*} else {
						if (! informedForError) {
							alert("Algunos valores no se muestran porque no hay suficientes colores disponibles");
							informedForError = true;
						}
						continue;
					}*/
				}
				paletteColor = valueForColor[valor];
				if (!rowsPerColor[paletteColor]) {
					rowsPerColor[paletteColor]=new Array();
				}
				rowsPerColor[paletteColor].push(i);
			}
			}
		}
	}
	return alMenosHayUno;
}
