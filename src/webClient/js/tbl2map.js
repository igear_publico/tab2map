﻿// en este array se guardan las consultas sin borrar
// por si pueden reusarse en el futuro
// para valores que se repitan en la misma tabla o en futuras nuevas tablas
var linkedPosibilities = [];
var numRows = 0;
//var invalidateRowPending = [];

// por si en el futuro se quiere poder excluir n primeras líneas
var firstRow = 0;

var isWorking = false;

function emparejarDatos() {
	var typeToLink = $("#typeToLink").val();
	if (solicitadoEmparejamiento == typeToLink) {
		// ya se había emparejado antes, ver si quiere aprovecharse de los valores ya emparejados
		var dialog= $('<div></div>')
			.html('<div class="ui-widget"><p>¿Desea reanudar el emparejamiento o volver a calcular todos los valores desde el inicio?</p></div>')
			.dialog({
				autoOpen: true,
				buttons: {
					Iniciar: function() {
						emparejarDatosConfirmado(false);
						$( this ).dialog( "close" );
						$( this ).dialog( "destroy" );
					},
					Reanudar: function() {
						emparejarDatosConfirmado(true);
						$( this ).dialog( "close" );
						$( this ).dialog( "destroy" );
					},
					Cancelar: function() {
						$( this ).dialog( "close" );
						$( this ).dialog( "destroy" );
					}
				},
				//width: 600,
				//height: 400,
				title: 'Reanudar emparejamiento',
				modal: true
			});
	} else {
		emparejarDatosConfirmado(false);
	}
}

function emparejarDatosConfirmado(preserveCache) {
	isWorking = true;
	pararEmparejamiento = false;
	numRows = grid.getDataLength();
	muestraLayerLoading();

	cuantosLlevo = 0;
	$("#cuantosLlevoEmparejadosTxt").html("");
	resetEmparejamiento(preserveCache);
	var typeToLink = $("#typeToLink").val();
	solicitadoEmparejamiento = typeToLink;
	emparejaUnDato(firstRow, numRows, typeToLink, true, true);
}

function updateRepeatedValues(typeToLink) {
	if (! isWorking) {
		var auxValues = [];
		for (var i = 0; i < numRows; i++) {
			var valorBuscado = getValueToFindNormalizado(i);
			if (auxValues[valorBuscado] != null) {
				updateOnlyOneColumnEmparejamientosFixedValue(i, -1, false);
			} else {
				auxValues[valorBuscado] = "0";
				updateOnlyOneColumnEmparejamientos(i, typeToLink, false);
			}
		}
	}
}

function emparejaUnDato(i, numRows, typeToLink, mustContinue, cuentaUnoMas) {
	var valorBuscado = getValueToFindNormalizado(i);
	var cacheado = false;
	if (valorBuscado != "") {
		// si ya la tengo, no vuelvo a consultar
		if (! linkedPosibilities[valorBuscado]) {
			var auxFunc = "updateOnlyOneColumnEmparejamientos(" + i + ",'" + typeToLink + "', " + cuentaUnoMas + ");showFlechaNext();";
			linkedPosibilities[valorBuscado] = [];
			//linkedPosibilities[valorBuscado]["types"] = [];

			switch (typeToLink) {
				case 'Municipio':
					if (! isNaN(valorBuscado)) {
						if (dMuniList[valorBuscado]) {
							cacheado = true;
							var currentCMuni = valorBuscado;
							saveData(valorBuscado, dMuniList[currentCMuni], typeToLink, objectidMuniList[currentCMuni]);
						}
					} else {
						if (cMuniList[valorBuscado.toUpperCase()]) {
							cacheado = true;
							var currentCMuni = cMuniList[valorBuscado.toUpperCase()];
							saveData(valorBuscado, cMuniList[currentCMuni], typeToLink, objectidMuniList[currentCMuni]);
						}
					}
					if (! cacheado) {
						doSpecialSearch(layerFieldList["Municipio"], valorBuscado, typeToLink, auxFunc);
					}
					break;
				case 'Comarca':
					if (! isNaN(valorBuscado)) {
						if (dComList[valorBuscado]) {
							cacheado = true;
							var currentCCom = valorBuscado;
							//saveData(valorBuscado, typeToLink, dComList[currentCCom], xminCom[currentCCom], yminCom[currentCCom], xmaxCom[currentCCom], ymaxCom[currentCCom], "OBJECTID", objectidComList[currentCCom]);
							saveData(valorBuscado, dComList[currentCCom], typeToLink, objectidComList[currentCCom]);
						}
					} else {
						if (cComList[valorBuscado.toUpperCase()]) {
							cacheado = true;
							var currentCCom = cComList[valorBuscado.toUpperCase()];
							saveData(valorBuscado, dComList[currentCCom], typeToLink, objectidComList[currentCCom]);
						}
					}
					if (! cacheado) {
						doSpecialSearch(layerFieldList["Comarca"], valorBuscado, typeToLink, auxFunc);
					}
					break;
				case 'Provincia':
					if (! isNaN(valorBuscado)) {
						if (dProvList[valorBuscado]) {
							cacheado = true;
							var currentCProv = valorBuscado;
							//saveData(valorBuscado, typeToLink, dComList[currentCCom], xminCom[currentCCom], yminCom[currentCCom], xmaxCom[currentCCom], ymaxCom[currentCCom], "OBJECTID", objectidComList[currentCCom]);
							saveData(valorBuscado, dProvList[currentCProv], typeToLink, objectidProvList[currentCProv]);
						}
					} else {
						if (cProvList[valorBuscado.toUpperCase()]) {
							cacheado = true;
							var currentCProv = cProvList[valorBuscado.toUpperCase()];
							saveData(valorBuscado, dProvList[currentCProv], typeToLink, objectidComList[currentCProv]);
						}
					}
					if (! cacheado) {
						doSpecialSearch(layerFieldList["Provincia"], valorBuscado, typeToLink, auxFunc);
					}
					break;
				case 'Localidad':
					if (! isNaN(valorBuscado)) {
						if (dLocaliList[valorBuscado]) {
							cacheado = true;
							var currentCLocali = valorBuscado;
							saveData(valorBuscado, dLocaliList[currentCLocali], typeToLink, objectidLocaliList[currentCLocali]);

						}
					} else {
						if (cLocaliList[valorBuscado.toUpperCase()]) {
							cacheado = true;
							var currentCLocali = cLocaliList[valorBuscado.toUpperCase()];
							saveData(valorBuscado, dLocaliList[currentCLocali], typeToLink, objectidLocaliList[currentCLocali]);

						}
					}
					if (! cacheado) {
						doSpecialSearch(layerFieldList["Localidad"], valorBuscado, typeToLink, auxFunc);
					}
					break;
				case 'Calle':

					if (! cacheado) {
						doSpecialSearch(layerFieldList["Calle"], valorBuscado, typeToLink, auxFunc);
					}
					break;

				case 'CuaUTM10k': doSpecialSearch(layerFieldList["CuaUTM10k"], valorBuscado, typeToLink, auxFunc); break;
				case 'Cuad5k':
					loadCache5k();
					if (! isNaN(valorBuscado)) {
						if (dH5List[valorBuscado]) {
							cacheado = true;
							var currentCH5 = valorBuscado;
							saveData(valorBuscado, dH5List[currentCH5], typeToLink, objectidH5List[currentCH5]);

						}
					}
					else {
						if (cH5List[valorBuscado.toUpperCase()]) {
							cacheado = true;
							var currentCH5 = cH5List[valorBuscado.toUpperCase()];
							saveData(valorBuscado, cH5List[currentCH5], typeToLink, objectidH5List[currentCH5]);

						}
					}
					if (! cacheado) {
						doSpecialSearch(layerFieldList["Cuad5k"], valorBuscado, typeToLink, auxFunc);
					}
					break;
				case 'Cuad50k':
					loadCache50k();
					if (! isNaN(valorBuscado)) {
						if (dH50List[valorBuscado]) {
							cacheado = true;
							var currentCH50 = valorBuscado;
							saveData(valorBuscado, dH50List[currentCH50], typeToLink, objectidH50List[currentCH50]);
						}
					}
					else {
						if (cH50List[valorBuscado.toUpperCase()]) {
							cacheado = true;
							var currentCH50 = cH50List[valorBuscado.toUpperCase()];
							saveData(valorBuscado, dH50List[currentCH50], typeToLink, objectidH50List[currentCH50]);
						}
					}
					if (! cacheado) {
						doSpecialSearch(layerFieldList["Cuad50k"], valorBuscado, typeToLink, auxFunc);
					}
					break;
				case 'Toponimo':
					var subtype = $("#patternToLink_" + typeToLink).val();
					var ine=null;
					if (subtype == 2) {
						var selColINE =  $("select[id='fieldToLink_" + typeToLink + "_" + subtype + "_2'] option:selected").index();
						ine = grid.getDataItem(i)["column" + selColINE];
					}

					buscarSOAP_tipo("Toponimo", valorBuscado,  auxFunc, ine); break;
				case 'BusParUrb':
					buscarSOAP_tipo("BusParUrb", valorBuscado,  auxFunc); break;
				case 'BusSIGPAC':
					buscarSOAP_tipo("BusSIGPAC", valorBuscado,  auxFunc); break;
				default:
					buscarSOAP(1, valorBuscado, true, auxFunc);
					break;
			}

			if (cacheado) {
				updateOnlyOneColumnEmparejamientos(i, typeToLink, cuentaUnoMas);
				showFlechaNext();
			}

			//} else {
			//linkedPosibilities[valorBuscado]["types"] = [];
		} else {
			cacheado = true;
			//updateOnlyOneColumnEmparejamientos(i, typeToLink, cuentaUnoMas);
		}
	} else {
		llevoUnoMas();
	}
	if (pararEmparejamiento) {
		isWorking = false;
		updateRepeatedValues(typeToLink);
		ocultaLayerLoading();
		return;
	} else {
		if (mustContinue) {
			if (i < (numRows -1)) {
				if (! cacheado) {
					// dar oportunidad de parar
					// doy latencia para que no se envíen todas de golpe
					// cada 30 emparejamientos, dejo un descanso
					var lag = 5;
					if ((i % 30) == 29) {
						lag = 100;
						console.log("Lag en " + i)
					}
					timeoutEmpareja = setTimeout("emparejaUnDato(" + (i+1) + ", " + numRows + ", '" + typeToLink + "', true, true)", lag);
				} else {
					emparejaUnDato((i+1), numRows, typeToLink, true, true);
				}
			}
		}
	}
}


/*
 function updateColumnaEmparejamientos() {
 var typeToLink = $("#typeToLink").val();

 for (var i = 0; i < numRows; i++) {
 updateOnlyOneColumnEmparejamientos(i, typeToLink, false);
 }
 }
 */

function llevoUnoMas() {
	if (cuantosLlevo < numRows) {
		cuantosLlevo++;
	}
	if (cuantosLlevo >= numRows) {
		var typeToLink = $("#typeToLink").val();
		isWorking = false;
		updateRepeatedValues(typeToLink);
		ocultaLayerLoading();
	}
	if (! pararEmparejamiento) {
		$("#cuantosLlevoEmparejadosTxt").html("Realizados " + cuantosLlevo + " de un total de " + numRows);
	}
}

function llevoUnoMenos() {
	if (cuantosLlevo > 0) {
		cuantosLlevo--;
	}
	if (! pararEmparejamiento) {
		$("#cuantosLlevoEmparejadosTxt").html("Realizados " + cuantosLlevo + " de un total de " + numRows);
	}
}

function updateOnlyOneColumnEmparejamientosFixedValue(i, cuantosHay, cuentaUnoMas) {
	if (cuentaUnoMas) {
		llevoUnoMas();
	}
	/*
	// invalidar y repintar toda la tabla es lento, si la tabla es grande.
	// lo guardo, y repinto cuando tengo unos cuantos
	var aux = [];
	aux['pos'] = i;
	aux['val'] = cuantosHay;
	invalidateRowPending.push(aux);

	if (invalidateRowPending.length >= 30) {
	flushPendingRows();
	}

	if (i == (numRows-1)) {
	flushPendingRows();
	}

	*/
	grid.invalidateRow(i);

	//grid.getData()[i][grid.getColumns()[columnCount].field] = cuantosHay;
	grid.getDataItem(i)["column" + columnCount] = cuantosHay;
	grid.render();
}

/*
 function flushPendingRows() {
 var datagrid = grid.getData();
 var columnsgrid = grid.getColumns();
 for (val in invalidateRowPending) {
 var item = invalidateRowPending[val];
 grid.invalidateRow(item['pos']);
 datagrid[item['pos']][columnsgrid[columnCount].field] = item['val'];
 }
 invalidateRowPending = [];
 grid.render();
 }
 */

function updateOnlyOneColumnEmparejamientos(i, typeToLink, cuentaUnoMas) {
	var valorBuscado = getValueToFindNormalizado(i);
	var cuantosHay = cuantosHayDelTipo(valorBuscado, typeToLink);
	updateOnlyOneColumnEmparejamientosFixedValue(i, cuantosHay, cuentaUnoMas);
}

function cuantosHayDelTipo(valor, tipo) {
	var cuantos = 0;
	for (i in linkedPosibilities[valor]) {
		if ((linkedPosibilities[valor][i]["type"] == tipo)||(linkedPosibilities[valor][i]["type"] == tipos[tipo])) {
			cuantos++;
		}
	}
	return cuantos;
}

function resetEmparejamiento(preserveCache) {
	cambiaColumnaEmparejamientoAVacia();
	$('#typeToLinkDiv').addClass("oculto");
	cuantosLlevo = 0;
	if (!preserveCache) {
		multipleOptionsSelection = [];
		linkedPosibilities = [];
		//invalidateRowPending = [];
	}
}

function cambiaColumnaEmparejamientoAVacia() {
	for (var i = 0; i < grid.getDataLength(); i++) {
		grid.invalidateRow(i);
		//grid.getData()[i][grid.getColumns()[columnCount].field] = '';
		grid.getDataItem(i)["column" + columnCount] = '';
		grid.render();
	}
}

var multipleOptionsSelection = [];

function solveMultiple(idxRow) {
	var optionsAvaliable = '';
	var valorBuscado = getValueToFindNormalizado(idxRow);

	var optionsAvailable = "<table class='ui-widget'>";
	var typeToLink = $("#typeToLink").val();

	for (var i=0; i < linkedPosibilities[valorBuscado].length; i++){
		var dato = linkedPosibilities[valorBuscado][i];
		if (( dato["type"] == typeToLink)||( dato["type"] == tipos[typeToLink]))  {
			//TODO: poner mejor el objectid o algo más identificativo
			optionsAvailable += '<tr><td><input type="radio" name="choiceItem" value="' + i + '" />' + dato["nombre"] + ((dato["municipio"]!=null) && (dato["municipio"].length>0)? ' ('+dato["municipio"]+')':'')+ '</td></tr>';
		}
	}

	var dialog= $('<div></div>')
		.html('<div class="ui-widget"><p>Seleccione un emparejamiento de entre los posibles:</p>' + optionsAvailable + '</div>')
		.dialog({
			autoOpen: true,
			buttons: {
				Aceptar: function() {
					var selectedValue = $('input:radio[name=choiceItem]:checked').val();
					multipleOptionsSelection[idxRow] = selectedValue;

					grid.invalidateRow(idxRow);
					//grid.getData()[idxRow][grid.getColumns()[columnCount].field] = "S";
					grid.getDataItem(idxRow)["column" + columnCount] = "S";
					grid.render();

					$( this ).dialog( "close" );
					$( this ).dialog( "destroy" );
				},
				Cancelar: function() {
					$( this ).dialog( "close" );
					$( this ).dialog( "destroy" );
				}
			},
			width: 600,
			height: 400,
			title: 'Múltiples candidatos',
			modal: true
		});
}

function loadJavaScriptSync(filePath) {
	var req = new XMLHttpRequest();
	req.open("GET", filePath, false); // 'false': synchronous.
	req.send(null);

	var headElement = document.getElementsByTagName("head")[0];
	var newScriptElement = document.createElement("script");
	newScriptElement.type = "text/javascript";
	newScriptElement.text = req.responseText;
	headElement.appendChild(newScriptElement);
}

var loadedCache5k = false;
var loadedCache50k = false;
function loadCache5k() {
	if (! loadedCache5k) {
		loadJavaScriptSync("dataCache/hoja5.js");
	}
}
function loadCache50k() {
	if (! loadedCache50k) {
		loadJavaScriptSync("dataCache/hoja50.js");
	}
}
