var selFieldList = new Array();
var hint = new Array();
var txtFieldList = new Array();
var layerFieldList = new Array();
var filterFieldList = new Array();
var prefixIdFieldList = new Array();
var codeFieldList = new Array();

function addHint(id, prefix, field, postfix) {
	if (! hint[id]) {
		hint[id] = [];
		hint[id]["count"] = 0;
	}
	var idx = hint[id]["count"];
	hint[id][idx] = [];
	hint[id][idx]["prefix"] = prefix;
	hint[id][idx]["field"] = field;
	hint[id][idx]["postfix"] = postfix;
	hint[id]["count"] = idx+1;
}

// Localidad
selFieldList["Localidad"]="D_NUCLEO_I C_NUCLEO_I D_MUNI_INE #SHAPE# #ID#";
codeFieldList["Localidad"]="C_NUCLEO_I";
txtFieldList["Localidad"]="D_NUCLEO_I";
layerFieldList["Localidad"]="Localidad_t2m";
filterFieldList["Localidad"]="";
prefixIdFieldList["Localidad"]="";
addHint("Localidad", "", "D_NUCLEO_I", "");
addHint("Localidad", "(", "D_MUNI_INE", ")");

// Provincia 
selFieldList["Provincia"]="ETIQUETA ID #SHAPE# #ID#";
codeFieldList["Provincia"]="ID";
txtFieldList["Provincia"]="ETIQUETA";
layerFieldList["Provincia"]="Provincia";
filterFieldList["Provincia"]="CARTO.T01_ESQUEMA_DESCARGA.ESQUEMA='provincia'";
prefixIdFieldList["Provincia"]="CARTO.T01_ESQUEMA_DESCARGA.";
addHint("Provincia", "", "ETIQUETA", "");

// Comarca 
selFieldList["Comarca"]="ETIQUETA ID #SHAPE# #ID#";
codeFieldList["Comarca"]="ID";
txtFieldList["Comarca"]="ETIQUETA";
layerFieldList["Comarca"]="Comarca_t2m";
filterFieldList["Comarca"]="CARTO.T01_ESQUEMA_DESCARGA.ESQUEMA='comarca'";
prefixIdFieldList["Comarca"]="CARTO.T01_ESQUEMA_DESCARGA.";
addHint("Comarca", "", "ETIQUETA", "");

// BusMun
selFieldList["BusMun"]="ETIQUETA ID #SHAPE# #ID#";
codeFieldList["BusMun"]="ID";
txtFieldList["BusMun"]="ETIQUETA";
layerFieldList["BusMun"]="ED";
filterFieldList["BusMun"]="CARTO.T01_ESQUEMA_DESCARGA.ESQUEMA='Municipio'";
prefixIdFieldList["BusMun"]="CARTO.T01_ESQUEMA_DESCARGA.";
addHint("BusMun", "", "ETIQUETA", "");

// Municipio
selFieldList["Municipio"]="ETIQUETA ID #SHAPE# #ID#";
codeFieldList["Municipio"]="ID";
txtFieldList["Municipio"]="ETIQUETA";
layerFieldList["Municipio"]="Municipio_t2m";
filterFieldList["Municipio"]="CARTO.T01_ESQUEMA_DESCARGA.ESQUEMA='Municipio'";
prefixIdFieldList["Municipio"]="CARTO.T01_ESQUEMA_DESCARGA.";
addHint("Municipio", "", "ETIQUETA", "");

// Cuadr�cula UTM 10K
selFieldList["CuaUTM10k"]="ETIQUETA ID #SHAPE# #ID#";
codeFieldList["CuaUTM10k"]="ID";
txtFieldList["CuaUTM10k"]="ETIQUETA";
layerFieldList["CuaUTM10k"]="CuaUTM10k_t2m";
filterFieldList["CuaUTM10k"]="CARTO.T01_ESQUEMA_DESCARGA.ESQUEMA='hojas_10'";
prefixIdFieldList["CuaUTM10k"]="CARTO.T01_ESQUEMA_DESCARGA.";
addHint("CuaUTM10k", "", "ETIQUETA", "");

// Cuadr�cula 5K
selFieldList["Cuad5k"]="ETIQUETA ID #SHAPE# #ID#";
codeFieldList["Cuad5k"]="ID";
txtFieldList["Cuad5k"]="ETIQUETA";
layerFieldList["Cuad5k"]="ED";
filterFieldList["Cuad5k"]="CARTO.T01_ESQUEMA_DESCARGA.ESQUEMA='hojas_5'";
prefixIdFieldList["Cuad5k"]="CARTO.T01_ESQUEMA_DESCARGA.";
addHint("Cuad5k", "", "ETIQUETA", "");

// Cuadr�cula 50K
selFieldList["Cuad50k"]="ETIQUETA ID #SHAPE# #ID#";
codeFieldList["Cuad50k"]="ID";
txtFieldList["Cuad50k"]="ETIQUETA";
layerFieldList["Cuad50k"]="ED";
filterFieldList["Cuad50k"]="CARTO.T01_ESQUEMA_DESCARGA.ESQUEMA='hoja50'";
prefixIdFieldList["Cuad50k"]="CARTO.T01_ESQUEMA_DESCARGA.";
addHint("Cuad50k", "", "ETIQUETA", "");


// BusParUrb 
selFieldList["BusParUrb"]="REFPAR #SHAPE# #ID#";
codeFieldList["BusParUrb"]="REFPAR";
txtFieldList["BusParUrb"]="REFPAR";
layerFieldList["BusParUrb"]="BusParUrb_t2m";
filterFieldList["BusParUrb"]="";
prefixIdFieldList["BusParUrb"]="";
addHint("BusParUrb", "", "REFPAR", "");

// BusSIGPAC 
selFieldList["BusSIGPAC"]="REFPAR #SHAPE# #ID#";
codeFieldList["BusSIGPAC"]="REFPAR";
txtFieldList["BusSIGPAC"]="REFPAR";
layerFieldList["BusSIGPAC"]="BusSIGPAC_t2m";
filterFieldList["BusSIGPAC"]="";
prefixIdFieldList["BusSIGPAC"]="";
addHint("BusSIGPAC", "", "REFPAR", "");

//Calle 
selFieldList["Calle"]="ETIQUETA #SHAPE# #ID#";
codeFieldList["Calle"]="ETIQUETA";
txtFieldList["Calle"]="ETIQUETA";
layerFieldList["Calle"]="BusCallUrb_t2m";
filterFieldList["Calle"]="";
prefixIdFieldList["Calle"]="";
addHint("Calle", "", "ETIQUETA", "");

//Portal
layerFieldList["Portal"]="TroidesV_t2m";
filterFieldList["Portal"]="";

//Coordenadas
layerFieldList["Coordenadas"]="";
filterFieldList["Coordenadas"]="";
