
var grid;
var data = [];
var dataView;
var page_size = 25;
var pagina_actual=1;
var num_results = 0;
/**
 * Elimina los acentos de una cadena
 * @param entry cadena de texto
 * @returns cadena de texto sin acentos
 */
/*function normalizaCadenaSimple(entry) {
		entry = replaceChar(entry, '�', 'a');
		entry = replaceChar(entry, '�', 'e');
		entry = replaceChar(entry, '�', 'i');
		entry = replaceChar(entry, '�', 'o');
		entry = replaceChar(entry, '�', 'u');
		entry = replaceChar(entry, '�', 'A');
		entry = replaceChar(entry, '�', 'E');
		entry = replaceChar(entry, '�', 'I');
		entry = replaceChar(entry, '�', 'O');
		entry = replaceChar(entry, '�', 'U');
		return entry;
	}*/

function inputKeyUp(e) {
    e.which = e.which || e.keyCode;
    if(e.which == 13) {
    	buscarSOAP(1);
    }
}

	/**
 * inicializa los botones de JQuery
 */
function initButtonsBuscador() {

	$("#pagSig").button({
		icons : {
			primary : "next-icon"
		}
	});

	$("#pagPrev").button({
		icons : {
			primary : "prev-icon"
		}
	});
		$("#pagUlt").button({
		icons : {
			primary : "last-icon"
		}
	});

	$("#pag1").button({
		icons : {
			primary : "first-icon"
		}
	});
	$("#buscarSOAP").button({
		icons : {
			primary : "search-icon"
		}
	});
}

/**
 * Elimina acentos y di�resis y sustituye � y �  por ny
 * @param entry cadena de texto en la que sustiuir
 * @returns cadena sin acentos, dieresis ni �/�
 */
function normalizaCadena(entry) {
	entry = encodeURI(entry);
	/*entry = normalizaCadenaSimple(entry);
		entry = replaceChar(entry, '�', 'u');
		entry = replaceChar(entry, '�', 'u');
		entry = replaceChar(entry, '�', 'ny');
		entry = replaceChar(entry, '�', 'ny');*/
	return entry;
}

function corrigeCaracteresExtranosBuscador(entry){
	entry=replaceChar(entry,'á','�');
	entry=replaceChar(entry,'é','�');
	entry=replaceChar(entry,'í','�');
	entry=replaceChar(entry,'ó','�');
	entry=replaceChar(entry,'ú','�');
	entry=replaceChar(entry,'�?','�');
	entry=replaceChar(entry,'�','�');
	entry=replaceChar(entry,'�?','�');
	entry=replaceChar(entry,'Ó','�');
	entry=replaceChar(entry,'�','�');
	entry=replaceChar(entry,'�?','�');
	entry=replaceChar(entry,'ä','�');
	entry=replaceChar(entry,'ë','�');
	entry=replaceChar(entry,'ï','�');
	entry=replaceChar(entry,'ü','�');
	entry=replaceChar(entry,'Ü','�');
	entry=replaceChar(entry,'ñ','�');
	entry=replaceChar(entry,'Ñ','�');
	entry=replaceChar(entry,'À','�');
	entry=replaceChar(entry,'� ','�');
	entry=replaceChar(entry,'è','�');
	entry=replaceChar(entry,'ò','�');
	entry=replaceChar(entry,'ç','�;');
	entry=replaceChar(entry,'Ç','�');
	entry=replaceChar(entry,'´','�');
	entry=replaceChar(entry,'’','&#39;');
	entry=replaceChar(entry,'‘','&#39;');
	entry=replaceChar(entry,'º','�');
	entry=replaceChar(entry,'ª','�');
	entry=replaceChar(entry,'¡','�');
	entry=replaceChar(entry,'º','�');
	
	entry=replaceChar(entry,'\\u00e1','�');
	entry=replaceChar(entry,'\\u00e9','�');
	entry=replaceChar(entry,'\\u00ed','�');
	entry=replaceChar(entry,'\\u00f3','�');
entry=replaceChar(entry,'\\u00fa','�');
	entry=replaceChar(entry,'\\u00c1','�');
	entry=replaceChar(entry,'\\u00c9','�');
	entry=replaceChar(entry,'\\u00cd','�');
		entry=replaceChar(entry,'\\u00d3','�');
	entry=replaceChar(entry,'\\u00da','�');
	entry=replaceChar(entry,'\\u00f1','�');
entry=replaceChar(entry,'\\u00d1','�');
entry=replaceChar(entry,'\\u00FC','�');
entry=replaceChar(entry,'\\u00DC','�');


	return entry;
	}/**
 * Devuelve una cadena de texto cambiando una subcadena por otra (todas las ocurrencias)
 * @param entry cadena de texto en la que hacer el reemplazo
 * @param oldCh caracter a sustituir
 * @param newCh caracter de sustituci�n
 * @returns cadena con los caracteres sustituidos
 */
function replaceChar(entry, oldCh, newCh) {
		var result = entry;
		var posAnt = -1;
		var posActual = result.indexOf(oldCh);

		while ((posActual != -1) && (posAnt != posActual)) {
			result = result.replace(oldCh, newCh);
			posAnt = posActual;
			posActual = result.indexOf(oldCh);
		}

		return result;
	}

function buscarSOAP_pre(){
	buscarSOAP(pagina_actual -1);
}

function buscarSOAP_sig(){
	buscarSOAP(pagina_actual +1);
}

function buscarSOAP_ult(){
	ult_pagina = Math.ceil(num_results/page_size);
	if (ult_pagina>pagina_actual){
		buscarSOAP(ult_pagina);
	}
	else{
		//alert("Ya est� en la �ltima p�gina");
	}
}

var ultimoValorBuscado = "";
function buscarSOAP_url(pagina, valor, asyncro, funcCallBack, urlService){

	showSearching(true);
	inicio = (pagina-1) * page_size + 1;

	//buscarGooglePlaces(document.getElementById("texto").value);
	//buscarGoogleMaps(document.getElementById("texto").value);
	var url = '';
	if (valor) {
		url = urlService + '?texto='+  normalizaCadena(valor) +"&inicio="+inicio+"&total="+page_size+"&app="+APP_NAME;
		ultimoValorBuscado = valor;
	} else {
		url = urlService + '?texto='+normalizaCadena(document.getElementById("texto").value)+"&inicio="+inicio+"&total="+page_size+"&app="+APP_NAME;
		ultimoValorBuscado = document.getElementById("texto").value;
	}

	pagina_actual = pagina;
	$.ajax({
		url: url,
		type: 'GET',
		//success:  function(data) { parseDataBotones($.trim(data)); },
		success:  function(data) { /*try{*/
						addTableData(data, valor);
						if (funcCallBack) {
							eval(funcCallBack);
						}
						/*updatePages();*/ /*}
		catch(err){
			showSearching(false);
			alert("No se pudo completar la b�squeda "+err);
		}*/},
		error:  function() { }, // alert("error "+url);},
		complete: function(xmlHttpRequest) { 
			showSearching(false);},
		processData: false,
		contentType: "text/xml; charset=\"utf-8\"",
		async: asyncro//,
		// timeout: timeoutvalue-200
	});
}	


function buscarSOAP(pagina, valor, asyncro, funcCallBack){
	buscarSOAP_url(pagina, valor, asyncro, funcCallBack, '/SimpleSearchService/geonamesSearchService');
}

function updatePages(){
	numPags = Math.ceil(num_results/page_size);
	document.getElementById("pags").innerHTML="P&aacute;gina "+pagina_actual+" de "+numPags;
	$( "#pag1" ).button( "option", "disabled", pagina_actual==1 );
	$( "#pagPrev" ).button( "option", "disabled", pagina_actual==1 );
	$( "#pagSig" ).button( "option", "disabled", pagina_actual==numPags);
	$( "#pagUlt" ).button( "option", "disabled", pagina_actual==numPags );

}


function getObjectIds(results){
	var ids=new Array();
	var strToFind = ".OBJECTID=";
	var count=0;
	var pos = results.indexOf(strToFind, 1);
	//alert(results);
	while (pos != -1) {
		var startpos = pos + strToFind.length;
		var endpos = results.indexOf('"',startpos+1);
		ids[count]= results.substring(startpos+1,endpos);
		pos = results.indexOf(strToFind, endpos);
		count++;
	}
	return ids;	
}

function getObjectField(results, field){
	var end='"';
	if (field.indexOf("<")>=0){

		end = field.replace("<","</");

	}
	else if (field.indexOf("\":\"")>=0){

		end='","';

	}

	var ids=new Array();
	var strToFind = field;
	var count=0;
	var pos = results.indexOf(strToFind, 1);
	//alert(results);
	while (pos != -1) {
		var startpos = pos + strToFind.length;
		var endpos = results.indexOf(end,startpos+1);
		ids[count]= results.substring(startpos,endpos);
		pos = results.indexOf(strToFind, endpos);
		count++;
	}
	return ids;	
}



function getType(type){
return tipos[type];
}

function getNameField(type){
	switch(type){
	case "BusCallUrb": 
	case "TroidesV": return ".VIA_LOC=\"";
	break;
	case "nombreExacto": 
	case "nombreTodas":
	case "nombreAlguna":return "<app:nombre>";
	break;
	case "Nominatim": return "\"display_name\":\"";

	case "Geonames":return "\"toponymName\":\"";
	break;
	case "BusMun": return ".D_MUNI_INE=\"";
	break;
	case "Cuad5k": return ".HOJA50_H5=\"";
	break;
	case "Cuad50k": return ".NAME_HOJA=\"";
	break;
	case "CuaUTM10k": return ".CODIGO=\"";
	break;
	case "ParRus":
	case "BusParUrb":
	case "BusSIGPAC": 
	case "RegVic94":return ".REFPAR=\"";
	break;
	case "Localidad": return ".D_NUCLEO_I=\"";
	break;
	case "Comarca": return ".D_COMARCA=\"";
	break;
	case "CatMin": return ".NOMBRE=\"";
	break;
	case "ENP": 
	case "HMD":
	case "LICS":
	case "ZEPAS":
	case "PORN":
	case "ZECS":return ".DESCRIPCIO=\"";
	break;
	case "VRTCC1":return ".NOMBRE=\"";
	break;
	case "MONTES":return ".DENOMINACION=\"";
	break;

	default: return type;
	}
}


function showSearching(show){
				/*
	if (show)
		document.getElementById("searchingOverlay").style.visibility="visible";
	else
		document.getElementById("searchingOverlay").style.visibility="hidden";
		*/
}



function verFormatter(row, cell, value, columnDef, dataContext){

	return seeIcon(value,'verMini.png');
}


/*function mdFormatter(row, cell, value, columnDef, dataContext){

			 return (value.indexOf(".xml")>=0 ? "<a href="+value+"><img  src='images/icon_layerMD.gif'></a>" : "");
	}

	function descargarFormatter(row, cell, value, columnDef, dataContext){

		resultado = "";
		if (value.indexOf(".pdf")>=0){
			resultado = resultado +"<a href=\"#\"><img class=\"icono_descarga\" src='images/PDF.png'></a>";
		}
		if (value.indexOf(".dgn")>=0){
			resultado = resultado +"<a href=\"#\"><img class=\"icono_descarga\" src='images/DGN.png'></a>";
		}
		if (value.indexOf(".dxf")>=0){
			resultado = resultado +"<a href=\"#\"><img class=\"icono_descarga\" src='images/DXF.jpg'></a>";
		}
		if (value.indexOf(".gml")>=0){
			resultado = resultado +"<a href=\"#\"><img class=\"icono_descarga\" src='images/GML.png'></a>";
		}
		if (value.indexOf(".jpg")>=0){
			resultado = resultado +"<a href=\"#\"><img class=\"icono_descarga\" src='images/JPG.png'></a>";
		}
		if (value.indexOf(".kmz")>=0){
			resultado = resultado +"<a href=\"#\"><img class=\"icono_descarga\" src='images/KMZ.png'></a>";
		}
		if (value.indexOf(".shp")>=0){
			resultado = resultado +"<a href=\"#\"><img class=\"icono_descarga\" src='images/SHP.png'></a>";
		}
		if (value.indexOf(".ecw")>=0){
			resultado = resultado +"<a href=\"#\"><img class=\"icono_descarga\" src='images/ECW.png'></a>";
		}
		//alert(resultado);
		return resultado;
	}*/

function seeIcon(value, icon, blank){

	return (value != null && value.length>0 ? "<a href="+value+"  ><img  src='"+IMAGES_PATH+"/"+icon+"'></a>" : "");

}


/*function setTableWidth(){
	//alert($("#mdPanel").width());
		//alert($("#toolbar").width()+"-"+$("#tools").width()+"-"+(withScroll ? 50 : -1)+"="+$("#tabla").width());
		$("#tabla").css('width', $("#mdPanel").width());
	//	alert($("#mdPanel").width());
		//alert($("#toolbar").width()+"-"+$("#tools").width()+"-"+(withScroll ? 50 : -1)+"="+$("#tabla").width());
		//alert($("#tabla").width());
	}
 */
/*function resizeTable(){
		switch ($("#tabs").tabs("option", "active")) {
		case 0:// tabla
			// ajustar el mapa
			//resizeMap();
			// programar que se ajuste la tabla cuando se active su pesta�a
			//$("#tabs").tabs("option", "activate", onActivateTable);
			$("#tabs").tabs("option", "active", 1);
			setTableWidth();
			$("#tabs").tabs("option", "active", 0);
			grid.resizeCanvas();
			break;
		case 1: // md
			setTableWidth();
			// ajustar el tam�o de la tabla
			//resizeTable();
			// programar que se ajuste el tama�o del mapa cuando se active su
			// pesta�a
			//$("#tabs").tabs("option", "activate", onActivateMap);
		}
		setTableColumnWidth();
	}*/
/*	function setTableColumnWidth(){
		ancho_total =  $("#tabla").width();//-$("#tools").width()-15;
		//alert($("#tabla").width());
		ancho_nombre = ancho_total-70-77-80-40-80-80;

		var data = grid.getColumns();
		data[grid.getColumnIndex("nombre")].width = ancho_nombre-17;
		grid.setColumns(data);
	}*/



function showMuniCatastrales(){
    $( "#muniCList" ).dialog( "open" );
    }
function initBuscador(divId, pageSize){

	$("#"+divId).append("<div id=\"resultados_tabla\"></div>");
	$("#"+divId).append("<div id=\"resultados_pag\"></div>");
	$("#resultados_pag").append("<button class=\"pag_button\" id=\"pag1\" disabled onclick=\"buscarSOAP(1)\"></button>");
	$("#resultados_pag").append("<button class=\"pag_button\"id=\"pagPrev\" disabled onclick=\"buscarSOAP_pre()\"></button>");
	$("#resultados_pag").append("<span id=\"pags\">P&aacute;gina 0 de 0</span>");
	$("#resultados_pag").append("<button class=\"pag_button\" id=\"pagSig\" disabled onclick=\"buscarSOAP_sig()\"></button>");
	$("#resultados_pag").append("<button class=\"pag_button\" id=\"pagUlt\" disabled onclick=\"buscarSOAP_ult()\"></button>");
	initButtonsBuscador();
	page_size = pageSize;
	var columns = [
	               {id: "nombre", name: "Nombre", field: "nombre",minWidth: 80, sortable:false},
	              {id: "municipio", name: "Municipio", field: "municipio",minWidth: 80, sortable:false},
	               {id: "tipo", name: "Tipo", field: "tipo",minWidth: 80, maxWidth: 150,sortable:false}];
	if (typeof VER_JS_FUNCTION !='undefined'){
        columns.push(              {id: "ver", name: "Ver", field: "ver",minWidth: VER_COLUMN_WIDTH,width:VER_COLUMN_WIDTH,maxWidth: VER_COLUMN_WIDTH, sortable:false, formatter:verFormatter, cssClass:"centered"});
	}
	if (typeof URL_VISOR !='undefined'){
	        columns.push({id: "visor", name: "Visor 2D", field: "visor",minWidth: VISOR_COLUMN_WIDTH,width:VISOR_COLUMN_WIDTH, maxWidth: VISOR_COLUMN_WIDTH, sortable:false, formatter:verFormatter, cssClass:"centered"});
	}
	if (typeof URL_CARTOTECA !='undefined'){
        columns.push( {id: "cartoteca", name: "Cartoteca", field: "cartoteca",minWidth: CARTOTECA_COLUMN_WIDTH,width:CARTOTECA_COLUMN_WIDTH,maxWidth: CARTOTECA_COLUMN_WIDTH, sortable:false, formatter:verFormatter, cssClass:"centered"});
	}
	if (typeof URL_DESCARGAS !='undefined'){
        columns.push(              {id: "descargas", name: "Descargas", field: "descargas",minWidth: DESCARGAS_COLUMN_WIDTH,width:DESCARGAS_COLUMN_WIDTH,maxWidth: DESCARGAS_COLUMN_WIDTH, sortable:false, formatter:verFormatter, cssClass:"centered"});
	}

	             

	var options = {
			editable: false,
			enableCellNavigation: true,
			enableColumnReorder: true,
			forceFitColumns:true
	};



	$(function () {



		dataView = new Slick.Data.DataView();

		grid = new Slick.Grid("#resultados_tabla", dataView, columns, options);
		//  setTableColumnWidth();
		grid.resizeCanvas();






		/* dataView.onRowsChanged.subscribe(function(e,args) {
		    grid.invalidateRows(args.rows);
		    grid.render();
		});*/
		//  var pager = new Slick.Controls.Pager(dataView, grid, $("#pager"));
		grid.onSort.subscribe(function (e, args) {
			var comparer = function(a, b) {
				return a[args.sortCol.field] > b[args.sortCol.field];
			};

			// Delegate the sorting to DataView.
			// This will fire the change events and update the grid.
			//  alert( dataView.getItem(1)["id"]);
			view = grid.getData();

			view.sort(comparer, args.sortAsc);

			//	 alert( dataView.getItem(1)["id"]);

		});

		grid.render();
		var url = window.location.href;
		index = url.indexOf("q=");
		if (index >0){ // se ha indicado un mapa por par�metro
			// obtener el identificador del mapa
			document.getElementById("texto").value = unescape(url.substring(index+2, url.length));
			buscarSOAP(1);
		}
	});




}

function addTableData(datos, ultimoValorBuscado){
	data = [];
	var json = $.xmlToJSON(datos);
	num_results = 0;
	//alert(json.Body[0].searchResult[0].type[0].Text);
	var pos=0;
	if((json == null)||(typeof json.Body[0].SearchResponse[0].SearchResult=='undefined')){
		
		return;
	}
	for ( var j = 0; j < json.Body[0].SearchResponse[0].SearchResult.length; j++) {
		var busquedaLine = json.Body[0].SearchResponse[0].SearchResult[j];
		var resultados ="";
		if (busquedaLine.Count[0].Text!="0"){
			num_results = num_results+parseInt(busquedaLine.Count[0].Text);
			if(typeof busquedaLine.List != 'undefined'){
				resultados = busquedaLine.List[0].Text;


				tipo = busquedaLine.Type[0].Text;
				
				if ((tipo=="nombreExacto")||(tipo== "nombreTodas")||(tipo=="nombreAlguna")){
					var lineas=resultados.split("\n");
					for (var i=0;i<lineas.length;i++){
						valores = lineas[i].split("#");
						var d = (data[pos] = {});

						d["id"] = "id_" + j+"_"+i;
						d["nombre"] = valores[0] ;
						d["municipio"] = valores[1] ;
						d["tipo"] = getType(tipo) ;
						var coords = valores[2].split(":");
						if (coords.length ==4){
							bbox = ((parseInt(coords[0])+parseInt(coords[2]))/2)+"\',\'"+((parseInt(coords[1])+parseInt(coords[3]))/2);
						}
						else if (coords.length==2){
							bbox = parseInt(coords[0])+"\',\'"+parseInt(coords[1]);
						}
						else{
						bbox="";
						}
						if (bbox!=""){
							if (typeof URL_VISOR !='undefined'){
							d["visor"] = URL_VISOR+"BOX="+bbox+" target=\"_blank\"";
							}
							if (typeof URL_CARTOTECA !='undefined'){
							d["cartoteca"] = URL_CARTOTECA+"BOX="+bbox+" target=\"_blank\"";
							}
							if (typeof VER_JS_FUNCTION !='undefined'){
								//bbox = "BOX="+bbox;
								d["ver"] = "javascript:"+VER_JS_FUNCTION+"(\'"+bbox+"\') ";
							}

							// dportoles
							if (coords.length ==4) {
								saveData(ultimoValorBuscado, d["tipo"], d["nombre"], parseInt(coords[0]), parseInt(coords[1]),  parseInt(coords[2]),  parseInt(coords[3]), '', '');
							} else if (coords.length==2) {
								saveData(ultimoValorBuscado, d["tipo"], d["nombre"], parseInt(coords[0]), parseInt(coords[1]),  parseInt(coords[0]),  parseInt(coords[1]), '', '');
							}
							// fin dportoles
						}

						pos++;
					}
					
				}
				else if (tipo.indexOf("Coordenadas")==0){
					var d = (data[pos] = {});
					var partes = resultados.split("#");
					d["id"] = "id_" + j+"_"+i;
					d["nombre"] = corrigeCaracteresExtranosBuscador(partes[0]) ;
					d["municipio"] = "" ;
					d["tipo"] = tipo ;
					var coords = partes[1].split(":");
					bbox = (parseInt(coords[0])-1000)+":"+(parseInt(coords[1])-1000)+":"+(parseInt(coords[0])+1000)+":"+(parseInt(coords[1])+1000)+
					"&MARKERX="+coords[0]+"&MARKERY="+coords[1];
					
					if (bbox!=""){
						if (typeof VER_JS_FUNCTION !='undefined'){
							bbox = "BOX="+bbox;
							d["ver"] = "javascript:"+VER_JS_FUNCTION+"(\""+bbox+"\")";
						}
						if (typeof URL_VISOR !='undefined'){
							d["visor"] = URL_VISOR+"BOX="+bbox+" target=\"_blank\"";
						}
						if (typeof URL_CARTOTECA !='undefined'){
							d["cartoteca"] = URL_CARTOTECA+"BOX="+bbox+" target=\"_blank\"";
						}

						// dportoles
						saveData(ultimoValorBuscado, "Coordenadas", d["nombre"], parseInt(coords[0]), parseInt(coords[1]),  parseInt(coords[0]),  parseInt(coords[1]), '', '');
						//saveData(ultimoValorBuscado, d["tipo"], d["nombre"], parseInt(coords[0]), parseInt(coords[1]),  parseInt(coords[0]),  parseInt(coords[1]), '', '');
						// fin dportoles						
					}
					pos++;
				}
				else{
					
					ids = getObjectField(resultados, ".OBJECTID=\"");
					ids1 = getObjectField(resultados, ".OBJECTID_1=\"");
					munis = getObjectField(resultados, ".C_MUNI_INE=\"");
					if (tipo.indexOf("TroidesV")>=0){
						nombres = getObjectField(resultados, getNameField("TroidesV"));
						id_calle= getObjectField(resultados,".C_MUN_VIA=\"");
					}
					else{
						
						nombres = getObjectField(resultados, getNameField(tipo));
					
					}
					var minx= getObjectField(resultados,"minx=\"");
					var miny= getObjectField(resultados,"miny=\"");
					var maxx=getObjectField(resultados,"maxx=\"");
					var 	maxy=getObjectField(resultados,"maxy=\"");
					if (tipo=="Nominatim"){
						 minx= getObjectField(resultados,"\"lat\":\"");
						 miny= getObjectField(resultados,"\"lon\":\"");
						
					}
					else if (tipo=="Geonames"){
						 minx= getObjectField(resultados,"\"lat\":\"");
						 miny= getObjectField(resultados,"\"lng\":\"");
						
					}
					for (var i = 0; i < nombres.length; i++) {
						bbox = "BOX="+(parseInt(minx[i])-100)+":"+  (parseInt(miny[i])-100)+":"+  (parseInt(maxx[i])+100)+":"+ ( parseInt(maxy[i])+100);
						
					 if (ids[i]!=null){
							params="ACTIVELAYER="+tipo+"&QUERY=OBJECTID="+ids[i];
							if ((tipo=="HMD")||(tipo=="PORN")||(tipo=="LICS")||(tipo=="ZEPAS")||(tipo=="ZECS")||(tipo=="ENP")||(tipo=="VRTCC1")||(tipo=="MONTES")){
								params = params+"&QUERYSERVICE=IDEAragon_inagis";
							}
						}
						else if(ids1[i]!=null){
							params="ACTIVELAYER="+tipo+"&QUERY=OBJECTID_1="+ids1[i];
							if ((tipo=="HMD")||(tipo=="PORN")||(tipo=="LICS")||(tipo=="ZEPAS")||(tipo=="ZECS")||(tipo=="ENP")||(tipo=="VRTCC1")||(tipo=="MONTES")){
								params = params+"&QUERYSERVICE=IDEAragon_inagis";
							}
						}
						else{
							
							params = bbox;
						}
						var d = (data[pos] = {});

						d["id"] = "id_" + j+"_"+i;
						
						d["nombre"] = corrigeCaracteresExtranosBuscador(nombres[i]) ;
						d["municipio"] =(munis.length >i ? corrigeCaracteresExtranosBuscador(munis[i]) :"");
						
						if (tipo.indexOf("TroidesV")>=0){
							bbox = (parseInt(minx[i])-100)+":"+  (parseInt(miny[i])-100)+":"+  (parseInt(maxx[i])+100)+":"+ ( parseInt(maxy[i])+100);
							d["tipo"] = getType("TroidesV") ;
							portal = tipo.substr(tipo.indexOf("_")+1);
							if (typeof URL_VISOR !='undefined'){
							d["visor"] ="javascript:locatePortal(\""+id_calle[i]+"\","+portal+",\""+bbox+"\",true);";
							}
							if (typeof URL_CARTOTECA !='undefined'){
							d["cartoteca"] ="javascript:locatePortal(\""+id_calle[i]+"\","+portal+",\""+bbox+"\",false);";
							}
							if (typeof VER_JS_FUNCTION !='undefined'){
								d["ver"] ="javascript:"+VER_PORTAL_JS_FUNCTION+"(\""+id_calle[i]+"\","+portal+",\""+bbox+"\");";
							}
						}
						else{ 
							if ((tipo=="Nominatim")||(tipo=="Geonames")){
								params = transformCoord(minx[i],miny[i]);
								
							}
							d["tipo"] = getType(tipo) ;
							
							if ((params.indexOf("undefined")<0)&&(params.indexOf("NaN:NaN:NaN:NaN")<0)){
								if (typeof URL_VISOR !='undefined'){
									d["visor"] = URL_VISOR+params+" target=\"_blank\"";
								}
								if (typeof URL_CARTOTECA !='undefined'){
									d["cartoteca"] = URL_CARTOTECA+params+" target=\"_blank\"";
								}
								if (typeof VER_JS_FUNCTION !='undefined'){
									if (params.indexOf("BOX=")<0){
									params = params+"&"+bbox;
									}
									d["ver"] = "javascript:"+VER_JS_FUNCTION+"(\""+params+"\")";
								}								
							}
							if(tipo=="BusMun"){
								if (typeof URL_DESCARGAS !='undefined'){
								d["descargas"] = encodeURI(URL_DESCARGAS.replace("<<TEXT>>",d["nombre"]))+" target=\"_blank\"";
								}
							}

						}
						// dportoles
						var idField = '';
						var idValue = '';
									
					 	if (ids[i]!=null){
							idField = "OBJECTID";
							idValue = ids[i];
						} else if(ids1[i]!=null){
							idField = "OBJECTID_1";
							idValue = ids1[i];
						}
						saveData(ultimoValorBuscado, d["tipo"], d["nombre"], parseInt(minx[i]), parseInt(miny[i]),  parseInt(maxx[i]),  parseInt(maxy[i]), idField, idValue);
						// fin dportoles
						pos++;
					}
				}

				
			}

		}
	}
	
	dataView.beginUpdate();
	dataView.setItems(data);
	dataView.endUpdate();
	grid.updateRowCount();	
	grid.invalidate();
	grid.render();
	if (num_results==1){
		document.getElementById("info").innerHTML="1 resultado encontrado";
	}
	else if (num_results>0)
		document.getElementById("info").innerHTML=num_results+" resultados encontrados";
	else
		document.getElementById("info").innerHTML="No se encontraron resultados para la b�squeda realizada";
}

function locatePortal(calle,portal,bbox_calle,visor){
	$.ajax({
		url: ARCIMS_QUERY,
		type: 'POST',
		data:"<ARCXML version=\"1.1\">"+
		"<REQUEST>"+
		"<GET_FEATURES geometry=\"false\" outputmode=\"xml\" checkesc=\"true\" envelope=\"true\" skipfeatures=\"false\" beginrecord=\"1\" featurelimit=\"1\">"+
		"<LAYER id=\"TroidesV\"/>"+
		"<SPATIALQUERY subfields=\"NUMERO #SHAPE# #ID#\" where=\"C_MUN_VIA='"+calle+"' AND NUMERO ="+portal+"\"/>"+
		"</GET_FEATURES>"+
		"</REQUEST>"+
		"</ARCXML>",
		//async:false,
		success:  function(data){showPortal(data,bbox_calle,visor);},
		error:  function(  jqXHR,  textStatus,  errorThrown) { }, //alert("Error al obtener el portal "+textStatus +"\n "+errorThrown);},
		 contentType: "text/xml",
		    dataType: "text"
		
	});	
}

function showPortal(data,bbox_calle,visor){
	count =getObjectField(data,"count=\"");
	var bbox = bbox_calle;
	if (count[0]!="0"){
		var ids= getObjectField(data,"OBJECTID=\"");
			
		var minx= getObjectField(data,"minx=\"");
		var miny= getObjectField(data,"miny=\"");
		var maxx=getObjectField(data,"maxx=\"");
		var maxy=getObjectField(data,"maxy=\"");
		/*if (ids[0]!=null){
			params="ACTIVELAYER=TroidesV&QUERY=OBJECTID="+ids[0];
		}
		
		else{*/
			
			params = "BOX="+(parseInt(minx[0])-100)+":"+  (parseInt(miny[0])-100)+":"+  (parseInt(maxx[0])+100)+":"+ ( parseInt(maxy[0])+100)+
			"&MARKERX="+minx[0]+"&MARKERY="+miny[0];
		//}
		
		
	}
	else{
		//alert("No se localiza el portal. Se muestra la calle completa");
	}
	if (visor){
		window.open(URL_VISOR+params);
	}
	else{
		window.open(URL_CARTOTECA+params);
	}
}
function transformCoord(lat,lon){
	
	destino = coordTransform("EPSG:4326", "EPSG:25830", lon, lat);
	xWCTS = destino[0];
	yWCTS = destino[1];

	return  "BOX="+(parseInt(xWCTS)-1000)+":"+  (parseInt(yWCTS)-1000)+":"+  (parseInt(xWCTS)+1000)+":"+ ( parseInt(yWCTS)+1000)+"&MARKERX="+parseInt(xWCTS)+"&MARKERY="+parseInt(yWCTS);


}
Proj4js.defs["EPSG:4326"]="+title=Geograficas WGS84 +proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs ";
Proj4js.defs["EPSG:25830"]="+title=25830 +proj=utm +zone=30 +ellps=GRS80 +units=m +no_defs ";

function coordTransform(srsOrigen, srsDestino, coordX, coordY) {
	destino = new Array();
	if (srsOrigen == srsDestino) {
		destino[0] = coordX;
		destino[1] = coordY;
		return destino;
	}
	else {
		var source = new Proj4js.Proj(srsOrigen);    //source coordinates
		var dest = new Proj4js.Proj(srsDestino);     //destination coordinates 

			// transforming point coordinates
		var p = new Proj4js.Point(coordX,coordY);   //any object will do as long as it has 'x' and 'y' properties
		Proj4js.transform(source, dest, p);      //do the transformation.  x and y are modified in place

		destino[0] = p.x;
		destino[1] = p.y;
		return destino;
	}
}

function increaseTypeCount(type) {
	if (possibleTypes[type]) {
		possibleTypes[type] = possibleTypes[type]+1;
	} else {
		possibleTypes[type] = 1;
	}
}

function saveData(ultimoValorBuscado, _tipo, _nombre, _minx, _miny, _maxx, _maxy, _idField, _idValue) {
}
