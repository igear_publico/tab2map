var grid;
var data = [];
var dataView;
var page_size = 25;
var pagina_actual=1;
var num_results = 0;
/**
 * Elimina los acentos de una cadena
 * @param entry cadena de texto
 * @returns cadena de texto sin acentos
 */
/*function normalizaCadenaSimple(entry) {
 entry = replaceChar(entry, 'á', 'a');
 entry = replaceChar(entry, 'é', 'e');
 entry = replaceChar(entry, 'í', 'i');
 entry = replaceChar(entry, 'ó', 'o');
 entry = replaceChar(entry, 'ú', 'u');
 entry = replaceChar(entry, 'Á', 'A');
 entry = replaceChar(entry, 'É', 'E');
 entry = replaceChar(entry, 'Í', 'I');
 entry = replaceChar(entry, 'Ó', 'O');
 entry = replaceChar(entry, 'Ú', 'U');
 return entry;
 }*/

function inputKeyUp(e) {
    e.which = e.which || e.keyCode;
    if(e.which == 13) {
        buscarSOAP(1);
    }
}

/**
 * inicializa los botones de JQuery
 */
function initButtonsBuscador() {

    $("#pagSig").button({
        icons : {
            primary : "next-icon"
        }
    });

    $("#pagPrev").button({
        icons : {
            primary : "prev-icon"
        }
    });
    $("#pagUlt").button({
        icons : {
            primary : "last-icon"
        }
    });

    $("#pag1").button({
        icons : {
            primary : "first-icon"
        }
    });
    $("#buscarSOAP").button({
        icons : {
            primary : "search-icon"
        }
    });
}

/**
 * Elimina acentos y diéresis y sustituye ń y Ń  por ny
 * @param entry cadena de texto en la que sustiuir
 * @returns cadena sin acentos, dieresis ni ń/Ń
 */
function normalizaCadena(entry) {
    entry = encodeURI(entry);
    /*entry = normalizaCadenaSimple(entry);
     entry = replaceChar(entry, 'ü', 'u');
     entry = replaceChar(entry, 'Ü', 'u');
     entry = replaceChar(entry, 'ń', 'ny');
     entry = replaceChar(entry, 'Ń', 'ny');*/
    return entry;
}

function corrigeCaracteresExtranosBuscador(entry){
    entry=replaceChar(entry,'ĂĄ','á');
    entry=replaceChar(entry,'ĂŠ','é');
    entry=replaceChar(entry,'Ă­','í');
    entry=replaceChar(entry,'Ăł','ó');
    entry=replaceChar(entry,'Ăş','ú');
    entry=replaceChar(entry,'Ă?','Á');
    entry=replaceChar(entry,'É','É');
    entry=replaceChar(entry,'Ă?','Í');
    entry=replaceChar(entry,'Ă','Ó');
    entry=replaceChar(entry,'Ú','Ú');
    entry=replaceChar(entry,'Ă?','Ä');
    entry=replaceChar(entry,'Ă¤','ä');
    entry=replaceChar(entry,'ĂŤ','ë');
    entry=replaceChar(entry,'ĂŻ','ď');
    entry=replaceChar(entry,'Ăź','ü');
    entry=replaceChar(entry,'Ă','Ü');
    entry=replaceChar(entry,'Ăą','ń');
    entry=replaceChar(entry,'Ă','Ń');
    entry=replaceChar(entry,'Ă','Ŕ');
    entry=replaceChar(entry,'Ă ','ŕ');
    entry=replaceChar(entry,'Ă¨','č');
    entry=replaceChar(entry,'Ă˛','ň');
    entry=replaceChar(entry,'Ă§','Ç;');
    entry=replaceChar(entry,'Ă','Ç');
    entry=replaceChar(entry,'Â´','á');
    entry=replaceChar(entry,'â','&#39;');
    entry=replaceChar(entry,'â','&#39;');
    entry=replaceChar(entry,'Âş','ş');
    entry=replaceChar(entry,'ÂŞ','Ş');
    entry=replaceChar(entry,'ÂĄ','Ą');
    entry=replaceChar(entry,'Âş','ş');

    entry=replaceChar(entry,'\\u00e1','á');
    entry=replaceChar(entry,'\\u00e9','é');
    entry=replaceChar(entry,'\\u00ed','í');
    entry=replaceChar(entry,'\\u00f3','ó');
    entry=replaceChar(entry,'\\u00fa','ú');
    entry=replaceChar(entry,'\\u00c1','Á');
    entry=replaceChar(entry,'\\u00c9','É');
    entry=replaceChar(entry,'\\u00cd','Í');
    entry=replaceChar(entry,'\\u00d3','Ó');
    entry=replaceChar(entry,'\\u00da','Ú');
    entry=replaceChar(entry,'\\u00f1','ń');
    entry=replaceChar(entry,'\\u00d1','Ń');
    entry=replaceChar(entry,'\\u00FC','ü');
    entry=replaceChar(entry,'\\u00DC','Ü');


    return entry;
}/**
 * Devuelve una cadena de texto cambiando una subcadena por otra (todas las ocurrencias)
 * @param entry cadena de texto en la que hacer el reemplazo
 * @param oldCh caracter a sustituir
 * @param newCh caracter de sustitución
 * @returns cadena con los caracteres sustituidos
 */
function replaceChar(entry, oldCh, newCh) {
    var result = entry;
    var posAnt = -1;
    var posActual = result.indexOf(oldCh);

    while ((posActual != -1) && (posAnt != posActual)) {
        result = result.replace(oldCh, newCh);
        posAnt = posActual;
        posActual = result.indexOf(oldCh);
    }

    return result;
}

function buscarSOAP_pre(){
    buscarSOAP(pagina_actual -1);
}

function buscarSOAP_sig(){
    buscarSOAP(pagina_actual +1);
}

function buscarSOAP_ult(){
    ult_pagina = Math.ceil(num_results/page_size);
    if (ult_pagina>pagina_actual){
        buscarSOAP(ult_pagina);
    }
    else{
        //alert("Ya está en la última página");
    }
}

var ultimoValorBuscado = "";

function upperSpecial2Hex( text){
    var result = text.replace(/Á/g, "&#193;");
    result = result.replace(/É/g, "&#201;");
    result = result.replace(/Í/g, "&#205;");
    result = result.replace(/Ó/g, "&#211;");
    result = result.replace(/Ú/g, "&#218;");
    result = result.replace(/Ń/g, "&#209;");
    result = result.replace(/Ü/g, "&#220;");
    return result;
}

//Para toponimos
function buscarSOAP_tipo(tipo, valor, funcCallBack, ine){
    //buscarGooglePlaces(document.getElementById("texto").value);
    //buscarGoogleMaps(document.getElementById("texto").value);
    var url = '/SimpleSearchService/typedSearchService?texto='+ encodeURIComponent(upperSpecial2Hex(valor)) +"&type="+tipo+"&maxResults=10&app="+APP_NAME+(ine !=null?"&muni="+ine:"");

    $.ajax({
        url: url,
        type: 'GET',
        //success:  function(data) { parseDataBotones($.trim(data)); },
        success:  function(data) { /*try{*/
            addTableData(data, valor);
            if (funcCallBack) {
                eval(funcCallBack);
            }
            /*updatePages();*/ /*}
             catch(err){
             showSearching(false);
             alert("No se pudo completar la búsqueda "+err);
             }*/},
        error:  function() { }, // alert("error "+url);},
        processData: false,
        contentType: "text/xml; charset=\"utf-8\"",
        async: true//,
        // timeout: timeoutvalue-200
    });
}

//Para calles con portal
function buscarSOAP_url(pagina, valor, asyncro, funcCallBack, urlService){

    showSearching(true);
    inicio = (pagina-1) * page_size + 1;

    //buscarGooglePlaces(document.getElementById("texto").value);
    //buscarGoogleMaps(document.getElementById("texto").value);
    var url = '';
    if (valor) {
        url = urlService + '?texto='+  normalizaCadena(valor) +"&inicio="+inicio+"&total="+page_size+"&app=tbl2map";
        ultimoValorBuscado = valor;
    } else {
        url = urlService + '?texto='+normalizaCadena(document.getElementById("texto").value)+"&inicio="+inicio+"&total="+page_size+"&app=tbl2map";
        ultimoValorBuscado = document.getElementById("texto").value;
    }

    pagina_actual = pagina;
    $.ajax({
        url: url,
        type: 'GET',
        //success:  function(data) { parseDataBotones($.trim(data)); },
        success:  function(data) { /*try{*/
            addTableData(data, valor, funcCallBack);
            /*
             if (funcCallBack) {
             eval(funcCallBack);
             }*/
            /*updatePages();*/ /*}
             catch(err){
             showSearching(false);
             alert("No se pudo completar la búsqueda "+err);
             }*/},
        error: function() { alert("error "+url);},
        processData: false,
        contentType: "text/xml; charset=\"utf-8\"",
        async: true

    });
}


function buscarSOAP(pagina, valor, asyncro, funcCallBack){
    buscarSOAP_url(pagina, valor, asyncro, funcCallBack, '/SimpleSearchService/services');
}

function updatePages(){
    numPags = Math.ceil(num_results/page_size);
    document.getElementById("pags").innerHTML="P&aacute;gina "+pagina_actual+" de "+numPags;
    $( "#pag1" ).button( "option", "disabled", pagina_actual==1 );
    $( "#pagPrev" ).button( "option", "disabled", pagina_actual==1 );
    $( "#pagSig" ).button( "option", "disabled", pagina_actual==numPags);
    $( "#pagUlt" ).button( "option", "disabled", pagina_actual==numPags );

}


function getObjectIds(results){
    var ids=new Array();
    var strToFind = ".OBJECTID=";
    var count=0;
    var pos = results.indexOf(strToFind, 1);
    //alert(results);
    while (pos != -1) {
        var startpos = pos + strToFind.length;
        var endpos = results.indexOf('"',startpos+1);
        ids[count]= results.substring(startpos+1,endpos);
        pos = results.indexOf(strToFind, endpos);
        count++;
    }
    return ids;
}

function getObjectField(results, field){
    var end='"';
    if (field.indexOf("<")>=0){

        end = field.replace("<","</");

    }
    else if (field.indexOf("\":\"")>=0){

        end='","';

    }

    var ids=new Array();
    var strToFind = field;
    var count=0;
    var pos = results.indexOf(strToFind, 1);
    //alert(results);
    while (pos != -1) {
        var startpos = pos + strToFind.length;
        var endpos = results.indexOf(end,startpos+1);
        ids[count]= results.substring(startpos,endpos);
        pos = results.indexOf(strToFind, endpos);
        count++;
    }
    return ids;
}



function getType(type){
    return tipos[type];
}

function getNameField(type){
    switch(type){
        case "BusCallUrb":
        case "TroidesV": return ".VIA_LOC=\"";
            break;
        case "nombreExacto":
        case "nombreTodas":
        case "nombreAlguna":return "<app:nombre>";
            break;
        case "Nominatim": return "\"display_name\":\"";

        case "Geonames":return "\"toponymName\":\"";
            break;
        case "BusMun": return ".D_MUNI_INE=\"";
            break;
        case "Cuad5k": return ".HOJA50_H5=\"";
            break;
        case "Cuad50k": return ".NAME_HOJA=\"";
            break;
        case "CuaUTM10k": return ".CODIGO=\"";
            break;
        case "ParRus":
        case "BusParUrb":
        case "BusSIGPAC":
        case "RegVic94":return ".REFPAR=\"";
            break;
        case "Localidad": return ".D_NUCLEO_I=\"";
            break;
        case "Comarca": return ".D_COMARCA=\"";
            break;
        case "CatMin": return ".NOMBRE=\"";
            break;
        case "ENP":
        case "HMD":
        case "LICS":
        case "ZEPAS":
        case "PORN":
        case "ZECS":return ".DESCRIPCIO=\"";
            break;
        case "VRTCC1":return ".NOMBRE=\"";
            break;
        case "MONTES":return ".DENOMINACION=\"";
            break;

        default: return type;
    }
}


function showSearching(show){
    /*
     if (show)
     document.getElementById("searchingOverlay").style.visibility="visible";
     else
     document.getElementById("searchingOverlay").style.visibility="hidden";
     */
}



function verFormatter(row, cell, value, columnDef, dataContext){

    return seeIcon(value,'verMini.png');
}


/*function mdFormatter(row, cell, value, columnDef, dataContext){

 return (value.indexOf(".xml")>=0 ? "<a href="+value+"><img  src='images/icon_layerMD.gif'></a>" : "");
 }

 function descargarFormatter(row, cell, value, columnDef, dataContext){

 resultado = "";
 if (value.indexOf(".pdf")>=0){
 resultado = resultado +"<a href=\"#\"><img class=\"icono_descarga\" src='images/PDF.png'></a>";
 }
 if (value.indexOf(".dgn")>=0){
 resultado = resultado +"<a href=\"#\"><img class=\"icono_descarga\" src='images/DGN.png'></a>";
 }
 if (value.indexOf(".dxf")>=0){
 resultado = resultado +"<a href=\"#\"><img class=\"icono_descarga\" src='images/DXF.jpg'></a>";
 }
 if (value.indexOf(".gml")>=0){
 resultado = resultado +"<a href=\"#\"><img class=\"icono_descarga\" src='images/GML.png'></a>";
 }
 if (value.indexOf(".jpg")>=0){
 resultado = resultado +"<a href=\"#\"><img class=\"icono_descarga\" src='images/JPG.png'></a>";
 }
 if (value.indexOf(".kmz")>=0){
 resultado = resultado +"<a href=\"#\"><img class=\"icono_descarga\" src='images/KMZ.png'></a>";
 }
 if (value.indexOf(".shp")>=0){
 resultado = resultado +"<a href=\"#\"><img class=\"icono_descarga\" src='images/SHP.png'></a>";
 }
 if (value.indexOf(".ecw")>=0){
 resultado = resultado +"<a href=\"#\"><img class=\"icono_descarga\" src='images/ECW.png'></a>";
 }
 //alert(resultado);
 return resultado;
 }*/

function seeIcon(value, icon, blank){

    return (value != null && value.length>0 ? "<a href="+value+"  ><img  src='"+IMAGES_PATH+"/"+icon+"'></a>" : "");

}


/*function setTableWidth(){
 //alert($("#mdPanel").width());
 //alert($("#toolbar").width()+"-"+$("#tools").width()+"-"+(withScroll ? 50 : -1)+"="+$("#tabla").width());
 $("#tabla").css('width', $("#mdPanel").width());
 //    alert($("#mdPanel").width());
 //alert($("#toolbar").width()+"-"+$("#tools").width()+"-"+(withScroll ? 50 : -1)+"="+$("#tabla").width());
 //alert($("#tabla").width());
 }
 */
/*function resizeTable(){
 switch ($("#tabs").tabs("option", "active")) {
 case 0:// tabla
 // ajustar el mapa
 //resizeMap();
 // programar que se ajuste la tabla cuando se active su pestańa
 //$("#tabs").tabs("option", "activate", onActivateTable);
 $("#tabs").tabs("option", "active", 1);
 setTableWidth();
 $("#tabs").tabs("option", "active", 0);
 grid.resizeCanvas();
 break;
 case 1: // md
 setTableWidth();
 // ajustar el tamńo de la tabla
 //resizeTable();
 // programar que se ajuste el tamańo del mapa cuando se active su
 // pestańa
 //$("#tabs").tabs("option", "activate", onActivateMap);
 }
 setTableColumnWidth();
 }*/
/*    function setTableColumnWidth(){
 ancho_total =  $("#tabla").width();//-$("#tools").width()-15;
 //alert($("#tabla").width());
 ancho_nombre = ancho_total-70-77-80-40-80-80;

 var data = grid.getColumns();
 data[grid.getColumnIndex("nombre")].width = ancho_nombre-17;
 grid.setColumns(data);
 }*/



function showMuniCatastrales(){
    $( "#muniCList" ).dialog( "open" );
}
function initBuscador(divId, pageSize){

    $("#"+divId).append("<div id=\"resultados_tabla\"></div>");
    $("#"+divId).append("<div id=\"resultados_pag\"></div>");
    $("#resultados_pag").append("<button class=\"pag_button\" id=\"pag1\" disabled onclick=\"buscarSOAP(1)\"></button>");
    $("#resultados_pag").append("<button class=\"pag_button\"id=\"pagPrev\" disabled onclick=\"buscarSOAP_pre()\"></button>");
    $("#resultados_pag").append("<span id=\"pags\">P&aacute;gina 0 de 0</span>");
    $("#resultados_pag").append("<button class=\"pag_button\" id=\"pagSig\" disabled onclick=\"buscarSOAP_sig()\"></button>");
    $("#resultados_pag").append("<button class=\"pag_button\" id=\"pagUlt\" disabled onclick=\"buscarSOAP_ult()\"></button>");
    initButtonsBuscador();
    page_size = pageSize;
    var columns = [
        {id: "nombre", name: "Nombre", field: "nombre",minWidth: 80, sortable:false},
        {id: "municipio", name: "Municipio", field: "municipio",minWidth: 80, sortable:false},
        {id: "tipo", name: "Tipo", field: "tipo",minWidth: 80, maxWidth: 150,sortable:false}];
    if (typeof VER_JS_FUNCTION !='undefined'){
        columns.push(              {id: "ver", name: "Ver", field: "ver",minWidth: VER_COLUMN_WIDTH,width:VER_COLUMN_WIDTH,maxWidth: VER_COLUMN_WIDTH, sortable:false, formatter:verFormatter, cssClass:"centered"});
    }
    if (typeof URL_VISOR !='undefined'){
        columns.push({id: "visor", name: "Visor 2D", field: "visor",minWidth: VISOR_COLUMN_WIDTH,width:VISOR_COLUMN_WIDTH, maxWidth: VISOR_COLUMN_WIDTH, sortable:false, formatter:verFormatter, cssClass:"centered"});
    }
    if (typeof URL_CARTOTECA !='undefined'){
        columns.push( {id: "cartoteca", name: "Cartoteca", field: "cartoteca",minWidth: CARTOTECA_COLUMN_WIDTH,width:CARTOTECA_COLUMN_WIDTH,maxWidth: CARTOTECA_COLUMN_WIDTH, sortable:false, formatter:verFormatter, cssClass:"centered"});
    }
    if (typeof URL_DESCARGAS !='undefined'){
        columns.push(              {id: "descargas", name: "Descargas", field: "descargas",minWidth: DESCARGAS_COLUMN_WIDTH,width:DESCARGAS_COLUMN_WIDTH,maxWidth: DESCARGAS_COLUMN_WIDTH, sortable:false, formatter:verFormatter, cssClass:"centered"});
    }



    var options = {
        editable: false,
        enableCellNavigation: true,
        enableColumnReorder: true,
        forceFitColumns:true
    };



    $(function () {



        dataView = new Slick.Data.DataView();

        grid = new Slick.Grid("#resultados_tabla", dataView, columns, options);
        //  setTableColumnWidth();
        grid.resizeCanvas();






        /* dataView.onRowsChanged.subscribe(function(e,args) {
         grid.invalidateRows(args.rows);
         grid.render();
         });*/
        //  var pager = new Slick.Controls.Pager(dataView, grid, $("#pager"));
        grid.onSort.subscribe(function (e, args) {
            var comparer = function(a, b) {
                return a[args.sortCol.field] > b[args.sortCol.field];
            };

            // Delegate the sorting to DataView.
            // This will fire the change events and update the grid.
            //  alert( dataView.getItem(1)["id"]);
            view = grid.getData();

            view.sort(comparer, args.sortAsc);

            //     alert( dataView.getItem(1)["id"]);

        });

        grid.render();
        var url = window.location.href;
        index = url.indexOf("q=");
        if (index >0){ // se ha indicado un mapa por parámetro
            // obtener el identificador del mapa
            document.getElementById("texto").value = unescape(url.substring(index+2, url.length));
            buscarSOAP(1);
        }
    });
}

function addTableData(datos, ultimoValorBuscado, funcCallBack){
    data = [];
    var json = $.xmlToJSON(datos);
    num_results = 0;
    //alert(json.Body[0].searchResult[0].type[0].Text);
    var pos=0;
    var esPortal = false;
    if((json == null)||(typeof json.Body[0].SearchResponse[0].SearchResult=='undefined')){

        return;
    }
    for ( var j = 0; j < json.Body[0].SearchResponse[0].SearchResult.length; j++) {
        var busquedaLine = json.Body[0].SearchResponse[0].SearchResult[j];
        var resultados ="";
        if (busquedaLine.Count[0].Text!="0"){
            num_results = num_results+parseInt(busquedaLine.Count[0].Text);
            if(typeof busquedaLine.List != 'undefined'){
                resultados = busquedaLine.List[0].Text;


                tipo = busquedaLine.Type[0].Text;


                if(tipo=="Toponimo") {
                    var lineas = resultados.split("\n");
                    for (var i = 0; i < lineas.length; i++) {
                        valores = lineas[i];
                        extractInfo(lineas[i], ultimoValorBuscado, "", tipo, "", true);
                    }
                }
                /*if ((tipo=="nombreExacto")||(tipo== "nombreTodas")||(tipo=="nombreAlguna")){
                 var lineas=resultados.split("\n");
                 for (var i=0;i<lineas.length;i++){
                 valores = lineas[i].split("#");
                 var d = (data[pos] = {});

                 d["id"] = "id_" + j+"_"+i;
                 d["nombre"] = valores[0] ;
                 d["municipio"] = valores[1] ;
                 d["tipo"] = getType(tipo) ;
                 var coords = valores[2].split(":");
                 if (coords.length ==4){
                 bbox = (parseInt(coords[0])-1000)+":"+(parseInt(coords[1])-1000)+":"+(parseInt(coords[2])+1000)+":"+(parseInt(coords[3])+1000);
                 }
                 else if (coords.length==2){
                 bbox = (parseInt(coords[0])-1000)+":"+(parseInt(coords[1])-1000)+":"+(parseInt(coords[0])+1000)+":"+(parseInt(coords[1])+1000)+
                 "&MARKERX="+coords[0]+"&MARKERY="+coords[1];
                 }
                 else{
                 bbox="";
                 }
                 if (bbox!=""){
                 if (typeof URL_VISOR !='undefined'){
                 d["visor"] = URL_VISOR+"BOX="+bbox+" target=\"_blank\"";
                 }
                 if (typeof URL_CARTOTECA !='undefined'){
                 d["cartoteca"] = URL_CARTOTECA+"BOX="+bbox+" target=\"_blank\"";
                 }
                 if (typeof VER_JS_FUNCTION !='undefined'){
                 bbox = "BOX="+bbox;
                 d["ver"] = "javascript:"+VER_JS_FUNCTION+"(\""+bbox+"\") ";
                 }

                 // dportoles
                 if (coords.length ==4) {
                 saveData(ultimoValorBuscado, d["tipo"], d["nombre"], parseInt(coords[0]), parseInt(coords[1]),  parseInt(coords[2]),  parseInt(coords[3]), '', '',d["municipio"]);
                 } else if (coords.length==2) {
                 saveData(ultimoValorBuscado, d["tipo"], d["nombre"], parseInt(coords[0]), parseInt(coords[1]),  parseInt(coords[0]),  parseInt(coords[1]), '', '',d["municipio"]);
                 }
                 // fin dportoles
                 }

                 pos++;
                 }

                 }*/
                else if (tipo.indexOf("Coordenadas")==0){
                    var d = (data[pos] = {});
                    var partes = resultados.split("#");
                    d["id"] = "id_" + j+"_"+i;
                    d["nombre"] = corrigeCaracteresExtranosBuscador(partes[0]) ;
                    d["municipio"] = "" ;
                    d["tipo"] = tipo ;
                    var coords = partes[1].split(":");
                    bbox = (parseInt(coords[0])-1000)+":"+(parseInt(coords[1])-1000)+":"+(parseInt(coords[0])+1000)+":"+(parseInt(coords[1])+1000)+
                        "&MARKERX="+coords[0]+"&MARKERY="+coords[1];

                    if (bbox!=""){
                        if (typeof VER_JS_FUNCTION !='undefined'){
                            bbox = "BOX="+bbox;
                            d["ver"] = "javascript:"+VER_JS_FUNCTION+"(\""+bbox+"\")";
                        }
                        if (typeof URL_VISOR !='undefined'){
                            d["visor"] = URL_VISOR+"BOX="+bbox+" target=\"_blank\"";
                        }
                        if (typeof URL_CARTOTECA !='undefined'){
                            d["cartoteca"] = URL_CARTOTECA+"BOX="+bbox+" target=\"_blank\"";
                        }

                        // dportoles
                        //saveData(ultimoValorBuscado, "Coordenadas", d["nombre"], parseInt(coords[0]), parseInt(coords[1]),  parseInt(coords[0]),  parseInt(coords[1]), '', '');
						// le paso como objectid d["nombre"] porque contiene las coordenadas separadas por ':'
                        saveData(ultimoValorBuscado, d["nombre"], "Coordenadas", d["nombre"]);
                        //saveData(ultimoValorBuscado, d["tipo"], d["nombre"], parseInt(coords[0]), parseInt(coords[1]),  parseInt(coords[0]),  parseInt(coords[1]), '', '');
                        // fin dportoles
                    }
                    pos++;
                }
                else if (tipo.indexOf("TroidesV")==0) {
                    //Nueva parte
                    var lineas = resultados.split("\n");
                    for(var i = 0; i < lineas.length; i++) {
                        var partes = lineas[i].split("#");
                        var nombre = partes[0];
                        var calle = partes[3];
                        portal = tipo.substr(tipo.indexOf("_")+1);

                        esPortal = true;
                        locatePortal(calle, portal, ultimoValorBuscado, funcCallBack);
                    }

                }
                else{
                	var lineas=resultados.split("\n");
					for (var i=0;i<lineas.length;i++){
						
						valores = lineas[i].split("#");
					
                  
                        // dportoles
                        var idField = '';
                        var idValue = '';

                      
                        if (tipo.indexOf("TroidesV")==0){
                            esPortal = true;
                            locatePortal(id_calle[i], portal, bbox, false, ultimoValorBuscado, d["tipo"], d["nombre"], idField, idValue, funcCallBack);
                        } else {
                            saveData(ultimoValorBuscado,   valores[0],getType(tipo),valores[3]);
                        }
                        // fin dportoles
                        pos++;
                    }
                }
            }
        }
    }

    if (! esPortal){
        if (funcCallBack) {
            eval(funcCallBack);
        }
    }
}

function locatePortal(calle,portal,ultimoValorBuscado,funcCallBack){
    $.ajax({
        url: '/Visor2D?service=WFS&' +
        'version=1.0.0&request=GetFeature&typename=TroidesV&' +
        'outputFormat=application/json&srsname=EPSG:25830&' +
        "CQL_FILTER=c_mun_via='"+calle+"' AND numero="+portal,
        type: 'GET',
        success:  function(data){
            var format = new ol.format.GeoJSON();
            var features = format.readFeatures(data);
            if (features.length>0){
                var objectid = features[0].getProperties().objectid;
                var nombre = features[0].getProperties().denomina;
                saveData(ultimoValorBuscado, nombre, "Portal", objectid);
                if (funcCallBack) {
                    eval(funcCallBack);
                }
            }
        },
        error:  function(  jqXHR,  textStatus,  errorThrown) {
            if (funcCallBack) {
                eval(funcCallBack);
            }
        }
    });
}

function showPortal(data,bbox_calle,visor, openWindow, ultimoValorBuscado, _tipo, _nombre, _idField, _idValue){
    count =getObjectField(data,"count=\"");
    var bbox = bbox_calle;
    var minx = "";
    var miny = "";
    var maxx = "";
    var maxy = "";
    if (count[0]!="0"){
        var ids= getObjectField(data,"OBJECTID=\"");

        minx = parseInt(getObjectField(data,"minx=\""));
        miny = parseInt(getObjectField(data,"miny=\""));
        maxx = parseInt(getObjectField(data,"maxx=\""));
        maxy = parseInt(getObjectField(data,"maxy=\""));
        /*if (ids[0]!=null){
         params="ACTIVELAYER=TroidesV&QUERY=OBJECTID="+ids[0];
         }

         else{*/

        params = "BOX="+(parseInt(minx[0])-100)+":"+  (parseInt(miny[0])-100)+":"+  (parseInt(maxx[0])+100)+":"+ ( parseInt(maxy[0])+100)+
            "&MARKERX="+minx[0]+"&MARKERY="+miny[0];
        //}


    }
    else{
        //alert("No se localiza el portal. Se muestra la calle completa");
    }
    if (openWindow) {
        if (visor){
            window.open(URL_VISOR+params);
        }
        else{
            window.open(URL_CARTOTECA+params);
        }
    } else {
        if (minx != "") {
            saveData(ultimoValorBuscado, _tipo, _nombre, minx, miny,  minx,  miny, _idField, _idValue);
        }
    }
}
function transformCoord(lat,lon){

    destino = coordTransform("EPSG:4326", "EPSG:25830", lon, lat);
    xWCTS = destino[0];
    yWCTS = destino[1];

    return  "BOX="+(parseInt(xWCTS)-1000)+":"+  (parseInt(yWCTS)-1000)+":"+  (parseInt(xWCTS)+1000)+":"+ ( parseInt(yWCTS)+1000)+"&MARKERX="+parseInt(xWCTS)+"&MARKERY="+parseInt(yWCTS);


}
Proj4js.defs["EPSG:4326"]="+title=Geograficas WGS84 +proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs ";
Proj4js.defs["EPSG:25830"]="+title=25830 +proj=utm +zone=30 +ellps=GRS80 +units=m +no_defs ";

function coordTransform(srsOrigen, srsDestino, coordX, coordY) {
    destino = new Array();
    if (srsOrigen == srsDestino) {
        destino[0] = coordX;
        destino[1] = coordY;
        return destino;
    }
    else {
        var source = new Proj4js.Proj(srsOrigen);    //source coordinates
        var dest = new Proj4js.Proj(srsDestino);     //destination coordinates

        // transforming point coordinates
        var p = new Proj4js.Point(coordX,coordY);   //any object will do as long as it has 'x' and 'y' properties
        Proj4js.transform(source, dest, p);      //do the transformation.  x and y are modified in place

        destino[0] = p.x;
        destino[1] = p.y;
        return destino;
    }
}

function increaseTypeCount(type) {
    if (possibleTypes[type]) {
        possibleTypes[type] = possibleTypes[type]+1;
    } else {
        possibleTypes[type] = 1;
    }
}

//function saveData(ultimoValorBuscado, _tipo, _nombre, _minx, _miny, _maxx, _maxy, _idField, _idValue, municipio) {
function saveData(ultimoValorBuscado, nombre, typeToFind, objectid) {

    /*
     var idx = linkedPosibilities[ultimoValorBuscado]["count"];
     linkedPosibilities[ultimoValorBuscado][idx] = [];
     linkedPosibilities[ultimoValorBuscado][idx]["type"] = _tipo;
     linkedPosibilities[ultimoValorBuscado][idx]["nombre"] = _nombre;
     linkedPosibilities[ultimoValorBuscado][idx]["xmin"] = _minx;
     linkedPosibilities[ultimoValorBuscado][idx]["ymin"] = _miny;
     linkedPosibilities[ultimoValorBuscado][idx]["xmax"] = _maxx;
     linkedPosibilities[ultimoValorBuscado][idx]["ymax"] = _maxy;
     linkedPosibilities[ultimoValorBuscado][idx]["idField"] = _idField;
     linkedPosibilities[ultimoValorBuscado][idx]["idValue"] = _idValue;
     linkedPosibilities[ultimoValorBuscado]["count"] = idx+1;
     */

    aux = [];
    aux["type"] = typeToFind;
    aux["idValue"] = objectid;
    aux["nombre"] = nombre;
    /*aux["type"] = _tipo;
     aux["nombre"] = _nombre;
     aux["xmin"] = _minx;
     aux["ymin"] = _miny;
     aux["xmax"] = _maxx;
     aux["ymax"] = _maxy;
     aux["idField"] = _idField;
     aux["idValue"] = _idValue;
     aux["municipio"] = municipio;*/
    linkedPosibilities[ultimoValorBuscado].push(aux);
}

