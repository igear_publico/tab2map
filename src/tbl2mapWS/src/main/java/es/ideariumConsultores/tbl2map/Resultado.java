package es.ideariumConsultores.tbl2map;

/**
 * Clase que implementa el resultado de una b�squeda
 * @author Raquel
 *
 */
public class Resultado{
	protected int count;  // n�mero total de coincidencias
	protected String type;
	protected String list;
	
	/**
	 * Constructor
	 * @param count n�mero total de coincidencias
	 * @param result resultado
	 */
	public Resultado (int count){
		this.count = count;
	}
	
	/**
	 * Constructor
	 * @param type tipo de b�squeda
	 * @param count n�mero total de coincidencias
	 * @param result resultado
	 */
	public Resultado (String type, String count, String list){
		this(type, Integer.parseInt(count),list);
	}
	
	/**
	 * Constructor
	 * @param type tipo de b�squeda
	 * @param count n�mero total de coincidencias
	 * @param result resultado
	 */
	public Resultado (String type, int count, String list){
		this.count = count;
		this.type = type;
		this.list = list;
	}
	
	/**
	 * devuelve la cadena de texto que represnta el resultado
	 * @return tipo;total;resultados
	 */
	public String getResult(){
		return type+";"+count +";"+(list != null ? list:"");
	}
}
