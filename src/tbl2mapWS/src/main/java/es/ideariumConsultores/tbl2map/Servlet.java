package es.ideariumConsultores.tbl2map;

import static java.lang.Integer.parseInt;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Serializable;
import java.io.StringReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.fop.servlet.MapaPDFServlet;
import org.geotools.data.DataUtilities;
import org.geotools.data.DefaultTransaction;
import org.geotools.data.Transaction;
import org.geotools.data.collection.ListFeatureCollection;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.shapefile.ShapefileDataStoreFactory;
import org.geotools.data.simple.SimpleFeatureCollection;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.data.simple.SimpleFeatureStore;
import org.geotools.feature.simple.SimpleFeatureBuilder;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.vividsolutions.jts.geom.GeometryFactory;
import com.vividsolutions.jts.geom.LineString;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.geom.Point;
import com.vividsolutions.jts.geom.Polygon;
import com.vividsolutions.jts.io.WKTReader;

import conexion.conexionBD;
import es.ideariumConsultores.batchMap.BatchMap;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

//import javax.xml.parsers.*;

public class Servlet extends HttpServlet {


	private static String nombreConexion = "conexTbl2Map";
	private static String nombreConexionCatrus = "conexTbl2Map_catrus";
	private static String nombreConexionSigpac = "conexTbl2Map_sigpac";
	private static int MAX_VERT_LEGEND=29;
	private static int MAX_HORIZ_LEGEND=22;
	/**
	 * Inicializa el servlet. Carga el fichero de propiedades
	 * @param servletConfig
	 */
	public void init(ServletConfig servletConfig) throws ServletException
	{
		super.init(servletConfig);
		Tbl2Map.initProperties(/*servletConfig.getInitParameter("properties_path")*/
				getServletContext().getRealPath("/WEB-INF/config")+"/tbl2mapWS.properties");
		BatchMap.initProperties(/*servletConfig.getInitParameter("properties_path")*/
				getServletContext().getRealPath("/WEB-INF/config")+"/batchMapWS.properties");
	}

	/**
	 * peticion por GET
	 * @param request peticion que representa una tabla que quiere convertise en mapa
	 * @param response resultado tipo mapa
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException
	{
		String op = request.getParameter("REQUEST");
		Tbl2Map.logger.info(op);
		if (op.equalsIgnoreCase("LOG")){
			try{
				Properties logProp = new Properties();

				logProp.load(new FileInputStream(getServletContext().getRealPath("/WEB-INF/classes")+"/log4j.properties"));
				FileInputStream log = new FileInputStream(logProp.getProperty("log4j.appender.A1.File"));

				ServletOutputStream sos = response.getOutputStream();
				response.setStatus(HttpServletResponse.SC_OK);
				response.setContentType("application/file");
				response.setHeader("Content-Disposition", "attachment; filename=\"Tbl2Map.log\"");
				byte[] logBytes = new byte[512];
				while (log.read(logBytes)>0){
					sos.write(logBytes);
					logBytes = new byte[512];
				}
				sos.flush();
				log.close();
			}
			catch(Exception ex){
				try{
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
				}catch(IOException ex2){
					Tbl2Map.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
					return;
				}
				Tbl2Map.logger.error("Error en la exportación a shapefile",ex);
			}
		}
		else if (op.equalsIgnoreCase("getJobProgress")){
			String jobid = request.getParameter("JOBID");
			if ((jobid!=null)&&(jobid.length()>0)){

				try{

					ServletOutputStream sos = response.getOutputStream();
					response.setStatus(HttpServletResponse.SC_OK);
					response.setContentType("application/text");
					Integer progreso = BatchMap.getJobProgress(jobid);
					if(progreso ==null){
						sos.write(new String("NOJOB").getBytes());
					}
					else if(progreso ==BatchMap.FINISHED){
						sos.write(new String("ALL").getBytes());
					}
					else if(progreso ==BatchMap.CANCELLED){
						sos.write(new String("CANCELLED").getBytes());
					}
					else if(progreso ==BatchMap.ERROR){
						sos.write(new String("ERROR").getBytes());
					}
					else{
						sos.write(progreso.toString().getBytes());
					}
					
					sos.flush();
				}
				catch(Exception ex){
					try{
						response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
					}catch(IOException ex2){
						BatchMap.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
						return;
					}
					BatchMap.logger.error("Error en la obtenci�n del progreso del trabajo "+jobid,ex);
				}
				return;
			}
			else{
				try{
					response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el par�metro JOBID");
				}
				catch(IOException ex){
					BatchMap.logger.error("No se pudo enviar respuesta de error (faltan par�metros o valores inv�lidos",ex);
					return;
				}
				return;
			}
		}
		else if (op.equalsIgnoreCase("getJob")){
			try{
				String jobid = request.getParameter("JOBID");
				try{
					if ((jobid==null)||(jobid.length()<0)){
						response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el par�metro JOBID");
						return;
					}
					
					if (!BatchMap.isFinished(jobid)){
						response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "El trabajo no est� finalizado. Int�ntelo m�s tarde.");
						return;

					}
					
				}
				catch(IOException ex){
					BatchMap.logger.error("No se pudo enviar respuesta de error (faltan par�metros o valores inv�lidos",ex);
					return;
				}
				byte[] resultado = BatchMap.getZippedFiles(true, jobid);
				ServletOutputStream sos = response.getOutputStream();
				response.setStatus(HttpServletResponse.SC_OK);
				response.setContentType("application/zip");
				response.setHeader("Content-Disposition", "attachment; filename=\"mapaIdearagon.zip\"");
				sos.write(resultado);
				sos.flush();
			}
			catch(Exception ex){
				try{
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
				}catch(IOException ex2){
					BatchMap.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
					return;
				}
				BatchMap.logger.error("Error en la obtenci�n de los mapas",ex);
			}
	}
		else if (op.equalsIgnoreCase("cancelJob")){
			try{
				String jobid = request.getParameter("JOBID");
				try{
					if ((jobid==null)||(jobid.length()<0)){
						response.sendError(HttpServletResponse.SC_PRECONDITION_FAILED, "Debe indicar un valor para el par�metro JOBID");
						return;
					}
					
				
					
				}
				catch(IOException ex){
					BatchMap.logger.error("No se pudo enviar respuesta de error (faltan par�metros o valores inv�lidos",ex);
					return;
				}
				BatchMap.cancelJob(jobid);
				response.setStatus(HttpServletResponse.SC_OK);
			
			}
			catch(Exception ex){
				try{
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
				}catch(IOException ex2){
					BatchMap.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
					return;
				}
				BatchMap.logger.error("Error en la cancelaci�n del trabajo",ex);
			}
	}
		else{
			doPost(request, response);
		}
	}

	/**
	 * peticion por POST
	 * @param request peticion que representa una tabla que quiere convertise en mapa
	 * @param response resultado tipo mapa
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
	throws ServletException
	{
		try {
			Tbl2Map.logger.info("**************  ******************  *****************");
			DocumentBuilderFactory docFactory =  DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
			Document doc = null;
			if (request.getParameter("datos") != null) {
				InputSource is = new InputSource(new StringReader(request.getParameter("datos")));
				doc = docBuilder.parse(is);
				// comentada esta llamada ya que no funciona hace tiempo. No existen las tablas
				//try {
				//	registraQuery(request, request.getParameter("datos"));
				//} catch (Exception ex) {
				//	Tbl2Map.logger.error("Error al registrar peticion en BBDD:");
				//}
			} else {
				// viene por post ajax

				// clonar el input stream para construir doc y para guardar en BBDD
				// no es posible leer dos veces el inputstream (al menos, no es f�cil)

				// para el registro en BBDD no hace falta el input stream, me quedo ya con un String
				// que es lo que me interesa
				ByteArrayOutputStream baos = new ByteArrayOutputStream();

				//Fake code simulating the copy
				//You can generally do better with nio if you need...
				//And please, unlike me, do something about the Exceptions :D
				byte[] buffer = new byte[1024];
				int len;
				String s = "";

				while ((len = request.getInputStream().read(buffer)) > -1 ) {
					baos.write(buffer, 0, len);
					s += new String(buffer);
				}
				baos.flush();

				//Open new InputStreams using the recorded bytes
				//Can be repeated as many times as you wish
				InputStream is1 = new ByteArrayInputStream(baos.toByteArray());

				doc = docBuilder.parse(is1);

				// comentada esta llamada ya que no funciona hace tiempo. No existen las tablas
				//try {
				//	registraQuery(request, s);
				//} catch (Exception ex) {
				//	Tbl2Map.logger.error("Error al registrar peticion en BBDD:");
				//}
			}


			Element  element = doc.getDocumentElement();
			NodeList root = element.getChildNodes();

			String resp = "";
			String envel = "";
			String type = "";
			String layer = "";
			String filter = "";
			int width = 500;
			int height = 500;
			String outputFormat = "";
			String lote =null;
			
			String legendTitle ="";
			String capas =null;
			String leyendas =null;
			String tamano = "";
			String titulo ="";
			String fecha = "";
			String orientacion ="";
			NodeList items = null;
			String url1 = "";
			String url2 = "";
			String NOM_FILE_MD = "info.txt";
			String CONTENT_FILE_MD_STR = "Fuente: Mapa realizado con la aplicación geográfica Tabla a Mapa de IDEAragon: https://idearagon.aragon.es/Tab2Map";
			byte[] CONTENT_FILE_MD = CONTENT_FILE_MD_STR.getBytes();

			for (int i=0; i<root.getLength(); i++) {
				Node firstLevelChild= root.item(i);
				if (firstLevelChild.getNodeName().equals("extent")) {
					envel = firstLevelChild.getTextContent();
				} else if (firstLevelChild.getNodeName().equals("outputFormat")) {
					outputFormat = firstLevelChild.getTextContent();
				} else if (firstLevelChild.getNodeName().equals("w")) {
					width = parseInt(firstLevelChild.getTextContent());
				} else if (firstLevelChild.getNodeName().equals("h")) {
					height = parseInt(firstLevelChild.getTextContent());
				} else if (firstLevelChild.getNodeName().equals("titulo")) {
					titulo = firstLevelChild.getTextContent();
				} else if (firstLevelChild.getNodeName().equals("fecha")) {
					fecha = firstLevelChild.getTextContent();
				} else if (firstLevelChild.getNodeName().equals("orientacion")) {
					orientacion = firstLevelChild.getTextContent();
				} else if (firstLevelChild.getNodeName().equals("url1")) {
					url1 = firstLevelChild.getTextContent();
				} else if (firstLevelChild.getNodeName().equals("url2")) {
					url2 = firstLevelChild.getTextContent();
				} else if (firstLevelChild.getNodeName().equals("type")) {
					type = firstLevelChild.getTextContent();
				} else if (firstLevelChild.getNodeName().equals("layer")) {
					layer = firstLevelChild.getTextContent();
				} else if (firstLevelChild.getNodeName().equals("filter")) {
					filter = firstLevelChild.getTextContent();
				} else if (firstLevelChild.getNodeName().equals("itemList")) {
					items = firstLevelChild.getChildNodes();
				}
				else if (firstLevelChild.getNodeName().equals("lote")) {
					lote = firstLevelChild.getTextContent();
				}
				
				else if (firstLevelChild.getNodeName().equals("capas")) {
					capas = firstLevelChild.getTextContent();
				}
				else if (firstLevelChild.getNodeName().equals("leyendas")) {
					leyendas = firstLevelChild.getTextContent();
				}
				else if (firstLevelChild.getNodeName().equals("tamano")) {
					tamano = firstLevelChild.getTextContent();
				}
				else if (firstLevelChild.getNodeName().equals("columna")) {
					legendTitle = firstLevelChild.getTextContent();
				}
			}
			Tbl2Map.logger.info("titulo:"+titulo);
			if (outputFormat.equals("GML")) {
				String format = "GML";
				Connection conn = null;
				if (type.equals("BusParUrb")) {
					conn = conexionBD.getConnection(nombreConexionCatrus);
				}
				else if (type.equals("BusSIGPAC")) {
					conn = conexionBD.getConnection(nombreConexionSigpac);
				}
				else {
					conn = conexionBD.getConnection(nombreConexion);
				}
				//Conexion bbdd aqui
				resp = generateData(format, items, type,   layer, filter, conn, null);

				// enviar la respuesta
				//System.out.println(resp);

				response.setStatus(HttpServletResponse.SC_OK);
				response.setContentType("text/xml; charset=\"utf-8\"");
				response.setHeader("Content-Disposition", "attachment; filename=\"gmlIdearagon.gml\"");
				response.getWriter().println(resp);
				response.getWriter().flush();
			} else if (outputFormat.equals("GeoJSON")) {
				String format = "GeoJSON";
				Connection conn = null;
				if (type.equals("BusParUrb")) {
					conn = conexionBD.getConnection(nombreConexionCatrus);
				}
				else if (type.equals("BusSIGPAC")) {
					conn = conexionBD.getConnection(nombreConexionSigpac);
				}
				else {
					conn = conexionBD.getConnection(nombreConexion);
				}

				//Conexion bbdd aqui
				resp = generateData(format, items, type,   layer, filter, conn, null);

				// enviar la respuesta
				//System.out.println(resp);

				response.setStatus(HttpServletResponse.SC_OK);
				response.setContentType("application/json; charset=\"utf-8\"");
				response.setHeader("Content-Disposition", "attachment; filename=\"geojsonIdearagon.json\"");
				response.getWriter().println(resp);
				response.getWriter().flush();
			}  else if (outputFormat.equals("KML")) {
				String format = "KML";
				Connection conn = null;
				if (type.equals("BusParUrb")) {
					conn = conexionBD.getConnection(nombreConexionCatrus);
				}
				else if (type.equals("BusSIGPAC")) {
					conn = conexionBD.getConnection(nombreConexionSigpac);
				}
				else {
					conn = conexionBD.getConnection(nombreConexion);
				}

				//Conexion bbdd aqui
				resp = generateData(format, items, type,   layer, filter, conn, null);

				// enviar la respuesta
				//System.out.println(resp);

				response.setStatus(HttpServletResponse.SC_OK);
				response.setContentType("application/vnd.google-earth.kml+xml; charset=\"utf-8\"");
				response.setHeader("Content-Disposition", "attachment; filename=\"kmlIdearagon.kml\"");
				response.getWriter().println(resp);
				response.getWriter().flush();
			} else if (outputFormat.equals("JPG") || outputFormat.equals("PDF")) {

				// PDF se hace igual que JPG
				String resultado = null;
				String format = "JPG";
				if (lote!=null){
					String job = BatchMap.initJob();
					if (job==null){
						try{
							response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR,"No se pudo iniciar el trabajo");
						}catch(IOException ex2){
							Tbl2Map.logger.error("No se pudo enviar respuesta de error (error interno)",ex2);
							return;
						}
					}
					ServletOutputStream sos = response.getOutputStream();
					response.setStatus(HttpServletResponse.SC_OK);
					response.setContentType("application/text");
					sos.write(job.getBytes());
					sos.flush();
					
					MapaPDFServlet pdfServlet = new MapaPDFServlet();
					pdfServlet.init(this.getServletContext());
					ArrayList<String> legendPathHoriz = generateLegend(items,legendTitle,MAX_HORIZ_LEGEND);
					ArrayList<String> legendPathVert = generateLegend(items,legendTitle,MAX_VERT_LEGEND);
					
					if(!type.equals("Toponimo") && !type.equals("Coordenadas")) { //utilizar SLD final
						resp = generateData(format, items, type,   layer, filter, null, null);
					}
					Tbl2Map.logger.info("titulo:"+titulo);
					new BatchMap(job, capas,leyendas, lote, outputFormat, orientacion, tamano,items,resp,legendPathHoriz,legendPathVert,titulo,pdfServlet).start();
					
				}
				else{
					BufferedImage img = new BufferedImage(height,width, BufferedImage.TYPE_INT_ARGB);
					Graphics2D g2d = img.createGraphics();
					g2d.setComposite(AlphaComposite.Clear);
					g2d.fillRect(0, 0, new Integer(height),new Integer(width));
					g2d.setComposite(AlphaComposite.Src);
					resp = generateData(format, items, type,   layer, filter, null, g2d);
					ArrayList<String> legendPath = generateLegend(items,legendTitle, (outputFormat.equals("JPG")?9999: (orientacion.equalsIgnoreCase("vertical")?MAX_VERT_LEGEND:MAX_HORIZ_LEGEND)));

					if(!type.equals("Toponimo") && !type.equals("Coordenadas")) { //utilizar SLD final

						resp += "<BoundingBox srsName=\"http://www.opengis.net/gml/srs/epsg.xml#25830\">\n";
						String[] listabbox = envel.split(":");
						resp += "<gml:coord><gml:X>" + listabbox[0] + "</gml:X><gml:Y>" + listabbox[1] + "</gml:Y></gml:coord>\n";
						resp += "<gml:coord><gml:X>" + listabbox[2] + "</gml:X><gml:Y>" + listabbox[3] + "</gml:Y></gml:coord>\n";
						/*         resp += "<gml:coord><gml:X>" + (677191-200) + "</gml:X><gml:Y>" + (4612732-200) + "</gml:Y></gml:coord>\n";
                    resp += "<gml:coord><gml:X>" + (677191+200) + "</gml:X><gml:Y>" + (4612732+200) + "</gml:Y></gml:coord>\n";*/
						resp += "</BoundingBox>\n";
						resp += "<Output>\n" +
						"   <Format>image/png</Format>\n" +
						"   <Transparent>true</Transparent>\n" +
						"   <Size><Width>" + width + "</Width><Height>" + height + "</Height></Size>\n" +
						"</Output>" +
						"</ogc:GetMap>\n";
						String getMap = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
						"<ogc:GetMap xmlns:ogc=\"http://www.opengis.net/ows\"\n" +
						"            xmlns:gml=\"http://www.opengis.net/gml\"\n" +
						"   version=\"1.1.1\" service=\"WMS\">\n";
						getMap += resp;

						resultado = Tbl2Map.doSearch("https://idearagon.aragon.es/Visor2D?SERVICE=WMS", getMap);
					}
					else {
						g2d.dispose();
						String filename = UUID.randomUUID().toString() + ".png";
						resultado = Tbl2Map.properties.getProperty("imgs_path")+ filename;
						File output = new File(resultado);
						ImageIO.write(img, "png", output);
						//resultado = Tbl2Map.properties.getProperty("imgs_url")+filename;
					}
					if (outputFormat.equals("JPG")){
						String imageUrl = "";
						String legendUrl = "";
						try {
							Tbl2Map.logger.debug(resultado);
							Tbl2Map.logger.info("titulo:"+titulo);
							/*DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                    Document doc2 = dBuilder.parse(new ByteArrayInputStream(resultado.getBytes()));
                    doc2.getDocumentElement().normalize();*/
							BatchMap batchMap = new BatchMap(null, capas,leyendas, lote, outputFormat, orientacion, tamano,items,resp,(orientacion.equalsIgnoreCase("vertical")?null:legendPath),(orientacion.equalsIgnoreCase("vertical")?legendPath:null),titulo,null);
							File imgFile = new File(resultado);
							BufferedImage dataImg = ImageIO.read(imgFile);
							BufferedImage mapaImg =batchMap.addPrintWMS(dataImg, envel.replaceAll(":",","),width,height);
							ImageIO.write(mapaImg, "PNG", imgFile);
							FileInputStream fis = new FileInputStream(imgFile);

							byte[] byteArray= new byte[(int) imgFile.length()];
							fis.read(byteArray);
							fis.close();
							BASE64Encoder encoder = new BASE64Encoder();
							imageUrl ="data:image/png;base64,"+encoder.encode(byteArray);
							//imageUrl = resultado;
							File legendFile = new File(legendPath.get(0));
							if (dataImg!=mapaImg){ //  se han añadido capas personalizadas
							BufferedImage mapaLegend= batchMap.addWMSLegend(ImageIO.read(legendFile));
							ImageIO.write(mapaLegend, "PNG", legendFile);
							}
							fis = new FileInputStream(legendFile);

							byteArray= new byte[(int) legendFile.length()];
							fis.read(byteArray);
							fis.close();

							legendUrl = "data:image/png;base64,"+encoder.encode(byteArray);
						}
						catch (Exception ex){
							Tbl2Map.logger.error("Error al procesar el resultado de ArcIMS "+resultado, ex);
						}

						response.setStatus(HttpServletResponse.SC_OK);
						/*
				response.setContentType("application/xml; charset=\"utf-8\"");
				//TODO: generar jgw y hacer un zip con jpg+jgw
				response.getWriter().println("<?xml version=\"1.0\" encoding=\"utf-8\"?><map><url>" + imageUrl + "</url></map>");
						 */


						response.setContentType("application/text; charset=\"utf-8\"");
						response.getWriter().println(imageUrl + "@" + legendUrl);
						response.getWriter().flush();
					}
					else{ //PDF

						MapaPDFServlet pdfServlet = new MapaPDFServlet();
						pdfServlet.init(this.getServletContext());
						BatchMap batchMap = new BatchMap(null, capas, leyendas,lote, outputFormat, orientacion, tamano,items,resp,(orientacion.equalsIgnoreCase("vertical")?null:legendPath),(orientacion.equalsIgnoreCase("vertical")?legendPath:null),titulo,pdfServlet);
						String capasXML = batchMap.getPrintWMS(envel.replaceAll(":",","),width,height);
						String leyendaXML="";
						for (int i=1;i<legendPath.size(); i++){
							leyendaXML+="<leyenda><laUrl>"+legendPath.get(i).replaceAll(" ", "%20")+"</laUrl></leyenda>";	
						}
						if (capasXML.length()>0){// hay capas personalizadas
							leyendaXML += batchMap.getPrintWMSLegend();
						}
						Tbl2Map.logger.info("titulo:"+titulo);
						String xml = "<imprimir><fecha>"+fecha+"</fecha><titulo>"+titulo+"</titulo>"+capasXML+"<mapa><laUrl>file:///" + resultado.replaceAll(" ", "%20") + "</laUrl></mapa><leyendaT2M><leyenda><laUrl>file:///" + legendPath.get(0).replaceAll(" ", "%20")+"</laUrl></leyenda></leyendaT2M>"+leyendaXML+"</imprimir>";
						Tbl2Map.logger.info("XML : " + xml);
						File temp = File.createTempFile("xml4pdf", ".xml");
						PrintWriter writer = new PrintWriter(temp.getAbsolutePath(), "UTF-8");
						writer.println(xml);

						writer.close();
						Tbl2Map.logger.info("Temp file : " + temp.getAbsolutePath());


						String xslt = Tbl2Map.properties.getProperty("printActionXSL_vertical");
						if (!orientacion.equalsIgnoreCase("vertical")){
							xslt = Tbl2Map.properties.getProperty("printActionXSL_horizontal");
						}

						pdfServlet.renderXML(temp.getAbsolutePath(),xslt, response);
					}
				}
			} else if (outputFormat.equals("ZIP")) {
				String[] files = { "mapa.jpg", "leyenda.jpg", NOM_FILE_MD };
				String[] urlFiles = { url1, url2 };


				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ZipOutputStream zos = new ZipOutputStream(baos);
				byte bytes[] = new byte[2048];

				int idx = 0;
				for (String fileName : files) {
					zos.putNextEntry(new ZipEntry(fileName));

					if (fileName.equals(NOM_FILE_MD)) {
						zos.write(CONTENT_FILE_MD, 0, CONTENT_FILE_MD.length);
					} else {


						BASE64Decoder decoder = new BASE64Decoder();
						String bytes64 = urlFiles[idx].replaceFirst("data:image/png;base64,", "");
						byte[] bytesImg = decoder.decodeBuffer(bytes64);
						zos.write(bytesImg, 0, bytesImg.length);
						/*InputStream fis = new URL(urlFiles[idx]).openStream();
						//FileInputStream fis = new FileInputStream(fileName);
						BufferedInputStream bis = new BufferedInputStream(fis);
						int bytesRead;
						while ((bytesRead = bis.read(bytes)) != -1) {
							zos.write(bytes, 0, bytesRead);
						}
						bis.close();
						fis.close();*/
					}
					zos.closeEntry();
					idx++;
				}
				zos.flush();
				baos.flush();
				zos.close();
				baos.close();

				byte[] zip = baos.toByteArray();

				// Sends the response back to the user / browser. The
				// content for zip file type is "application/zip". We
				// also set the content disposition as attachment for
				// the browser to show a dialog that will let user
				// choose what action will he do to the sent content.

				ServletOutputStream sos = response.getOutputStream();
				response.setStatus(HttpServletResponse.SC_OK);
				response.setContentType("application/zip");
				response.setHeader("Content-Disposition", "attachment; filename=\"mapaIdearagon.zip\"");
				sos.write(zip);
				sos.flush();

				// borrar ficheros temporales
				for (String fileName : files) {
					File aux = new File(fileName);
					aux.delete();
				}
			} else if (outputFormat.equals("PNG")) {
				String format = "PNG";
				resp = generateData(format, items, type,   layer, filter, null, null);


				String resultado = Tbl2Map.doSearch(
						Tbl2Map.properties.getProperty("ARCIMS_URL")+"?"+(Tbl2Map.properties.getProperty("ARCIMS_URL_PARAMS_VIEW")),
						resp);

				String imageUrl = "";
				try {
					DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
					DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
					Document doc2 = dBuilder.parse(new ByteArrayInputStream(resultado.getBytes()));
					doc2.getDocumentElement().normalize();
					imageUrl = ((Element)doc2.getElementsByTagName("OUTPUT").item(0)).getAttribute("url");
				}
				catch (Exception ex){
					Tbl2Map.logger.error("Error al procesar el resultado de ArcIMS "+resultado, ex);
				}
				response.setStatus(HttpServletResponse.SC_OK);
				response.setContentType("application/xml; charset=\"utf-8\"");
				response.getWriter().println("<?xml version=\"1.0\" encoding=\"utf-8\"?><map><url>" + imageUrl + "</url></map>");
				response.getWriter().flush();
			} else if (outputFormat.equals("SHP")) {
				String format = "SHP";
				// We use the DataUtilities class to create a FeatureType that will describe the data in our shapefile.
				// See also the createFeatureType method below for another, more flexible approach.
				SimpleFeatureType TYPE = null;

				// A list to collect features as we create them.
				List<SimpleFeature> features = new ArrayList<SimpleFeature>();

				Connection conn = null;
				if (type.equals("BusParUrb")) {
					conn = conexionBD.getConnection(nombreConexionCatrus);
				}
				else if (type.equals("BusSIGPAC")) {
					conn = conexionBD.getConnection(nombreConexionSigpac);
				}
				else {
					conn = conexionBD.getConnection(nombreConexion);
				}

				for (int j=0; j < items.getLength(); j++) {
					// Lo primero es un <it> => Hay que saltarlo
					NodeList theItem = items.item(j).getChildNodes();

					String idValue = "";
					String idField = "";
					String color = "";
					Vector<String> atribNames = new Vector<String>();
					Vector<String> atribValues = new Vector<String>();

					for (int k=0; k < theItem.getLength(); k++){
						Node firstLevelChild = theItem.item(k);

						if (firstLevelChild.getNodeName().equals("_val")) {
							idValue = firstLevelChild.getTextContent();
						} else if (firstLevelChild.getNodeName().equals("_fld")) {
							idField = firstLevelChild.getTextContent();
						} else if (firstLevelChild.getNodeName().equals("_rgb")) {
							color = firstLevelChild.getTextContent();
						} else {
							atribNames.add(firstLevelChild.getNodeName());
							atribValues.add(firstLevelChild.getTextContent());
						}
					}

					//Conexion bbdd aqui

					String geometryString = getGeometry(format, type, idValue, idField, color, "", j, null, conn);

					// la primera geometr�a decide el tipo y los atributos
					if (TYPE == null) {
						String attribStr = "";
						for (int i = 0; i < atribNames.size(); i++) {
							// Todos los considero como String
							attribStr += "," + atribNames.get(i) + ":String";
						}
						if (geometryString.contains("POLYGON")) {
							//                            TYPE = DataUtilities.createType("Location", "the_geom:Polygon:srid=25830" + attribStr);
							TYPE = DataUtilities.createType("Location", "the_geom:MultiPolygon:srid=25830" + attribStr);

						} else if (geometryString.startsWith("LINESTRING")) {
							TYPE = DataUtilities.createType("Location", "the_geom:LineString:srid=25830" + attribStr);
						} else if (geometryString.startsWith("POINT")) {
							TYPE = DataUtilities.createType("Location", "the_geom:Point:srid=25830" + attribStr);
						} else {
							TYPE = DataUtilities.createType("Location", "the_geom:Point:srid=25830" + attribStr);
						}
					}

					SimpleFeatureBuilder featureBuilder = new SimpleFeatureBuilder(TYPE);

					// create features

					// GeometryFactory will be used to create the geometry attribute of each feature,
					// using a Point object for the location.
					GeometryFactory geometryFactory = JTSFactoryFinder.getGeometryFactory();
					WKTReader reader = new WKTReader(geometryFactory);
					if (geometryString.contains("POLYGON")) {
						if (geometryString.startsWith("POLYGON")) {
							//geo.setType(GeometryType.POLYGON);
							Polygon mp = (Polygon) reader.read(geometryString);
							featureBuilder.add(mp);
						} else if (geometryString.startsWith("MULTIPOLYGON")) {
							//geo.setType(GeometryType.MULITPOLYGON);
							MultiPolygon mp = (MultiPolygon) reader.read(geometryString);
							featureBuilder.add(mp);
							/*
	        				com.vividsolutions.jts.geom.Geometry e = mp.getEnvelope();
	        				Coordinate[] boundingBox = e.getCoordinates();
	        				geo.setBoundingBox(boundingBox);*/
						}
					} else if (geometryString.startsWith("LINESTRING")) {
						LineString line = (LineString) reader.read(geometryString);
					} else if (geometryString.startsWith("POINT")) {
						Point point = (Point) reader.read(geometryString);
						featureBuilder.add(point);
					}

					for (int i = 0; i < atribValues.size(); i++) {
						featureBuilder.add(atribValues.get(i));
					}
					SimpleFeature feature = featureBuilder.buildFeature(null);
					features.add(feature);
				}

				// Create a shapefile From a FeatureCollection

				// Get an output file name and create the new shapefile
				long startTime = (long) 1410455556382L;
				long diff = System.currentTimeMillis() - startTime;

				String shpName = Long.toString(diff);
				//TODO: Comprobar que no existe ya el fichero
				// No deber�a ocurrir puesto que no deber�a generarse en el mismo timestamp otro
				File newFile = new File(shpName + ".shp");
				ShapefileDataStoreFactory dataStoreFactory = new ShapefileDataStoreFactory();

				Map<String, Serializable> params = new HashMap<String, Serializable>();
				params.put("url", newFile.toURI().toURL());
				params.put("create spatial index", Boolean.TRUE);

				ShapefileDataStore newDataStore = (ShapefileDataStore) dataStoreFactory.createNewDataStore(params);

				// TYPE is used as a template to describe the file contents
				newDataStore.createSchema(TYPE);

				// Write the feature data to the shapefile
				Transaction transaction = new DefaultTransaction("create");

				String typeName = newDataStore.getTypeNames()[0];
				SimpleFeatureSource featureSource = newDataStore.getFeatureSource(typeName);
				//SimpleFeatureType SHAPE_TYPE = featureSource.getSchema();
				/*
				 * The Shapefile format has a couple limitations:
				 * - "the_geom" is always first, and used for the geometry attribute name
				 * - "the_geom" must be of type Point, MultiPoint, MuiltiLineString, MultiPolygon
				 * - Attribute names are limited in length 
				 * - Not all data types are supported (example Timestamp represented as Date)
				 * 
				 * Each data store has different limitations so check the resulting SimpleFeatureType.
				 */

				if (featureSource instanceof SimpleFeatureStore) {
					SimpleFeatureStore featureStore = (SimpleFeatureStore) featureSource;
					// SimpleFeatureStore has a method to add features from a
					// SimpleFeatureCollection object, so we use the ListFeatureCollection
					// class to wrap our list of features.
					SimpleFeatureCollection collection = new ListFeatureCollection(TYPE, features);
					featureStore.setTransaction(transaction);
					try {
						featureStore.addFeatures(collection);
						transaction.commit();
					} catch (Exception problem) {
						problem.printStackTrace();
						transaction.rollback();
					} finally {
						transaction.close();
					}
					// success!

					String[] files = { shpName + ".dbf", shpName + ".fix", shpName + ".prj", shpName + ".shp", shpName + ".shx", NOM_FILE_MD };

					ByteArrayOutputStream baos = new ByteArrayOutputStream();
					ZipOutputStream zos = new ZipOutputStream(baos);
					byte bytes[] = new byte[2048];

					for (String fileName : files) {
						zos.putNextEntry(new ZipEntry(fileName));
						if (fileName.equals(NOM_FILE_MD)) {
							zos.write(CONTENT_FILE_MD, 0, CONTENT_FILE_MD.length);
						} else {
							FileInputStream fis = new FileInputStream(fileName);
							BufferedInputStream bis = new BufferedInputStream(fis);

							int bytesRead;
							while ((bytesRead = bis.read(bytes)) != -1) {
								zos.write(bytes, 0, bytesRead);
							}
							bis.close();
							fis.close();
						}
						zos.closeEntry();
					}
					zos.flush();
					baos.flush();
					zos.close();
					baos.close();

					byte[] zip = baos.toByteArray();

					// Sends the response back to the user / browser. The
					// content for zip file type is "application/zip". We
					// also set the content disposition as attachment for
					// the browser to show a dialog that will let user
					// choose what action will he do to the sent content.

					ServletOutputStream sos = response.getOutputStream();
					response.setStatus(HttpServletResponse.SC_OK);
					response.setContentType("application/zip");
					response.setHeader("Content-Disposition", "attachment; filename=\"" + shpName + ".zip\"");
					sos.write(zip);
					sos.flush();

					// borrar ficheros temporales
					for (String fileName : files) {
						File aux = new File(fileName);
						aux.delete();
					}

				} else {
					System.out.println(typeName + " does not support read/write access");
					Tbl2Map.logger.error(typeName + " does not support read/write access");
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				}
			}
		}
		catch(Throwable e) {
			Tbl2Map.logger.error("Error en la búsqueda de ", e);
			System.out.println("Error en la busqueda de "+ e.toString());
			try {
				response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			}
			catch(Exception ex){

			}
		}
	}

	private void registraQuery(HttpServletRequest request, String doc)  {
		// comentada esta llamada ya que no funciona hace tiempo. No existen las tablas

/*
		Connection conn=null;//Objeto para la conexi�n a la BD
		PreparedStatement stmt=null;//Objeto para la ejecucion de sentencias
		Statement stmt2=null;//Objeto para la ejecucion de sentencias
		//PreparedStatement stmt2=null;
		try {

			String ip = request.getHeader("X-Forwarded-For");
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("Proxy-Client-IP");
			}
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("WL-Proxy-Client-IP");
			}
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("HTTP_CLIENT_IP");
			}
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getHeader("HTTP_X_FORWARDED_FOR");
			}
			if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
				ip = request.getRemoteAddr();
			}
			Calendar c = new GregorianCalendar();
			String fecha = Integer.toString(c.get(Calendar.YEAR))+"/"+Integer.toString(c.get(Calendar.MONTH)+1)+"/"+Integer.toString(c.get(Calendar.DATE));

			//Nos conectamos a la BD
			conn = conexionBD.getConnection(nombreConexion);

			//conn = conexionOracle.getConnection();
			stmt = conn.prepareStatement("INSERT INTO carto.peticiones_tbl2map(request, timestamp, app,ip,inicio_req,req_clob) VALUES (? ,'"+fecha+"','tbl2map','"+ip+"', ?, ?)");

			// esta parte de codigo de stmt2 aparentemente no se utiliza y parece una prueba antigua
			// Creo que se puede eliminar

			stmt2 = conn.createStatement();
			//stmt2 = conn.prepareStatement("SELECT d_comarca FROM carto.t101d_comarcas");
			ResultSet rs = stmt2.executeQuery("SELECT row_to_json(fc)\n" +
					" FROM ( SELECT 'FeatureCollection' As type, array_to_json(array_agg(f)) As features\n" +
					" FROM (SELECT 'Feature' As type\n" +
					"    , ST_AsGeoJSON(lg.shape)::json As geometry\n" +
					"    , row_to_json((SELECT l FROM (SELECT d_comarca,C_COMARCA, SCALE) As l\n" +
					"      )) As properties\n" +
			"   FROM carto.t101d_comarcas As lg) As f) As fc;");
			while(rs.next()) {
				//String nombre = rs.getString("coordinates");
				System.out.println(rs);
				System.out.println(rs.getString("type"));
			}
			rs.close();
			stmt2.close();

			String aux = doc;

			Reader reader = new StringReader(aux);
			stmt.setCharacterStream(1, reader, aux.length());
			if (aux.length() > 4000) {
				stmt.setString(2, aux.substring(0, 4000));
				stmt.setString(3, "Y");
			} else {
				stmt.setString(2, aux);
				stmt.setString(3, "N");
			}

			stmt.executeUpdate();
			conn.commit();
		}
		catch(Exception ex){
			Tbl2Map.logger.error("Fallo al registrar query: "+ex.getMessage());
		}finally {
			try {
				if (conn!=null)
					conn.close();
			} catch (SQLException e2) {
				Tbl2Map.logger.error("Fallo al desconectar SQL: "+e2.getMessage());
			} catch (NullPointerException e2) {
				Tbl2Map.logger.error("Fallo null2 SQL: "+e2.getMessage());
			}
			try {
				if (stmt!=null)
					stmt.close();
			} catch (SQLException e3) {
				Tbl2Map.logger.error("Fallo al cerrar sentencia SQL: "+e3.getMessage());
			} catch (NullPointerException e3) {
				Tbl2Map.logger.error("Fallo null3 SQL: "+e3.getMessage());
			}
		}
		*/
	}

	private String generateData(String format, NodeList items, String type,   String layer, String filter, Connection conn, Graphics2D g2d) throws Exception {

		String resp = "";
		resp += getInicioLista(format,   layer); //inicioCabeceraRespuesta

		int j;
		for (j=0; j < items.getLength(); j++){
			resp += getInicioItem(format, type, "item" + j);

			// Lo primero es un <it> => Hay que saltarlo
			NodeList theItem = items.item(j).getChildNodes();

			String idValue = "";
			String idField = "";
			String color = "";
			String title = "";
			Vector<String> atribNames = new Vector<String>();
			Vector<String> atribValues = new Vector<String>();

			for (int k=0; k < theItem.getLength(); k++){
				Node firstLevelChild = theItem.item(k);

				if (firstLevelChild.getNodeName().equals("_val")) {
					idValue = firstLevelChild.getTextContent();
				} else if (firstLevelChild.getNodeName().equals("_fld")) {
					idField = firstLevelChild.getTextContent();
				} else if (firstLevelChild.getNodeName().equals("_rgb")) {
					color = firstLevelChild.getTextContent();
				} else if (firstLevelChild.getNodeName().equals("_tit")) {
					title = firstLevelChild.getTextContent();
				} else {
					atribNames.add(firstLevelChild.getNodeName());
					atribValues.add(firstLevelChild.getTextContent());
				}
			}



			//Geometria y atributos unidos despues

			resp += getGeometry(format, type, idValue, idField, color, title, j, g2d, conn);

			resp += getAttributes(format, atribNames, atribValues);

			resp += getFinItem(format, type, j < items.getLength()-1);

		}
		resp += getFinLista(format);


		return resp;
	}
	private ArrayList<String> generateLegend(NodeList items, String legendTitle, int maxItems) throws Exception {
	Tbl2Map.logger.info("legend max:"+maxItems);
	Tbl2Map.logger.info("items:"+items.getLength());
		int rectH=15;
		int rectW=20;
		int left=10;
		int margin=10;
		int titleHeigth=15;
		int height=Math.min(items.getLength(),maxItems)*(rectH+margin)+titleHeigth;
		int width=200;
		BufferedImage img = new BufferedImage(width,height, BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = img.createGraphics();
		g2d.setComposite(AlphaComposite.Clear);
		g2d.fillRect(0, 0, new Integer(width),new Integer(height));
		g2d.setComposite(AlphaComposite.Src);
		g2d.setColor(Color.BLACK);
		g2d.setFont(new Font("Sans-Serif", Font.BOLD, 13));
		g2d.drawString(legendTitle, left,titleHeigth);
		int j;
		ArrayList<String> leyendas = new ArrayList<String>();
		int count=0;
		for (j=0; j < items.getLength(); j++){
			if (count>=maxItems){
				
				g2d.dispose();
				String filename = UUID.randomUUID().toString() + ".png";
				String resultado = Tbl2Map.properties.getProperty("imgs_path")+ filename;
				File output = new File(resultado);
				Tbl2Map.logger.info("otra leyenda:"+resultado);
				ImageIO.write(img, "png", output);
				leyendas.add(resultado);
				count=0;
				height=Math.min(items.getLength()-maxItems*(count+1),maxItems)*(rectH+margin)+titleHeigth;
				img = new BufferedImage(width,height, BufferedImage.TYPE_INT_ARGB);
				 g2d = img.createGraphics();
				g2d.setComposite(AlphaComposite.Clear);
				g2d.fillRect(0, 0, new Integer(width),new Integer(height));
				g2d.setComposite(AlphaComposite.Src);
				g2d.setColor(Color.BLACK);
				g2d.setFont(new Font("Sans-Serif", Font.BOLD, 13));
				g2d.drawString(legendTitle, left,titleHeigth);
			}
			// Lo primero es un <it> => Hay que saltarlo
			NodeList theItem = items.item(j).getChildNodes();

			String color = "";
			String title = "";

			for (int k=0; k < theItem.getLength(); k++){
				Node firstLevelChild = theItem.item(k);

				if (firstLevelChild.getNodeName().equals("_rgb")) {
					color = firstLevelChild.getTextContent();
				} else if (firstLevelChild.getNodeName().equals("_tit")) {
					title = firstLevelChild.getTextContent();
				}
			}
			g2d.setColor(Color.decode(color));
			//g2d.setBackgroundColor(Color.RED);
			g2d.fillRect(left, titleHeigth+margin+count*(rectH+10),rectW, rectH);
			g2d.setColor(Color.BLACK);
			g2d.setFont(new Font("Sans-Serif", Font.BOLD, 12));
			g2d.drawString(title, left+rectW+margin,margin-2+rectH+count*(rectH+10)+titleHeigth);
			count++;

		}
		g2d.dispose();
		String filename = UUID.randomUUID().toString() + ".png";
		String resultado = Tbl2Map.properties.getProperty("imgs_path")+ filename;
		File output = new File(resultado);
		ImageIO.write(img, "png", output);
		leyendas.add(resultado);
		Tbl2Map.logger.info("otra leyenda:"+resultado);
		Tbl2Map.logger.info("leyendas"+leyendas.size());
		return leyendas;
	}

	private String getInicioLista(String _format, String layer) {

		String resp = "";
		if (_format.equals("GML")) {
			resp += "<?xml version=\"1.0\" encoding=\"utf-8\"?>";
			resp += "<ogr:FeatureCollection xmlns:ogr=\"http://ogr.maptools.org/\" xmlns:gml=\"http://www.opengis.net/gml\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">";

			/*
        	    <gml:boundedBy srsName=\"EPSG:25830\">
					<gml:Box>
  						<gml:coord><gml:X>581750.9374838568</gml:X><gml:Y>4413136.00010211</gml:Y></gml:coord>
  						<gml:coord><gml:X>810739.000226472</gml:X><gml:Y>4755089.00032624</gml:Y></gml:coord>
					</gml:Box>
					</gml:boundedBy>
			 */

		} else if (_format.equals("GeoJSON")) {
			resp += "{ \"type\": \"FeatureCollection\",";
			resp += "\n  \"crs\": {";
			resp += "\n     \"type\": \"name\",";
			resp += "\n     \"properties\": {";
			resp += "\n        \"name\": \"urn:ogc:def:crs:EPSG:25830\"";
			resp += "\n      }";
			resp += "\n  },";
			resp += "\n  \"features\": [\n";
		}else if (_format.equals("KML")) {
			resp += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
			
			resp += "<kml xmlns=\"http://earth.google.com/kml/2.0\">\n";
			
			resp += "<Document>\n";
			
		} else if (_format.equals("SHP")) {
		} else if ((_format.equals("JPG")) || (_format.equals("PNG"))) {

			resp += "<sld:StyledLayerDescriptor version=\"1.0.0\" xmlns:sld=\"http://www.opengis.net/sld\" xmlns:ogc=\"http://www.opengis.net/ogc\">\n";
			resp += "<sld:NamedLayer>\t\n" +
			//"\t\t<sld:Name>" + _type + "</sld:Name>\n" +
			"\t\t<sld:Name>" + layer + "</sld:Name>\n" +
			"\t\t<sld:UserStyle>\n" +
			"\t\t<sld:Name>Nombre</sld:Name>\n" +
			"\t\t<sld:Title>Titulo</sld:Title>\n" +
			"\t\t<sld:FeatureTypeStyle>\n";
		}
		return resp;
	}
	private String getFinLista(String _format) {

		String resp = "";
		if (_format.equals("GML")) {
			resp += "</ogr:FeatureCollection>";
		} else if (_format.equals("GeoJSON")) {
			resp += "\n	]";
			resp += "\n}";
		} else if (_format.equals("KML")) {
			resp += "</Document>\n";
			resp += "</kml>\n";
		} else if (_format.equals("SHP")) {
		} else if ((_format.equals("JPG")) || (_format.equals("PNG"))) {
			resp += "\t\t</sld:FeatureTypeStyle>\n" +
			"\t\t</sld:UserStyle>\n" +
			"\t</sld:NamedLayer>\n" +
			"</sld:StyledLayerDescriptor>\n";
		}
		return resp;
	}

	private String getInicioItem(String _format, String _type, String _id) {

		String resp = "";
		if (_format.equals("GML")) {
			resp += "<gml:featureMember>";
			resp += "<ogr:" + _type + " gml:id=\"" + _id + "\">";
		} else if (_format.equals("KML")) {
			resp += "<Placemark>";
		} else if (_format.equals("GeoJSON")) {
			resp += "\n 		{ 	\"type\": \"Feature\",";
		} else if (_format.equals("SHP")) {
		} else if ((_format.equals("JPG")) || (_format.equals("PNG"))) {
		}
		return resp;
	}

	private String getFinItem(String _format, String _type, boolean _notIsLast) {

		String resp = "";
		if (_format.equals("GML")) {
			resp += "</ogr:" + _type + ">";
			resp += "</gml:featureMember>";
		} else if (_format.equals("GeoJSON")) {
			resp += "\n	 	}";
			if (_notIsLast) {
				resp += ",";
			}
		}else if (_format.equals("KML")) {
			resp += "</Placemark>";
			
		} else if (_format.equals("SHP")) {
		} else if ((_format.equals("JPG")) || (_format.equals("PNG"))) {
		}
		return resp;
	}

	private String getAttributes(String _format, Vector<String> _atribNames, Vector<String> _atribValues) {

		String resp = "";
		if (_format.equals("GML")) {
			for (int i = 0; i < _atribNames.size(); i++) {
				resp += "<ogr:" + _atribNames.get(i) + ">" + _atribValues.get(i) +"</ogr:" + _atribNames.get(i) + ">";
			}
		}else if (_format.equals("KML")) {
			resp += ("\t\t<ExtendedData>\n");
			
			for (int i = 0; i < _atribNames.size(); i++) {
				resp += "\t\t\t<Data name=\""+_atribNames.get(i)+"\"><value>"+ _atribValues.get(i) +"</value></Data>\n";
			}
			resp += ("\t\t</ExtendedData>\n");
		} else if (_format.equals("GeoJSON")) {
			if (_atribNames.size() > 0) {
				resp += ",\n	 		\"properties\": {";
				for (int i = 0; i < _atribNames.size(); i++) {
					resp += "\n	 			\"" + _atribNames.get(i) + "\": \"" + _atribValues.get(i) + "\"";
					if (i < _atribNames.size()-1) {
						resp += ",";
					}
				}
				resp += "\n	 		}";
			}
		} else if (_format.equals("SHP")) {
		} else if ((_format.equals("JPG")) || (_format.equals("PNG"))) {
		}
		return resp;
	}

	private String getGeometry(String _format, String _type, String _idValue, String _idField, String _color, String title, int _idx, Graphics2D g2d, Connection conn) throws Exception {

		Statement st = null;
		String s = null;
		String nombreTabla = null;
		String resp = "";

		String shapeFieldName = "shape";
		String idFieldName = "objectid";

		if(_type.equals("Comarca")) {
			nombreTabla = "carto.t101d_comarcas";
		}
		else if (_type.equals("Municipio")) {
			nombreTabla = "carto.t101e_municipios";
		}
		else if (_type.equals("Provincia")) {
			nombreTabla = "carto.t101d_provincias";
		}
		else if (_type.equals("Localidad")) {
			nombreTabla = "carto.v112_nucleos";
		}
		else if (_type.equals("Calle")) {
			nombreTabla = "carto.t111_ejes";
		}
		else if (_type.equals("Portal")) {
			nombreTabla = "carto.t111_troidesvisor";
		}
		else if (_type.equals("BusParUrb")) {
			nombreTabla = "cat_r.t103_parcelacat";
		}
		else if (_type.equals("BusSIGPAC")) {
			nombreTabla = "sigpac.tsigpac_parcelas";
		}
		else if (_type.equals("Toponimo")) {
			nombreTabla = "carto.t102_entidades";
			shapeFieldName = "geom";
			idFieldName = "gid";
		}
		else if (_type.equals("Cuad5k")) {
			nombreTabla = "carto.t101b_hojas5000";
		}
		else if (_type.equals("Cuad50k")) {
			nombreTabla = "carto.t101b_hojas50000";
		}
		else if (_type.equals("CuaUTM10k")) {
			nombreTabla = "carto.t101b_cuadricula10000";
		}

		if (_format.equals("GeoJSON")) {
			if (_type.equals("Coordenadas")) {
				String[] coords = _idValue.split(":");
				resp += "\"geometry\":";
				resp += "{\"type\":\"Point\",\"coordinates\":[" + coords[0] + "," + coords[1] + "]}";
			} else {
				st = conn.createStatement();
				ResultSet rs = st.executeQuery("SELECT ST_AsGeoJSON(lg." + shapeFieldName + ")::json As features\n" +
						"FROM "+nombreTabla+" As lg WHERE lg." + idFieldName + "=" + _idValue);

				while (rs.next()) {
					s = rs.getString("features");

					resp += "\"geometry\":";
					resp += s;
				}
				rs.close();
				st.close();
			}
		} else if (_format.equals("GML")) {
			if (_type.equals("Coordenadas")) {
				String[] coords = _idValue.split(":");
				resp += "<ogr:geometryProperty>";
				resp += "<gml:Point srsName=\"EPSG:25830\"><gml:coordinates>" + coords[0] + "," + coords[1] + "</gml:coordinates></gml:Point>";
				resp += "</ogr:geometryProperty>";
			} else {
				st = conn.createStatement();
				ResultSet rs = st.executeQuery("SELECT ST_AsGML(" + shapeFieldName + ") As features\n" +
						"FROM "+nombreTabla+" As lg WHERE lg." + idFieldName + "=" + _idValue);

				while (rs.next()) {
					resp += "<ogr:geometryProperty>";
					s = rs.getString("features");
					resp += s;
					resp += "</ogr:geometryProperty>";
				}
				rs.close();
				st.close();
			}
		}else if (_format.equals("KML")) {
			if (_type.equals("Coordenadas")) {
				String[] coords = _idValue.split(":");
				resp += "<Point>";
				resp += " <coordinates>" + coords[0] + "," + coords[1] + "</coordinates>";
				resp += "</Point>";
			} else {
				st = conn.createStatement();
				ResultSet rs = st.executeQuery("SELECT ST_AsKML(" + shapeFieldName + ") As features\n" +
						"FROM "+nombreTabla+" As lg WHERE lg." + idFieldName + "=" + _idValue);

				while (rs.next()) {
					
					s = rs.getString("features");
					resp += s;
					
				}
				rs.close();
				st.close();
			}
		}
		else if (_format.equals("SHP")) {
			if (_type.equals("Coordenadas")) {
				String[] coords = _idValue.split(":");
				resp += "POINT(" + coords[0] + " " + coords[1] + ")";
			} else {
				st = conn.createStatement();

				ResultSet rs = st.executeQuery("SELECT ST_AsText(" + shapeFieldName + ") As features\n" +
						"FROM "+nombreTabla+" As lg WHERE lg." + idFieldName + "=" + _idValue);

				while (rs.next()) {
					s = rs.getString("features");
					resp += s;
				}

				rs.close();
				st.close();
			}
		}

		else if ((_format.equals("JPG")) || (_format.equals("PNG"))) {
			String[] fieldList;
			if (_type.equals("Coordenadas") || _type.equals("Toponimo")) {

				g2d.setPaint(Color.decode(_color));
				String[] coords = _idValue.split(":")[0].split(" ");
				g2d.fillOval(parseInt(coords[0])-3,parseInt(coords[1])-3,6,6);

			} else {
				resp += "\t\t\t<sld:Rule>\n" +
				"\t\t\t\t<ogc:Filter>\n" +
				"\t\t\t\t\t<ogc:Or>\n";
				/*  if(_type.equals("Portal")){
                    //fieldList = _idValue.split(" ");
                    String numberOnly= _idValue.replaceAll("[^0-9]", "");
                    int portal = parseInt(numberOnly);
                    String calle = _idValue.substring(0, _idValue.indexOf(numberOnly)-1);
                    resp += "\t\t\t\t\t<ogc:And>\n" +
                            "\t\t\t\t\t\t<ogc:PropertyIsEqualTo>\n" +
                            "\t\t\t         \t<ogc:PropertyName>c_mun_via</ogc:PropertyName>\n" +
                            "\t\t\t      \t<ogc:Literal>" + calle + "</ogc:Literal>\n" +
                            "\t\t\t\t\t\t</ogc:PropertyIsEqualTo>\n" +
                            "\t\t\t\t\t\t<ogc:PropertyIsEqualTo>\n" +
                            "\t\t\t         \t<ogc:PropertyName>numero</ogc:PropertyName>\n" +
                            "\t\t\t      \t<ogc:Literal>" + portal + "</ogc:Literal>\n" +
                            "\t\t\t\t\t\t</ogc:PropertyIsEqualTo>\n" +
                            "\t\t\t\t\t</ogc:And>\n";
                }
                else {*/
				fieldList = _idValue.split(",");
				for (int i = 0; i < fieldList.length; i++) {
					resp += "\t\t\t\t\t\t<ogc:PropertyIsEqualTo>\n" +
					"\t\t\t         \t<ogc:PropertyName>objectid</ogc:PropertyName>\n" +
					"\t\t\t      \t<ogc:Literal>" + fieldList[i] + "</ogc:Literal>\n" +
					"\t\t\t\t\t\t</ogc:PropertyIsEqualTo>\n";
				}
				//}
				resp += "\t\t\t\t\t</ogc:Or>\n" +
				"\t\t\t\t</ogc:Filter>\n";
				if (_type.equals("Toponimo") || _type.equals("Portal")) {
					resp += "        <sld:PointSymbolizer>\n" +
					"           <sld:Graphic>\n" +
					"               <Mark>\n" +
					"                   <WellKnownName>circle</WellKnownName>" +
					"                   <Fill>\n" +
					"                       <CssParameter name=\"fill\">" + _color + "</CssParameter>\n" +
					"                   </Fill>" +
					"               </Mark>\n" +
					"           </sld:Graphic>\n" +
					"        </sld:PointSymbolizer>\n";
				}
				else {
					resp += "\t\t\t\t<sld:PolygonSymbolizer>\n" +
					"\t\t\t\t\t<sld:Fill>\n" +
					"\t\t\t\t\t\t<sld:CssParameter name=\"fill\">" + _color + "</sld:CssParameter>\n" +
					"\t\t\t\t\t\t<sld:CssParameter name=\"fill-opacity\">0.7</sld:CssParameter>\n" +
					"\t\t\t\t\t</sld:Fill>\n" +
					"\t\t\t\t\t<sld:Stroke>\n" +
					"\t\t\t\t\t\t<sld:CssParameter name=\"stroke\">#000000</sld:CssParameter>\n" +
					"\t\t\t\t\t\t<sld:CssParameter name=\"stroke-width\">1</sld:CssParameter>\n" +
					"\t\t\t\t\t</sld:Stroke>\n" +
					"\t\t\t\t</sld:PolygonSymbolizer>\n";
				}
				resp += "\t\t\t</sld:Rule>\n";
			}
		}

		return resp;
	}


	private Node navigateNextItem(NodeList node, String txt)
	{
		for (int i=0; i<node.getLength(); i++){
			if (node.item(i).getNodeName().equals(txt)) {
				return node.item(i);
			}
		}
		return null;
	}
}
