package es.ideariumConsultores.tbl2map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class XLS2CSV extends HttpServlet {
	/**
	 * peticion por GET
	 * @param request fichero excel a convertir 
	 * @param response fichero convertido a CSV 
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException
	{
		doPost(request, response);
	}

	/**
	 * peticion por POST 
	 * @param request fichero excel a convertir 
	 * @param response fichero convertido a CSV 
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
	throws ServletException
	{
		try {
			String txt = "hola";
			response.setContentType("application/text; charset=\"utf-8\"");
			response.getWriter().println(txt);
			response.getWriter().flush();	
		} catch(Throwable e) {
				Tbl2Map.logger.error("XML2CSV: Error en la conversion a CSV ", e);
				System.out.println("Error en la conversion a CSV "+ e.toString());
				try {
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				}
				catch(Exception ex){
					
				}
		}
	}
}
