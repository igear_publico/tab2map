package es.ideariumConsultores.tbl2map;

import java.io.File;
import java.io.InputStream;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.DiskFileUpload;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class XLS2CSVServlet extends HttpServlet {
	private static final long MAX_SIZE = 1024*1024;
	/**
	 * peticion por GET
	 * @param request fichero excel a convertir 
	 * @param response fichero convertido a CSV 
	 */
	public void doGet(HttpServletRequest request, HttpServletResponse response)
	throws ServletException
	{
		doPost(request, response);
	}

	/**
	 * peticion por POST 
	 * @param request fichero excel a convertir 
	 * @param response fichero convertido a CSV 
	 */
	public void doPost(HttpServletRequest request, HttpServletResponse response)
	throws ServletException
	{
		try {
			/*
System.out.println("0");
			// Create a factory for disk-based file items
			DiskFileItemFactory factory = new DiskFileItemFactory();

System.out.println("1");
			// Configure a repository (to ensure a secure temp location is used)
			ServletContext servletContext = this.getServletConfig().getServletContext();
			File repository = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
			factory.setRepository(repository);

System.out.println("2");
			// Create a new file upload handler
			ServletFileUpload upload = new ServletFileUpload(factory);

System.out.println("3");
			// Parse the request
			List<FileItem> items = upload.parseRequest(request);
			
System.out.println("4");
			// Process the uploaded items
			Iterator<FileItem> iter = items.iterator();
			while (iter.hasNext()) {
System.out.println("5");
			    FileItem item = iter.next();
			    InputStream uploadedStream = item.getInputStream();

			    InputStream filecontent = item.getInputStream();
			    XSSFWorkbook wb = new XSSFWorkbook(filecontent);
			  		// get first worksheet
				XSSFSheet sheet = wb.getSheetAt(0);
			  		// get cell at top left
				System.out.println(sheet.getRow(0).getCell(0).getStringCellValue());

			    uploadedStream.close();
			}	
*/
			DiskFileUpload fu = new DiskFileUpload();
			fu.setSizeMax(MAX_SIZE);
			fu.setSizeThreshold(1024);
			fu.setRepositoryPath(System.getProperty("java.io.tmpdir"));
			List fileList = fu.parseRequest(request);
			InputStream uploadedFileStream = null;
			String uploadedFileName = null; // name of file on user's computer
			
			String txt = "";

			for (Iterator i = fileList.iterator(); i.hasNext(); ) {
				FileItem fi = (FileItem)i.next();
			    if (! fi.isFormField()) {
			      if (fi.getSize() < 1) {
			        throw new Exception("No file was uplaoded");
			      }

				  InputStream filecontent = fi.getInputStream();
				  HSSFWorkbook wb = new HSSFWorkbook(filecontent);
				  		// get first worksheet
				  HSSFSheet source = wb.getSheetAt(0);
				  		// get cell at top left
				  for (int k = source.getFirstRowNum(); k <= source.getLastRowNum(); k++) {
					  HSSFRow srcRow = source.getRow(k);
					  if (srcRow != null) {
						for (int j = srcRow.getFirstCellNum(); j <= srcRow.getLastCellNum(); j++) {
							HSSFCell cell = source.getRow(k).getCell(j);
							if (cell != null) {
								cell.setCellType(Cell.CELL_TYPE_STRING);
								if (cell.getStringCellValue().indexOf(";") != -1) {
									txt += "\"" + cell.getStringCellValue() + "\"";
								} else {
									txt += cell.getStringCellValue();
								}
								if (j < (srcRow.getLastCellNum()-1)) {
									txt += ";";
								}
							}
						}
						txt += "\n";
					  }
				  }
				  //System.out.println(sheet.getRow(0).getCell(0).setCellType(Cell.CELL_TYPE_STRING).getNumericCellValue());

				  filecontent.close();
			    }
			}

			response.setContentType("application/text; charset=\"utf-8\"");
			System.out.println(txt);
			response.getWriter().println(txt);
			response.getWriter().flush();	

		} catch(Throwable e) {
				Tbl2Map.logger.error("XML2CSV: Error en la conversion a CSV ", e);
				System.out.println("Error en la conversion a CSV "+ e.toString());
				try {
					response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
				}
				catch(Exception ex){
					
				}
		}
	}
/*	
	protected String getCellValue(Cell cell) {
		switch (cell.getCellType()) {
		 	case HSSFCell.CELL_TYPE_STRING: return cell.getStringCellValue();
		 	case HSSFCell.CELL_TYPE_NUMERIC: return cell.getNumericCellValue();
		 	case HSSFCell.CELL_TYPE_BLANK: return ""; 
		 	case HSSFCell.CELL_TYPE_BOOLEAN: return cell.getBooleanCellValue();
		 	case HSSFCell.CELL_TYPE_ERROR: return cell.getErrorCellValue();
		 	case HSSFCell.CELL_TYPE_FORMULA: return cell.getCellFormula();
			default: return null;
		}
	}
	*/
}
