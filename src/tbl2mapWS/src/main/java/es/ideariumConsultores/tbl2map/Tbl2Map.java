package es.ideariumConsultores.tbl2map;

import java.awt.image.BufferedImage;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Properties;
import java.util.StringTokenizer;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageInputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * Clase que implementa el servicio de tabla a mapa (basado en es.ideariumConsultores.search.simple.BusquedaSimple)
 * @author dportoles 
 *
 */
public class Tbl2Map {

	public static Properties properties;  // archivo de configuraciï¿½n del servicio
	public static Logger logger = Logger.getLogger("tbl2mapWS");
	/**
	 * carga el fichero de propiedades
	 * @param propertiesPath ruta completa del fichero de propiedades
	 */
	static public void initProperties(String propertiesPath){
		
		properties = new Properties();
		try{
			logger.info(propertiesPath);
			properties.load(new FileInputStream(propertiesPath));
			
		}
		catch(Exception ex){
			logger.error("Error al cargar el fichero de propiedades "+propertiesPath, ex);
		//	ex.printStackTrace();	
		}
	}
	
	/**
	 * Indica si el texto representa un número entero
	 * @param texto
	 * @return
	 */
	static protected boolean esEntero(String texto){
		try{
			Integer.parseInt(texto);
		}
		catch(NumberFormatException ex){
			return false;
		}
		return true;
	}

	/**
	 * Indica si el texto representa un número real
	 * @param texto
	 * @return
	 */
	static protected boolean esReal(String texto){
		try{
			Double.parseDouble(texto);
		}
		catch(NumberFormatException ex){
			return false;
		}
		return true;
	}

	/**
	 * indica si el texto comienza por un número
	 * @param texto
	 * @return
	 */
	static protected boolean empiezaPorNumero(String texto){
		return esEntero(texto.substring(0,1))||
		(texto.substring(0,1).equalsIgnoreCase("-")&& esEntero(texto.substring(1,2)));
	}

	/**
	 * indica si el texto es un cÃ¯Â¿Â½digo que empieza por el prefijo indicado seguido de números
	 * @param texto texto a comprobar si es cÃ¯Â¿Â½digo
	 * @param prefijo prefijo por el que debe comenzar el texto para considerarlo cÃ¯Â¿Â½digo
	 * @return true si texto empieza por el prefijo y el resto son números, false en caso contrario
	 */
	static protected boolean esCodigo(String texto, String prefijo){
		if(texto.toUpperCase().startsWith(prefijo.toUpperCase())){
			return esEntero(texto.toUpperCase().replaceFirst(prefijo.toUpperCase(), ""));
		}
		return false;
	}

	/**
	 * Devuelve el número de apariciones de una cadena de texto dentro de otra
	 * @param texto cadena de texto en la que contar
	 * @param textoAContar cadena de texto cuyas apariciones hay que contar
	 * @return número de apariciones de textoAContar en texto
	 */
	static protected int numeroApariciones(String texto, String textoAContar){
		int count=0;
		int pos =texto.indexOf(textoAContar);
		while (pos>=0){
			count++;
			pos =texto.indexOf(textoAContar,pos+1);
		}
		return count;
	}
	
	/**
	 * Sustituye  el espacio en blanco por %20
	 * @param txt Cadena de texto a normalizar
	 * @return cadena sin espacios en blanco (sustituidos por %20)
	 */
	static protected String normalizaCadena(String txt, boolean replaceBlank){
		String txt_norm =txt;
		try{

		 txt_norm =URLDecoder.decode(txt,"UTF-8");
		}
		catch(UnsupportedEncodingException ex){
			ex.printStackTrace();
		}
		txt_norm = txt_norm.replace("ÃƒÂ¡", "a").replace("ÃƒÂ©", "e").replace("ÃƒÂ­", "i").replace("ÃƒÂ³", "o").replace("ÃƒÂº", "u");
		txt_norm = txt_norm.replace("Ãƒï¿½", "A").replace("Ãƒâ€°", "E").replace("Ãƒï¿½", "I").replace("Ãƒâ€œ", "O").replace("ÃƒÅ¡", "U");
		txt_norm = txt_norm.replace("ÃƒÅ“", "U").replace("ÃƒÂ¼", "u").replace("ÃƒÂ±", "ny").replace("Ãƒâ€˜", "NY").replace("Ã‚Âº", "\u00BA");
		if (replaceBlank){
		txt_norm = txt_norm.replace(" ", "%20");
		}
		return txt_norm;

	}
	static protected String normalizaCadena(String txt){
		return normalizaCadena(txt,true);
	}

	/**
	 * Devuelve la cadena que representa la fonï¿½tica del texto pasado como parï¿½metro
	 * @param txt cadena de la que se obtendrï¿½ la fonetica. No debe contener acentos ni Ã± (sustituir por n)
	 * @return cadena de texto que representa la fonetica del texto pasado como parï¿½metro
	 */
	static protected String getFonetica(String txt){
		
		try{
			URL url = new URL(properties.getProperty("BD_GIS_FONETICA_URL")+normalizaCadena(txt));
			HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("GET");

			//read the result from the server
			BufferedReader rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			//StringBuilder sb = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null)
			{
				if ((!line.startsWith("<"))&&(line.length()>0)){
				//	sb.append(line + '\n');
					connection.disconnect();
					//System.err.println(txt+"->"+line);
					return line;
				}
			}
		/*	connection.disconnect();
			return sb.toString();*/
		}
		catch(MalformedURLException ex){
			logger.error("Error al obtener la fonÃ©tica del texto "+txt, ex);
		}
		catch(IOException ex){
			logger.error("Error al obtener la fonÃ©tica del texto "+txt, ex);
		}
		return txt;		// si hay error se devuelve el propio texto de entrada
	}

	
	
	
	static public String completeCode(String code, int length){
		String result = code;
		while (result.length()<length){
			result ="0"+result;
		}
		return result;
	}

	static public void registraQuery(String txt) {
		registraQuery(txt, "");
	}

	static public void registraQuery(String txt, String app) {
		String charset = "UTF-8";
		
		try {
			String query = String.format("literal=%s&app=%s", URLEncoder.encode(txt, charset), app);
			URL url = new URL(properties.getProperty("BD_GIS_REGISTRABUSQUEDA_URL") + query);

			HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("GET");
		    connection.getOutputStream().close();
		} catch (Exception e) {
			logger.error("Error al registrar la búsqueda "+ e);
		} finally {
		}

		/*
		connection.setDoOutput(true);
		connection.setRequestProperty("Accept-Charset", this.charset);
		connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded;charset=" + charset);
		    connection.getOutputStream().write(query.getBytes(charset));
		*/
	}





	protected static String doSearch(String parametrized_url){
		return doSearch(parametrized_url,0);
	}
	
	
	protected static String doSearch(String parametrized_url,int timeout){
		try{
			URL url = new URL(parametrized_url);
			HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("GET");
			connection.setDoOutput(true);
			
			connection.setConnectTimeout(timeout);
			
			logger.info(url);

			/*connection.setRequestProperty("Content-Type", "text/xml"); 
			connection.setRequestProperty("charset", "utf-8");
			connection.setRequestProperty("Content-Length", "" + Integer.toString(request.getBytes().length));
			*/
			//connection.setRequestProperty("charset", "utf-8");
			connection.connect();
		/*	OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
			wr.write(request);
			wr.flush();*/

			//read the result from the server
			BufferedReader rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null)
			{
				sb.append(line + '\n');
			}


			connection.disconnect();
			
			return sb.toString();
		}
		catch(MalformedURLException ex){
			logger.error("Error en la consulta "+parametrized_url, ex);
		}
		catch(java.net.SocketTimeoutException ex){
			logger.error("Tiempo máximo de respuesta agotado "+parametrized_url, ex);
		}
		catch(IOException ex){
			logger.error("Error en la consulta "+parametrized_url, ex);
		}
		return "";
	}
	
	/**
	 * Realiza una peticiï¿½n POST a un servicio Web y guarda el resultado en fichero (imagen)
	 * @param service_url URL del servicio
	 * @param request peticiï¿½n
	 * @return respuesta del servicio
	 */
	public static String doSearch(String service_url, String request){
		try{
			URL url = new URL(service_url);
			HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);

			logger.info(request);

			connection.setRequestProperty("Content-Type", "text/xml"); 
			connection.setRequestProperty("charset", "utf-8");
			connection.setRequestProperty("Content-Length", "" + Integer.toString(request.getBytes().length));
			connection.connect();
			OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
			wr.write(request);
			wr.flush();

			//read the result from the server
			//BufferedReader rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			/*InputStreamReader isr = new InputStreamReader(connection.getInputStream());
			BufferedImage bi = ImageIO.read((ImageInputStream) new InputStreamReader(connection.getInputStream()));
			ImageIO.write(bi, "png", new File(path));*/


			//File f = File.createTempFile("C:\\imageTbl2Map", ".png");
			String filename = UUID.randomUUID().toString() + ".png";
			String path = properties.getProperty("imgs_path")+ filename;
			FileOutputStream outFile = new FileOutputStream(path);
			logger.debug(connection.getResponseMessage());

			InputStream is =connection.getInputStream();

			byte[] fileData = new byte[256];
			int count;
			while ((count=is.read(fileData))>0)
			{
				outFile.write(fileData,0,count);
			}
			outFile.flush();
			outFile.close();

			return path;
			/*StringBuilder sb = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null)
			{
				sb.append(line + '\n');
			}

			//System.out.println(sb.toString());
			connection.disconnect();
			return sb.toString();*/
		}
		catch(MalformedURLException ex){
			logger.error("Error en la consulta "+request+ " al servicio "+service_url, ex);
		}
		catch(IOException ex){
			logger.error("Error en la consulta "+request+ " al servicio "+service_url, ex);
		}
		return "";
	}
	
	public static BufferedImage doImgRequest(String urlRequest){
		try{
			URL url = new URL(urlRequest.replaceAll("&amp;","&"));
			HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("GET");
			connection.setDoOutput(true);

			logger.info(urlRequest);

			connection.setRequestProperty("Content-Type", "text/xml"); 
			connection.setRequestProperty("charset", "utf-8");
			
			connection.connect();
			

			//read the result from the server
			//BufferedReader rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));

			//InputStreamReader isr = new InputStreamReader(connection.getInputStream());
			BufferedImage bi = ImageIO.read(connection.getInputStream());
			//ImageIO.write(bi, "png", new File("C:/wms.png"));


			//File f = File.createTempFile("C:\\imageTbl2Map", ".png");
			return bi;

			
		}
		catch(MalformedURLException ex){
			logger.error("Error en la consulta "+urlRequest, ex);
		}
		catch(IOException ex){
			logger.error("Error en la consulta "+urlRequest, ex);
		}
		return null;
	}

}
