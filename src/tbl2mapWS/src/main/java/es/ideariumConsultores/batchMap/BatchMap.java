package es.ideariumConsultores.batchMap;

import static java.lang.Integer.parseInt;

import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.UUID;
import java.util.Vector;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.imageio.ImageIO;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.fop.servlet.MapaPDFServlet;
import org.apache.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import es.ideariumConsultores.tbl2map.Tbl2Map;

/**
 * Clase que implementa el servicio de tabla a mapa (basado en es.ideariumConsultores.search.simple.BusquedaSimple)
 * @author dportoles 
 *
 */
public class BatchMap extends Thread{
	static public int CANCELLED=-1;
	static public int ERROR=-9;
	static public int FINISHED=Integer.MAX_VALUE;
	protected static Properties properties;  // archivo de configuraciï¿½n del servicio
	public static Logger logger = Logger.getLogger("tbl2mapWS");
	static HashMap<String,Integer> progresos = new  HashMap<String,Integer> ();

	MapaPDFServlet pdfServlet;
	NodeList clientData;

	String capas;
	String leyendas;
	String ambito;
	String formato;
	String orientacion;
	String tamano;
	String sld;
	ArrayList<String> legendH;
	ArrayList<String> legendV;
	String titulo;
	File temp_dir;
	int mapWidth;
	int mapHeight;
	String jobid;
	ArrayList<String> capas_x_wms = new ArrayList<String>() ;
	/**
	 * carga el fichero de propiedades
	 * @param propertiesPath ruta completa del fichero de propiedades
	 */
	static public void initProperties(String propertiesPath){

		properties = new Properties();
		try{
			properties.load(new FileInputStream(propertiesPath));
			logger.info(propertiesPath);
			logger.info("TEMP_DIR="+properties.getProperty("TEMP_DIR"));
			logger.info("MAPA_URL="+properties.getProperty("MAPA_URL"));
		}
		catch(Exception ex){
			logger.error("Error al cargar el fichero de propiedades "+propertiesPath, ex);
			//	ex.printStackTrace();	
		}
		
	}


	static public boolean checkMapa(String mapa){
		return (properties.getProperty("URL_"+mapa+"_1")!=null);

	}
	static public boolean checkAmbito(String ambito){
		return (properties.getProperty(ambito+"_1_BBOX")!=null);

	}
	static public boolean checkFormato(String formato){
		return (formato.equalsIgnoreCase("jpg")||formato.equalsIgnoreCase("pdf"));

	}
	static public boolean checkJobId(String jobid){
		boolean valido = (progresos.get(jobid)==null);
		File temp_dir = new File(properties.getProperty("TEMP_DIR","/app/jboss/tab2mapWS/maps/temp")+"\\"+jobid);
		valido = valido && temp_dir.exists() &&(temp_dir.list().length ==0);

		return valido;
	}

	public BatchMap(String jobid,  String capas,  String leyendas, String ambito, String formato, String orientacion, String tamano, NodeList clientData,String sld, ArrayList<String> legendH,ArrayList<String> legendV,String titulo, MapaPDFServlet pdfServlet){
		this.jobid=jobid;

		this.ambito = ambito;
		this.formato = formato;
		this.sld = sld;
		this.clientData = clientData;
		this.legendH = legendH;
		this.legendV = legendV;
		this.pdfServlet = pdfServlet;
		this.titulo = titulo;
		if (orientacion == null){
			this.orientacion = "auto";
		}
		else{
			this.orientacion = orientacion;
		}
		if (tamano == null){
			this.tamano = "A4";
		}
		else{
			this.tamano = tamano;
		}
		if (capas == null){
			this.capas = "";
		}
		else{
			this.capas = capas;
		}
		if (leyendas == null){
			this.leyendas = "";
		}
		else{
			this.leyendas = leyendas;
		}
	}

	public void run() {
		try{
			progresos.put(jobid, 0);
			temp_dir = new File(properties.getProperty("TEMP_DIR","/app/jboss/tab2mapWS/maps/temp")+"/"+jobid);


			int numFiles = new Integer(properties.getProperty(ambito+"_COUNT"));
			for (int i=1;i<=numFiles;i++){
				if(progresos.get(jobid)!= CANCELLED){
					generateMap(i);
					progresos.put(jobid, i);
				}
				else{
					break;
				}
			}
			if(progresos.get(jobid)!= CANCELLED){
				progresos.put(jobid, FINISHED);//terminado
			}
		}
		catch(Exception ex){
			ex.printStackTrace();
			logger.error("Error en la generaci�n de los mapas", ex);
			progresos.put(jobid, ERROR);//error
		}
	}

	static public boolean isFinished(String jobid){
		Integer progreso = progresos.get(jobid);
		return ((progreso != null)&&((progreso==FINISHED)||(progreso==CANCELLED)));
	}
	static public byte[] getZippedFiles(boolean deleteTempDir, String jobid) throws IOException{
		if (!isFinished(jobid)){
			return null;
		}
		File temp_dir = new File(properties.getProperty("TEMP_DIR","/app/jboss/tab2mapWS/maps/temp")+"/"+jobid);
		File[] files =  temp_dir.listFiles();
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ZipOutputStream zos = new ZipOutputStream(baos);
		byte bytes[] = new byte[2048];


		for (File file : files) {
			zos.putNextEntry(new ZipEntry(file.getName()));
			FileInputStream bis = new FileInputStream(file);
			int bytesRead;
			while ((bytesRead = bis.read(bytes)) != -1) {
				zos.write(bytes, 0, bytesRead);
			}
			bis.close();

			if (deleteTempDir){
				try{
					file.delete();
				}
				catch(Exception ex){
					logger.error("No se pudo borrar el fichero temporal "+file.getAbsolutePath(), ex);
					ex.printStackTrace();
				}

			}
			zos.closeEntry();

		}
		zos.flush();
		baos.flush();
		zos.close();
		baos.close();
		if (deleteTempDir){
			try{
				temp_dir.delete();
			}
			catch(Exception ex){
				logger.error("No se pudo borrar la carpeta temporal "+temp_dir.getAbsolutePath(), ex);
				ex.printStackTrace();
			}
		}
		logger.debug("progresos "+progresos);
		progresos.remove(jobid);
		return baos.toByteArray();   
	}

	protected String getOrientacion(String bbox){
		if (orientacion.equalsIgnoreCase("vertical")){
			return "VERTICAL";
		}
		else if (orientacion.equalsIgnoreCase("horizontal")){
			return "HORIZ";

		}
		else{
			String[] bounds = bbox.split(",");
			double _minx = Double.parseDouble(bounds[0]);
			double _miny = Double.parseDouble(bounds[1]);
			double _maxx = Double.parseDouble(bounds[2]);
			double _maxy = Double.parseDouble(bounds[3]);
			double diffX = _maxx - _minx;
			double diffY = _maxy - _miny;
			if (diffX > diffY){
				return "HORIZ";
			}
			else{
				return "VERTICAL";
			}
		}
	}

	public static String upperSpecial2Hex(String text){
		String result = text.replaceAll("�", "&#193;");
		result = result.replace("�", "&#201;");
		result = result.replace("�", "&#205;");
		result = result.replace("�", "&#211;");
		result = result.replace("�", "&#218;");
		result = result.replace("�", "&#209;");
		result = result.replace("�", "&#220;");
		return result;
	}

	// para jpg
	public BufferedImage addPrintWMS(BufferedImage imgFile, String bbox, int width, int height) throws Exception{
		boolean hayCapas=false;

		BufferedImage biResultado = new BufferedImage(imgFile.getWidth(), imgFile.getHeight(), BufferedImage.TYPE_INT_ARGB); //suponiendo tamaño de imagen  y ARGB que soporta trasparencia

		Graphics g = biResultado.getGraphics();
		
		if (capas.length()>0){
			String[] incluir = capas.split("#");
			for (int i=0; i<incluir.length; i++){
				hayCapas=true;
				

				
				BufferedImage imBuff = Tbl2Map.doImgRequest(incluir[i].replaceAll("&LAYERS=", "&amp;LAYERS=")+"&amp;STYLES=&amp;SERVICE=WMS&amp;VERSION=1.1.1&amp;REQUEST=GetMap&amp;FORMAT=image%2Fpng&amp;TRANSPARENT=TRUE&amp;SRS=EPSG%3A25830&amp;BBOX="+bbox+"&amp;WIDTH="+width+"&amp;HEIGHT="+height);


				//ImageIO.write(imBuff, "PNG", new File("c:/wms.png"));

				g.drawImage(imBuff, 0, 0, null);

			}
		}
		g.drawImage(imgFile, 0, 0, null);
		if (hayCapas){
			return biResultado;
		}
		else{
			return imgFile;
		}


	}

	public BufferedImage addWMSLegend(BufferedImage imgFile) throws Exception{
		BufferedImage biResultado=new BufferedImage(imgFile.getWidth(), imgFile.getHeight(), BufferedImage.TYPE_INT_ARGB); //suponiendo tamaño de imagen  y ARGB que soporta trasparencia ;
		Graphics g = biResultado.getGraphics();
		g.drawImage(imgFile, 0, 0, null);
	
		g.dispose();
		String[] leys = this.leyendas.split("#");
		String[] wms = this.capas.split("#");
		for (int i=0; i<leys.length; i++){
			if (leys[i].length()>0){
				String[] labels = leys[i].split(",");
				String[] partesWMS=wms[i].split("=");
				String[] capasWMS=partesWMS[partesWMS.length-1].split(",");
				for (int j=0; j<labels.length ;j++){
										
					BufferedImage legendWMS = Tbl2Map.doImgRequest(wms[i]+"&SERVICE=WMS&REQUEST=GetLegendGraphic&format=image/png&VERSION=1.1.1&STYLES=&layer="+capasWMS[j]+"&width=20&height=20");
					BufferedImage temp = new BufferedImage(Math.max(biResultado.getWidth(),legendWMS.getWidth()), biResultado.getHeight()+legendWMS.getHeight()+30, BufferedImage.TYPE_INT_ARGB); //suponiendo tamaño de imagen  y ARGB que soporta trasparencia

					 g = temp.getGraphics();
					g.drawImage(biResultado, 0, 0, null);
					boolean texto=false;
					logger.info(labels[j]);
					if ((j==0) ||(!labels[j].trim().equalsIgnoreCase(labels[j-1].trim()))){
						logger.info("Pongo texto");
						texto=true;
						g.setColor(Color.BLACK);
						g.setFont(new Font("Sans-Serif", Font.BOLD, 12));
						g.drawString(labels[j], 1, biResultado.getHeight()+10);
					}
					g.drawImage(legendWMS, 0, biResultado.getHeight()+(texto ? 20:1), null);
					g.dispose();
					biResultado= temp;
				}
			}
		}

		return biResultado;
	}
	public String getPrintWMS(String bbox, int width, int height){

		String xml="";
		if (capas.length()>0){
			String[] incluir = capas.split("#");
			for (int i=0; i<incluir.length; i++){
			
				xml +="<mapa><laUrl>"+incluir[i].replaceAll("&LAYERS=", "&amp;LAYERS=")+"&amp;STYLES=&amp;SERVICE=WMS&amp;VERSION=1.1.1&amp;REQUEST=GetMap&amp;FORMAT=image%2Fpng&amp;TRANSPARENT=TRUE&amp;SRS=EPSG%3A25830&amp;BBOX="+bbox+"&amp;WIDTH="+width+"&amp;HEIGHT="+height+"</laUrl></mapa>";
			}
		}
		return xml;
	}
	public String getPrintWMSLegend(){
		String xml="";
	
		String[] leys = this.leyendas.split("#");
		String[] wms = this.capas.split("#");
		for (int i=0; i<leys.length; i++){
			if (leys[i].length()>0){
				String[] labels = leys[i].split(",");
				String[] partesWMS=wms[i].split("=");
				String[] capasWMS=partesWMS[partesWMS.length-1].split(",");
				for (int j=0; j<labels.length ;j++){
					if ((j==0) ||(!labels[j].trim().equalsIgnoreCase(labels[j-1].trim()))){
					xml +="<leyenda><label>"+labels[j]+"</label><laUrl>"+wms[i]+"&amp;SERVICE=WMS&amp;REQUEST=GetLegendGraphic&amp;format=image/png&amp;VERSION=1.1.1&amp;STYLES=&amp;layer="+capasWMS[j]+"&amp;width=20&amp;height=20"+"</laUrl></leyenda>";
					}
				}
			}
		}
		return xml;
	}
	protected void generateMap(int unidad_idx) throws Exception{


		String unit_bbox = properties.getProperty(ambito+"_"+unidad_idx+"_BBOX");
		logger.debug(ambito+"_"+unidad_idx+"_BBOX="+unit_bbox);
		String orientacionMapa = getOrientacion(unit_bbox);
		ArrayList<String> legend=legendH;
		if (orientacionMapa.equalsIgnoreCase("VERTICAL")){
			legend = legendV;
		}
		logger.debug("MAPA_"+orientacionMapa+"_"+tamano+"_WIDTH");
		int mapWidth=Integer.parseInt(properties.getProperty("MAPA_"+orientacionMapa+"_"+tamano+"_WIDTH"));
		int mapHeight=Integer.parseInt(properties.getProperty("MAPA_"+orientacionMapa+"_"+tamano+"_HEIGHT"));
		String bbox = adaptBbox(unit_bbox,mapWidth,mapHeight);
		String pathMap ="";
		String[] listabbox = bbox.split(",");
		if ((sld!=null)&&(sld.length()>0) ){ //no es coord/topo
			String resp = sld;
			resp += "<BoundingBox srsName=\"http://www.opengis.net/gml/srs/epsg.xml#25830\">\n";

			resp += "<gml:coord><gml:X>" + listabbox[0] + "</gml:X><gml:Y>" + listabbox[1] + "</gml:Y></gml:coord>\n";
			resp += "<gml:coord><gml:X>" + listabbox[2] + "</gml:X><gml:Y>" + listabbox[3] + "</gml:Y></gml:coord>\n";
			/*         resp += "<gml:coord><gml:X>" + (677191-200) + "</gml:X><gml:Y>" + (4612732-200) + "</gml:Y></gml:coord>\n";
    resp += "<gml:coord><gml:X>" + (677191+200) + "</gml:X><gml:Y>" + (4612732+200) + "</gml:Y></gml:coord>\n";*/
			resp += "</BoundingBox>\n";
			resp += "<Output>\n" +
					"   <Format>image/png</Format>\n" +
					"   <Transparent>true</Transparent>\n" +
					"   <Size><Width>" + mapWidth + "</Width><Height>" + mapHeight + "</Height></Size>\n" +
					"</Output>" +
					"</ogc:GetMap>\n";
			String getMap = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
					"<ogc:GetMap xmlns:ogc=\"http://www.opengis.net/ows\"\n" +
					"            xmlns:gml=\"http://www.opengis.net/gml\"\n" +
					"   version=\"1.1.1\" service=\"WMS\">\n";
			getMap += resp;

			pathMap = Tbl2Map.doSearch("https://idearagon.aragon.es/Visor2D?SERVICE=WMS", getMap);
		}
		else{
			BufferedImage img = new BufferedImage(mapWidth,mapHeight, BufferedImage.TYPE_INT_ARGB);
			Graphics2D g2d = img.createGraphics();
			g2d.setComposite(AlphaComposite.Clear);
			g2d.fillRect(0, 0, new Integer(mapWidth),new Integer(mapHeight));
			g2d.setComposite(AlphaComposite.Src);
			for (int j=0; j < clientData.getLength(); j++){

				// Lo primero es un <it> => Hay que saltarlo
				NodeList theItem = clientData.item(j).getChildNodes();

				String idValue = "";

				String color = "";



				for (int k=0; k < theItem.getLength(); k++){
					Node firstLevelChild = theItem.item(k);

					if (firstLevelChild.getNodeName().equals("_val")) {
						idValue = firstLevelChild.getTextContent();
					}  else if (firstLevelChild.getNodeName().equals("_rgb")) {
						color = firstLevelChild.getTextContent();
					}
				}
				String[] coords = idValue.split(":")[1].split(" ");
				if((new Double(coords[0])>=new Double(listabbox[0]))&&(new Double(coords[0])<=new Double(listabbox[2]))
						&&(new Double(coords[1])>=new Double(listabbox[1]))&&(new Double(coords[1])<=new Double(listabbox[3]))){// entra en este mapa
					g2d.setPaint(Color.decode(color));
					String[] pxs = idValue.split(":")[0].split(" ");
					g2d.fillOval(parseInt(pxs[0])-3,parseInt(pxs[1])-3,6,6);
				}

			}
			g2d.dispose();
			String filename = UUID.randomUUID().toString() + ".png";
			pathMap = Tbl2Map.properties.getProperty("imgs_path")+ filename;
			File output = new File(pathMap);
			ImageIO.write(img, "png", output);
		}
		Calendar fecha = Calendar.getInstance();
		String nombre = upperSpecial2Hex(properties.getProperty(ambito+"_"+unidad_idx+"_NAME"));
		String xml = "<imprimir><fecha>"+fecha.get(Calendar.DAY_OF_MONTH)+"-"+(fecha.get(Calendar.MONTH)+1)+"-"+fecha.get(Calendar.YEAR)+"</fecha><titulo>"+titulo+" ("+nombre+")</titulo><leyendaT2M><leyenda><laUrl>file:///" + legend.get(0).replaceAll(" ", "%20")+"</laUrl></leyenda></leyendaT2M>";
		/*int wms_count = new Integer(properties.getProperty(mapa+"_COUNT"));
		for (int i=1; i<=wms_count; i++){
			String capas = capas_x_wms.get(i-1);
			if ((capas!=null)&&(capas.length()>0)){
				xml +="<mapa><laUrl>"+properties.getProperty("URL_"+mapa+"_"+i).replaceAll("&","&amp;")+"&amp;BBOX="+bbox+"&amp;WIDTH="+mapWidth+"&amp;HEIGHT="+mapHeight+"&amp;LAYERS="+capas+"</laUrl></mapa>";
			}
		}*/
		for (int i=1;i<legend.size(); i++){
			xml+="<leyenda><laUrl>"+legend.get(i).replaceAll(" ", "%20")+"</laUrl></leyenda>";	
		}
		xml+=this.getPrintWMSLegend();
		xml+=this.getPrintWMS(bbox, mapWidth, mapHeight);
		//		xml+="<mapa><laUrl>"+getPrintScalebar(bbox,mapWidth,mapHeight, unidad_idx)+"</laUrl></mapa>";
		//	xml+="<situacionAragon><laUrl>"+getPrintOverview(properties.getProperty(ambito+"_"+unidad_idx+"_BBOX"))+"</laUrl></situacionAragon></imprimir>";
		xml+="<mapa><laUrl>file:///" + pathMap.replaceAll(" ", "%20") + "</laUrl></mapa>";
		xml+="</imprimir>";
		logger.info("XML:"+xml);
		File temp = File.createTempFile("xml4pdf", ".xml");
		PrintWriter writer = new PrintWriter(temp.getAbsolutePath(), "UTF-8");
		writer.println(xml);

		writer.close();
		Tbl2Map.logger.info("Temp file : " + temp.getAbsolutePath());


		String xslt = properties.getProperty("MAPA_PATH")+properties.getProperty("MAPA_"+orientacionMapa+"_"+tamano+"_XSL");


		String filePath = temp_dir+"/MAPA_"+ambito+"_"+(ambito.equalsIgnoreCase("CCAA")?"ARAGON":properties.getProperty(ambito+"_"+unidad_idx+"_CODE"))+".pdf";

		pdfServlet.outputFile = new FileOutputStream(filePath);
		pdfServlet.renderXML(temp.getAbsolutePath(),xslt, null);
		if (formato.equalsIgnoreCase("jpg")){
			convertPDF2JPG(filePath);

		}

	}

	protected void convertPDF2JPG(String pdfPath) throws IOException{
		File pdfFile = new File(pdfPath);
		PDDocument doc = PDDocument.load(pdfFile);
		List<PDPage>pages =  doc.getDocumentCatalog().getAllPages();
		for (int i=0; i<pages.size();i++){
			PDPage page = pages.get(i);
			BufferedImage image =page.convertToImage();
			File outputfile = new File(pdfPath.replaceAll(".pdf", "")+(pages.size()>1 ? "_pag"+(i+1):"")+".jpg");
			ImageIO.write(image, "jpg", outputfile);
		}
		doc.close();
		pdfFile.delete();
	}
	protected String getPrintOverview(String newBboxPrint){
		String[] extent = newBboxPrint.split(",");
		double x_centro =(Double.parseDouble(extent[0])+Double.parseDouble(extent[2]))/2;
		double y_centro =(Double.parseDouble(extent[1])+Double.parseDouble(extent[3]))/2;
		String[] aragon_bbox =properties.getProperty("CCAA_1_BBOX").split(",");
		String xminOverview = aragon_bbox[0];
		String yminOverview = aragon_bbox[1];
		String xmaxOverview = aragon_bbox[2];
		String ymaxOverview = aragon_bbox[3];

		String request ="<ARCXML version=\"1.1\">"+
				"<REQUEST>"+
				"<GET_IMAGE>"+
				"<PROPERTIES>"+
				"<ENVELOPE minx=\"530000\" miny=\"4410000\" maxx=\"855000\" maxy=\"4760000\"/>"+
				"<IMAGESIZE height=\"240\" width=\"223\"/>"+
				"<LAYERLIST>"+
				"<LAYERDEF id=\"CENTROS_SICA\" visible=\"false\" />"+
				"<LAYERDEF id=\"INAEXPPNT\" visible=\"false\" />"+
				"<LAYERDEF id=\"INAEXPLIN\" visible=\"false\" />"+
				"<LAYERDEF id=\"INAEXPPOL\" visible=\"false\" />"+
				"<LAYERDEF id=\"INFPUBL_PNT\" visible=\"false\" />"+
				"<LAYERDEF id=\"INFPUBL_LIN\" visible=\"false\" />"+
				"<LAYERDEF id=\"INFPUBL_POL\" visible=\"false\" />"+
				"<LAYERDEF id=\"BASEINFPUBL_POL\" visible=\"false\" />"+
				"<LAYERDEF id=\"EIP\" visible=\"false\" />"+
				"<LAYERDEF id=\"EIPA\" visible=\"false\" />"+
				"<LAYERDEF id=\"URB_PLU\" visible=\"false\" />"+
				"<LAYERDEF id=\"FPM_Info\" visible=\"false\" />"+
				"<LAYERDEF id=\"OCIC\" visible=\"false\" />"+
				"<LAYERDEF id=\"MA_SET\" visible=\"false\" />"+
				"<LAYERDEF id=\"SA_SET\" visible=\"false\" />"+
				"<LAYERDEF id=\"ExplotacionesGanaderas\" visible=\"false\" />"+
				"<LAYERDEF id=\"Dist_Nucleos\" visible=\"false\" />"+
				"<LAYERDEF id=\"Dist_ExplotacionesGanaderas2\" visible=\"false\" />"+
				"<LAYERDEF id=\"Dist_ExplotacionesGanaderas\" visible=\"false\" />"+
				"<LAYERDEF id=\"GasoOleoductos\" visible=\"false\" />"+
				"<LAYERDEF id=\"OTP\" visible=\"false\" />"+
				"<LAYERDEF id=\"OTM\" visible=\"false\" />"+
				"<LAYERDEF id=\"SIOSE_2009\" visible=\"false\" />"+
				"<LAYERDEF id=\"SIOSE_2006\" visible=\"false\" />"+
				"<LAYERDEF id=\"Ferro_ZP\" visible=\"false\" />"+
				"<LAYERDEF id=\"Ferro_DP\" visible=\"false\" />"+
				"<LAYERDEF id=\"RedCarCodC\" visible=\"false\" />"+
				"<LAYERDEF id=\"RedCarCodB\" visible=\"false\" />"+
				"<LAYERDEF id=\"RedCarCodA\" visible=\"false\" />"+
				"<LAYERDEF id=\"CATEt\" visible=\"false\" />"+
				"<LAYERDEF id=\"CAT\" visible=\"false\" />"+
				"<LAYERDEF id=\"GNPVEt\" visible=\"false\" />"+
				"<LAYERDEF id=\"GNPV\" visible=\"false\" />"+
				"<LAYERDEF id=\"GNPEt\" visible=\"false\" />"+
				"<LAYERDEF id=\"GNP\" visible=\"false\" />"+
				"<LAYERDEF id=\"GRPEt\" visible=\"false\" />"+
				"<LAYERDEF id=\"GRP\" visible=\"false\" />"+
				"<LAYERDEF id=\"TRAEt\" visible=\"false\" />"+
				"<LAYERDEF id=\"TRA\" visible=\"false\" />"+
				"<LAYERDEF id=\"GesResEt\" visible=\"false\" />"+
				"<LAYERDEF id=\"GesRes\" visible=\"false\" />"+
				"<LAYERDEF id=\"Puentes\" visible=\"false\" />"+
				"<LAYERDEF id=\"Troides\" visible=\"false\" />"+
				"<LAYERDEF id=\"Zon_Educa\" visible=\"false\" />"+
				"<LAYERDEF id=\"Cen_Educa\" visible=\"false\" />"+
				"<LAYERDEF id=\"Toponimo\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMinAT\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMinAO\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMinBT\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMinBO\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMinCT\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMinCO\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMinDT\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMinDO\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMinCDT\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMinCDO\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMinRE\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMinDMC\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMin\" visible=\"false\" />"+
				"<LAYERDEF id=\"CuaMinED50\" visible=\"false\" />"+
				"<LAYERDEF id=\"CuaMin\" visible=\"false\" />"+
				"<LAYERDEF id=\"CuaGeo\" visible=\"false\" />"+
				"<LAYERDEF id=\"Cuad50k\" visible=\"false\" />"+
				"<LAYERDEF id=\"CuaUTM10k_t2m\" visible=\"false\" />"+
				"<LAYERDEF id=\"CuaUTM10k\" visible=\"false\" />"+
				"<LAYERDEF id=\"Cuad5k\" visible=\"false\" />"+
				"<LAYERDEF id=\"FechaVueloSer\" visible=\"false\" />"+
				"<LAYERDEF id=\"FechaVueloRec\" visible=\"false\" />"+
				"<LAYERDEF id=\"ToponimoRio\" visible=\"false\" />"+
				"<LAYERDEF id=\"TopCapProv\" visible=\"false\" />"+
				"<LAYERDEF id=\"CapCom\" visible=\"false\" />"+
				"<LAYERDEF id=\"CapProv\" visible=\"false\" />"+
				"<LAYERDEF id=\"ToponimoComarca\" visible=\"false\" />"+
				"<LAYERDEF id=\"ToponimoMunicipio\" visible=\"false\" />"+
				"<LAYERDEF id=\"ToponimoProvincia\" visible=\"false\" />"+
				"<LAYERDEF id=\"Exonimos\" visible=\"false\" />"+
				"<LAYERDEF id=\"ClCatUrb\" visible=\"false\" />"+
				"<LAYERDEF id=\"PlCatUrb\" visible=\"false\" />"+
				"<LAYERDEF id=\"TroidesV\" visible=\"false\" />"+
				"<LAYERDEF id=\"NumPorUrb\" visible=\"false\" />"+
				"<LAYERDEF id=\"KraCer\" visible=\"false\" />"+
				"<LAYERDEF id=\"VelPse\" visible=\"false\" />"+
				"<LAYERDEF id=\"GypBar\" visible=\"false\" />"+
				"<LAYERDEF id=\"FalNau\" visible=\"false\" />"+
				"<LAYERDEF id=\"CypCal\" visible=\"false\" />"+
				"<LAYERDEF id=\"BorCho\" visible=\"false\" />"+
				"<LAYERDEF id=\"MarAur\" visible=\"false\" />"+
				"<LAYERDEF id=\"AusPal\" visible=\"false\" />"+
				"<LAYERDEF id=\"RNP_AC\" visible=\"false\" />"+
				"<LAYERDEF id=\"PORN\" visible=\"false\" />"+
				"<LAYERDEF id=\"RedCarCod\" visible=\"false\" />"+
				"<LAYERDEF id=\"RedCar\" visible=\"false\" />"+
				"<LAYERDEF id=\"Carretera\" visible=\"false\" />"+
				"<LAYERDEF id=\"RedCarSinCapProv\" visible=\"false\" />"+
				"<LAYERDEF id=\"Rio\" visible=\"false\" />"+
				"<LAYERDEF id=\"Hidro300k\" visible=\"false\" />"+
				"<LAYERDEF id=\"Alt5k\" visible=\"false\" />"+
				"<LAYERDEF id=\"ParRus\" visible=\"false\" />"+
				"<LAYERDEF id=\"ZonVulNit\" visible=\"false\" />"+
				"<LAYERDEF id=\"EdRelUrb\" visible=\"false\" />"+
				"<LAYERDEF id=\"PolInd08\" visible=\"false\" />"+
				"<LAYERDEF id=\"DepUrb\" visible=\"false\" />"+
				"<LAYERDEF id=\"ZvUrb\" visible=\"false\" />"+
				"<LAYERDEF id=\"SubParRus\" visible=\"false\" />"+
				"<LAYERDEF id=\"ParUrb\" visible=\"false\" />"+
				"<LAYERDEF id=\"RegVic94\" visible=\"false\" />"+
				"<LAYERDEF id=\"ParSIGPAC\" visible=\"false\" />"+
				"<LAYERDEF id=\"RecSIGPAC\" visible=\"false\" />"+
				"<LAYERDEF id=\"BusSIGPAC\" visible=\"false\" />"+
				"<LAYERDEF id=\"Altimetria\" visible=\"false\" />"+
				"<LAYERDEF id=\"NucPob\" visible=\"false\" />"+
				"<LAYERDEF id=\"ConCarto1k_nuc\" visible=\"false\" />"+
				"<LAYERDEF id=\"Localidad_t2m\" visible=\"false\" />"+
				"<LAYERDEF id=\"Localidad\" visible=\"false\" />"+
				"<LAYERDEF id=\"Rel5k\" visible=\"false\" />"+
				"<LAYERDEF id=\"AltPun5k\" visible=\"false\" />"+
				"<LAYERDEF id=\"CurvaNivel5000\" visible=\"false\" />"+
				"<LAYERDEF id=\"CurvaDirectora5000\" visible=\"false\" />"+
				"<LAYERDEF id=\"Uso5k\" visible=\"false\" />"+
				"<LAYERDEF id=\"ConCarto5k\" visible=\"false\" />"+
				"<LAYERDEF id=\"SinAltimetria5k\" visible=\"false\" />"+
				"<LAYERDEF id=\"SinPlanimetria5k\" visible=\"false\" />"+
				"<LAYERDEF id=\"Plani1000Zar\" visible=\"false\" />"+
				"<LAYERDEF id=\"Plani1000Hue\" visible=\"false\" />"+
				"<LAYERDEF id=\"Plani1000Ter\" visible=\"false\" />"+
				"<LAYERDEF id=\"Alti1000Zar\" visible=\"false\" />"+
				"<LAYERDEF id=\"Alti1000Hue\" visible=\"false\" />"+
				"<LAYERDEF id=\"Alti1000Ter\" visible=\"false\" />"+
				"<LAYERDEF id=\"Planimetria5000\" visible=\"false\" />"+
				"<LAYERDEF id=\"A5000_R\" visible=\"false\" />"+
				"<LAYERDEF id=\"P5000_R\" visible=\"false\" />"+
				"<LAYERDEF id=\"Ferrocarril\" visible=\"false\" />"+
				"<LAYERDEF id=\"LimPas\" visible=\"false\" />"+
				"<LAYERDEF id=\"LimAragon\" visible=\"true\" >"+

		"</LAYERDEF><LAYERDEF id=\"LimCCAA\" visible=\"false\" />"+
		"<LAYERDEF id=\"Municipio_t2m\" visible=\"false\" />"+
		"<LAYERDEF id=\"Municipio\" visible=\"false\" />"+
		"<LAYERDEF id=\"ConConv\" visible=\"false\" />"+
		"<LAYERDEF id=\"RegMon\" visible=\"false\" />"+
		"<LAYERDEF id=\"EncMon\" visible=\"false\" />"+
		"<LAYERDEF id=\"CLC2000_2006\" visible=\"false\" />"+
		"<LAYERDEF id=\"CLC2006_N3\" visible=\"false\" />"+
		"<LAYERDEF id=\"SumPie\" visible=\"false\" />"+
		"<LAYERDEF id=\"SumMat\" visible=\"false\" />"+
		"<LAYERDEF id=\"SumCul\" visible=\"false\" />"+
		"<LAYERDEF id=\"CLC2000_N3\" visible=\"false\" />"+
		"<LAYERDEF id=\"CLC1990_N3\" visible=\"false\" />"+
		"<LAYERDEF id=\"RTC\" visible=\"false\" />"+
		"<LAYERDEF id=\"RTC_E\" visible=\"false\" />"+
		"<LAYERDEF id=\"RTC_ZAP\" visible=\"false\" />"+
		"<LAYERDEF id=\"RTC_RCCS\" visible=\"false\" />"+
		"<LAYERDEF id=\"RTC_TNC\" visible=\"false\" />"+
		"<LAYERDEF id=\"Comarca_t2m\" visible=\"false\" />"+
		"<LAYERDEF id=\"Comarca\" visible=\"false\" />"+
		"<LAYERDEF id=\"Provincia\" visible=\"false\" />"+
		"<LAYERDEF id=\"ConCarto1k_mun\" visible=\"false\" />"+
		"<LAYERDEF id=\"PsAvUrb\" visible=\"false\" />"+
		"<LAYERDEF id=\"ClMayUrb\" visible=\"false\" />"+
		"<LAYERDEF id=\"ComunidadAutonoma\" visible=\"false\" />"+
		"<LAYERDEF id=\"MDT200\" visible=\"false\" />"+
		"<LAYERDEF id=\"MasaUrb\" visible=\"false\" />"+
		"<LAYERDEF id=\"MetodologiaSusceptibilidad\" visible=\"false\" />"+
		"<LAYERDEF id=\"T0212_VIE_I\" visible=\"false\" />"+
		"<LAYERDEF id=\"MM_JUL_2014\" visible=\"false\" />"+
		"<LAYERDEF id=\"T0212_VIE\" visible=\"false\" />"+
		"<LAYERDEF id=\"T0212_IND_DES\" visible=\"false\" />"+
		"<LAYERDEF id=\"T0212_DES\" visible=\"false\" />"+
		"<LAYERDEF id=\"T0212_COL\" visible=\"false\" />"+
		"<LAYERDEF id=\"T0212_DOL\" visible=\"false\" />"+
		"<LAYERDEF id=\"T0212_INUNPR500A_2\" visible=\"false\" />"+
		"<LAYERDEF id=\"T0212_INUNPR500A_1\" visible=\"false\" />"+
		"<LAYERDEF id=\"T0212_ALU\" visible=\"false\" />"+
		"<LAYERDEF id=\"T0212_INU\" visible=\"false\" />"+
		"<LAYERDEF id=\"MP_VU\" visible=\"false\" />"+
		"<LAYERDEF id=\"MP_UF\" visible=\"false\" />"+
		"<LAYERDEF id=\"MP_DP\" visible=\"false\" />"+
		"<LAYERDEF id=\"MP_UP\" visible=\"false\" />"+
		"<LAYERDEF id=\"MP_TP\" visible=\"false\" />"+
		"<LAYERDEF id=\"MP_GDP\" visible=\"false\" />"+
		"<LAYERDEF id=\"BusMun\" visible=\"false\" />"+
		"<LAYERDEF id=\"BusParUrb\" visible=\"false\" />"+
		"<LAYERDEF id=\"BusCallUrb_t2m\" visible=\"false\" />"+
		"<LAYERDEF id=\"BusCallUrb\" visible=\"false\" />"+
		"<LAYERDEF id=\"ED\" visible=\"false\" />"+
		"<LAYERDEF id=\"EV\" visible=\"false\" />"+
		"<LAYERDEF id=\"AraFon\" visible=\"true\" />"+
		"</LAYERLIST>"+
		"</PROPERTIES>"+
		"<LAYER type=\"ACETATE\" id=\"extensionOverview\" name=\"extensionOverview\">"+
		"<OBJECT units=\"database\">"+
		"<POLYGON coords=\""+extent[0]+" "+extent[3]+";"+extent[0]+" "+extent[1]+";"+extent[2]+" "+extent[1]+";"+extent[2]+" "+extent[3]+";\">"+
		"<SIMPLEPOLYGONSYMBOL fillcolor=\"255,0,0\" filltype=\"fdiagonal\" transparency=\"0,5\" boundarycolor=\"255,0,0\"/>"+
		"</POLYGON>"+
		"</OBJECT>"+
		"</LAYER>"+
		"<LAYER type=\"ACETATE\" id=\"extensionOverviewN\" name=\"extensionOverviewN\">"+
		"<OBJECT units=\"database\">"+
		"<POLYGON coords=\""+x_centro+" "+extent[3]+";"+x_centro+" "+ymaxOverview+";"+x_centro+" "+ymaxOverview+";"+x_centro+" "+extent[3]+";\">"+
		"<SIMPLEPOLYGONSYMBOL fillcolor=\"255,0,0\" filltype=\"fdiagonal\" transparency=\"0,5\" boundarycolor=\"255,0,0\"/>"+
		"</POLYGON>"+
		"</OBJECT>"+
		"</LAYER>"+
		"<LAYER type=\"ACETATE\" id=\"extensionOverviewW\" name=\"extensionOverviewW\">"+
		"<OBJECT units=\"database\">"+
		"<POLYGON coords=\""+xminOverview+" "+y_centro+";"+xminOverview+" "+y_centro+";"+extent[0]+" "+y_centro+";"+extent[0]+" "+y_centro+";\">"+
		"<SIMPLEPOLYGONSYMBOL fillcolor=\"255,0,0\" filltype=\"fdiagonal\" transparency=\"0,5\" boundarycolor=\"255,0,0\"/>"+
		"</POLYGON>"+
		"</OBJECT>"+
		"</LAYER>"+
		"<LAYER type=\"ACETATE\" id=\"extensionOverviewE\" name=\"extensionOverviewE\">"+
		"<OBJECT units=\"database\">"+
		"<POLYGON coords=\""+extent[2]+" "+y_centro+";"+extent[2]+" "+y_centro+";"+xmaxOverview+" "+y_centro+";"+xmaxOverview+" "+y_centro+";\">"+
		"<SIMPLEPOLYGONSYMBOL fillcolor=\"255,0,0\" filltype=\"fdiagonal\" transparency=\"0,5\" boundarycolor=\"255,0,0\"/>"+
		"</POLYGON>"+
		"	</OBJECT>"+
		"</LAYER>"+
		"<LAYER type=\"ACETATE\" id=\"extensionOverviewS\" name=\"extensionOverviewS\">"+
		"<OBJECT units=\"database\">"+
		"<POLYGON coords=\""+x_centro+" "+yminOverview+";"+x_centro+" "+extent[1]+";"+x_centro+" "+extent[1]+";"+x_centro+" "+yminOverview+";\">"+
		"<SIMPLEPOLYGONSYMBOL fillcolor=\"255,0,0\" filltype=\"fdiagonal\" transparency=\"0,5\" boundarycolor=\"255,0,0\"/>"+
		"</POLYGON>"+
		"</OBJECT>"+
		"</LAYER>"+
		"</GET_IMAGE>"+
		"</REQUEST>"+
		"</ARCXML>";
		return getArcImsImg(request);
	}
	protected String getPrintScalebar(String newBboxPrint,int printMapWidth,int printMapHeight, int mapa_idx){
		String[] extent = newBboxPrint.split(",");
		int scale_left; 
		if (printMapWidth>printMapHeight){// horizontal
			scale_left = 845;
		}
		else{
			scale_left = 530;

		}
		if (tamano.equalsIgnoreCase("A3")){
			scale_left += 400;
		}
		String request ="<ARCXML version=\"1.1\">"+
				"<REQUEST>"+
				"<GET_IMAGE>"+
				"<PROPERTIES>"+
				"<ENVELOPE minx=\""+extent[0]+"\" miny=\""+extent[1]+"\" maxx=\""+extent[2]+"\" maxy=\""+extent[3]+"\"/>"+
				"<IMAGESIZE height=\""+printMapHeight+"\" width=\""+printMapWidth+"\"/>"+
				"<LAYERLIST>"+
				"<LAYERDEF id=\"CENTROS_SICA\" visible=\"false\" />"+
				"<LAYERDEF id=\"INAEXPPNT\" visible=\"false\" />"+
				"<LAYERDEF id=\"INAEXPLIN\" visible=\"false\" />"+
				"<LAYERDEF id=\"INAEXPPOL\" visible=\"false\" />"+
				"<LAYERDEF id=\"INFPUBL_PNT\" visible=\"false\" />"+
				"<LAYERDEF id=\"INFPUBL_LIN\" visible=\"false\" />"+
				"<LAYERDEF id=\"INFPUBL_POL\" visible=\"false\" />"+
				"<LAYERDEF id=\"BASEINFPUBL_POL\" visible=\"false\" />"+
				"<LAYERDEF id=\"EIP\" visible=\"false\" />"+
				"<LAYERDEF id=\"EIPA\" visible=\"false\" />"+
				"<LAYERDEF id=\"URB_PLU\" visible=\"false\" />"+
				"<LAYERDEF id=\"FPM_Info\" visible=\"false\" />"+
				"<LAYERDEF id=\"OCIC\" visible=\"false\" />"+
				"<LAYERDEF id=\"MA_SET\" visible=\"false\" />"+
				"<LAYERDEF id=\"SA_SET\" visible=\"false\" />"+
				"<LAYERDEF id=\"ExplotacionesGanaderas\" visible=\"false\" />"+
				"<LAYERDEF id=\"Dist_Nucleos\" visible=\"false\" />"+
				"<LAYERDEF id=\"Dist_ExplotacionesGanaderas2\" visible=\"false\" />"+
				"<LAYERDEF id=\"Dist_ExplotacionesGanaderas\" visible=\"false\" />"+
				"<LAYERDEF id=\"GasoOleoductos\" visible=\"false\" />"+
				"<LAYERDEF id=\"OTP\" visible=\"false\" />"+
				"<LAYERDEF id=\"OTM\" visible=\"false\" />"+
				"<LAYERDEF id=\"SIOSE_2009\" visible=\"false\" />"+
				"<LAYERDEF id=\"SIOSE_2006\" visible=\"false\" />"+
				"<LAYERDEF id=\"Ferro_ZP\" visible=\"false\" />"+
				"<LAYERDEF id=\"Ferro_DP\" visible=\"false\" />"+
				"<LAYERDEF id=\"RedCarCodC\" visible=\"false\" />"+
				"<LAYERDEF id=\"RedCarCodB\" visible=\"false\" />"+
				"<LAYERDEF id=\"RedCarCodA\" visible=\"false\" />"+
				"<LAYERDEF id=\"CATEt\" visible=\"false\" />"+
				"<LAYERDEF id=\"CAT\" visible=\"false\" />"+
				"<LAYERDEF id=\"GNPVEt\" visible=\"false\" />"+
				"<LAYERDEF id=\"GNPV\" visible=\"false\" />"+
				"<LAYERDEF id=\"GNPEt\" visible=\"false\" />"+
				"<LAYERDEF id=\"GNP\" visible=\"false\" />"+
				"<LAYERDEF id=\"GRPEt\" visible=\"false\" />"+
				"<LAYERDEF id=\"GRP\" visible=\"false\" />"+
				"<LAYERDEF id=\"TRAEt\" visible=\"false\" />"+
				"<LAYERDEF id=\"TRA\" visible=\"false\" />"+
				"<LAYERDEF id=\"GesResEt\" visible=\"false\" />"+
				"<LAYERDEF id=\"GesRes\" visible=\"false\" />"+
				"<LAYERDEF id=\"Puentes\" visible=\"false\" />"+
				"<LAYERDEF id=\"Troides\" visible=\"false\" />"+
				"<LAYERDEF id=\"Zon_Educa\" visible=\"false\" />"+
				"<LAYERDEF id=\"Cen_Educa\" visible=\"false\" />"+
				"<LAYERDEF id=\"Toponimo\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMinAT\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMinAO\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMinBT\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMinBO\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMinCT\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMinCO\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMinDT\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMinDO\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMinCDT\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMinCDO\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMinRE\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMinDMC\" visible=\"false\" />"+
				"<LAYERDEF id=\"CatMin\" visible=\"false\" />"+
				"<LAYERDEF id=\"CuaMinED50\" visible=\"false\" />"+
				"<LAYERDEF id=\"CuaMin\" visible=\"false\" />"+
				"<LAYERDEF id=\"CuaGeo\" visible=\"false\" />"+
				"<LAYERDEF id=\"Cuad50k\" visible=\"false\" />"+
				"<LAYERDEF id=\"CuaUTM10k_t2m\" visible=\"false\" />"+
				"<LAYERDEF id=\"CuaUTM10k\" visible=\"false\" />"+
				"<LAYERDEF id=\"Cuad5k\" visible=\"false\" />"+
				"<LAYERDEF id=\"FechaVueloSer\" visible=\"false\" />"+
				"<LAYERDEF id=\"FechaVueloRec\" visible=\"false\" />"+
				"<LAYERDEF id=\"ToponimoRio\" visible=\"false\" />"+
				"<LAYERDEF id=\"TopCapProv\" visible=\"false\" />"+
				"<LAYERDEF id=\"CapCom\" visible=\"false\" />"+
				"<LAYERDEF id=\"CapProv\" visible=\"false\" />"+
				"<LAYERDEF id=\"ToponimoComarca\" visible=\"false\" />"+
				"<LAYERDEF id=\"ToponimoMunicipio\" visible=\"false\" />"+
				"<LAYERDEF id=\"ToponimoProvincia\" visible=\"false\" />"+
				"<LAYERDEF id=\"Exonimos\" visible=\"false\" />"+
				"<LAYERDEF id=\"ClCatUrb\" visible=\"false\" />"+
				"<LAYERDEF id=\"PlCatUrb\" visible=\"false\" />"+
				"<LAYERDEF id=\"TroidesV\" visible=\"false\" />"+
				"<LAYERDEF id=\"NumPorUrb\" visible=\"false\" />"+
				"<LAYERDEF id=\"KraCer\" visible=\"false\" />"+
				"<LAYERDEF id=\"VelPse\" visible=\"false\" />"+
				"<LAYERDEF id=\"GypBar\" visible=\"false\" />"+
				"<LAYERDEF id=\"FalNau\" visible=\"false\" />"+
				"<LAYERDEF id=\"CypCal\" visible=\"false\" />"+
				"<LAYERDEF id=\"BorCho\" visible=\"false\" />"+
				"<LAYERDEF id=\"MarAur\" visible=\"false\" />"+
				"<LAYERDEF id=\"AusPal\" visible=\"false\" />"+
				"<LAYERDEF id=\"RNP_AC\" visible=\"false\" />"+
				"<LAYERDEF id=\"PORN\" visible=\"false\" />"+
				"<LAYERDEF id=\"RedCarCod\" visible=\"false\" />"+
				"<LAYERDEF id=\"RedCar\" visible=\"false\" />"+
				"<LAYERDEF id=\"Carretera\" visible=\"false\" />"+
				"<LAYERDEF id=\"RedCarSinCapProv\" visible=\"false\" />"+
				"<LAYERDEF id=\"Rio\" visible=\"false\" />"+
				"<LAYERDEF id=\"Hidro300k\" visible=\"false\" />"+
				"<LAYERDEF id=\"Alt5k\" visible=\"false\" />"+
				"<LAYERDEF id=\"ParRus\" visible=\"false\" />"+
				"<LAYERDEF id=\"ZonVulNit\" visible=\"false\" />"+
				"<LAYERDEF id=\"EdRelUrb\" visible=\"false\" />"+
				"<LAYERDEF id=\"PolInd08\" visible=\"false\" />"+
				"<LAYERDEF id=\"DepUrb\" visible=\"false\" />"+
				"<LAYERDEF id=\"ZvUrb\" visible=\"false\" />"+
				"<LAYERDEF id=\"SubParRus\" visible=\"false\" />"+
				"<LAYERDEF id=\"ParUrb\" visible=\"false\" />"+
				"<LAYERDEF id=\"RegVic94\" visible=\"false\" />"+
				"<LAYERDEF id=\"ParSIGPAC\" visible=\"false\" />"+
				"<LAYERDEF id=\"RecSIGPAC\" visible=\"false\" />"+
				"<LAYERDEF id=\"BusSIGPAC\" visible=\"false\" />"+
				"<LAYERDEF id=\"Altimetria\" visible=\"false\" />"+
				"<LAYERDEF id=\"NucPob\" visible=\"false\" />"+
				"<LAYERDEF id=\"ConCarto1k_nuc\" visible=\"false\" />"+
				"<LAYERDEF id=\"Localidad_t2m\" visible=\"false\" />"+
				"<LAYERDEF id=\"Localidad\" visible=\"false\" />"+
				"<LAYERDEF id=\"Rel5k\" visible=\"false\" />"+
				"<LAYERDEF id=\"AltPun5k\" visible=\"false\" />"+
				"<LAYERDEF id=\"CurvaNivel5000\" visible=\"false\" />"+
				"<LAYERDEF id=\"CurvaDirectora5000\" visible=\"false\" />"+
				"<LAYERDEF id=\"Uso5k\" visible=\"false\" />"+
				"<LAYERDEF id=\"ConCarto5k\" visible=\"false\" />"+
				"<LAYERDEF id=\"SinAltimetria5k\" visible=\"false\" />"+
				"<LAYERDEF id=\"SinPlanimetria5k\" visible=\"false\" />"+
				"<LAYERDEF id=\"Plani1000Zar\" visible=\"false\" />"+
				"<LAYERDEF id=\"Plani1000Hue\" visible=\"false\" />"+
				"<LAYERDEF id=\"Plani1000Ter\" visible=\"false\" />"+
				"<LAYERDEF id=\"Alti1000Zar\" visible=\"false\" />"+
				"<LAYERDEF id=\"Alti1000Hue\" visible=\"false\" />"+
				"<LAYERDEF id=\"Alti1000Ter\" visible=\"false\" />"+
				"<LAYERDEF id=\"Planimetria5000\" visible=\"false\" />"+
				"<LAYERDEF id=\"A5000_R\" visible=\"false\" />"+
				"<LAYERDEF id=\"P5000_R\" visible=\"false\" />"+
				"<LAYERDEF id=\"Ferrocarril\" visible=\"false\" />"+
				"<LAYERDEF id=\"LimPas\" visible=\"false\" />"+
				"<LAYERDEF id=\"LimAragon\" visible=\"false\" >"+

		"</LAYERDEF><LAYERDEF id=\"LimCCAA\" visible=\"false\" />"+
		"<LAYERDEF id=\"Municipio_t2m\" visible=\"false\" />"+
		"<LAYERDEF id=\"Municipio\" visible=\"false\" />"+
		"<LAYERDEF id=\"ConConv\" visible=\"false\" />"+
		"<LAYERDEF id=\"RegMon\" visible=\"false\" />"+
		"<LAYERDEF id=\"EncMon\" visible=\"false\" />"+
		"<LAYERDEF id=\"CLC2000_2006\" visible=\"false\" />"+
		"<LAYERDEF id=\"CLC2006_N3\" visible=\"false\" />"+
		"<LAYERDEF id=\"SumPie\" visible=\"false\" />"+
		"<LAYERDEF id=\"SumMat\" visible=\"false\" />"+
		"<LAYERDEF id=\"SumCul\" visible=\"false\" />"+
		"<LAYERDEF id=\"CLC2000_N3\" visible=\"false\" />"+
		"<LAYERDEF id=\"CLC1990_N3\" visible=\"false\" />"+
		"<LAYERDEF id=\"RTC\" visible=\"false\" />"+
		"<LAYERDEF id=\"RTC_E\" visible=\"false\" />"+
		"<LAYERDEF id=\"RTC_ZAP\" visible=\"false\" />"+
		"<LAYERDEF id=\"RTC_RCCS\" visible=\"false\" />"+
		"<LAYERDEF id=\"RTC_TNC\" visible=\"false\" />"+
		"<LAYERDEF id=\"Comarca_t2m\" visible=\"false\" />"+
		"<LAYERDEF id=\"Comarca\" visible=\"false\" />"+
		"<LAYERDEF id=\"Provincia\" visible=\"false\" />"+
		"<LAYERDEF id=\"ConCarto1k_mun\" visible=\"false\" />"+
		"<LAYERDEF id=\"PsAvUrb\" visible=\"false\" />"+
		"<LAYERDEF id=\"ClMayUrb\" visible=\"false\" />"+
		"<LAYERDEF id=\"ComunidadAutonoma\" visible=\"false\" />"+
		"<LAYERDEF id=\"MDT200\" visible=\"false\" />"+
		"<LAYERDEF id=\"MasaUrb\" visible=\"false\" />"+
		"<LAYERDEF id=\"MetodologiaSusceptibilidad\" visible=\"false\" />"+
		"<LAYERDEF id=\"T0212_VIE_I\" visible=\"false\" />"+
		"<LAYERDEF id=\"MM_JUL_2014\" visible=\"false\" />"+
		"<LAYERDEF id=\"T0212_VIE\" visible=\"false\" />"+
		"<LAYERDEF id=\"T0212_IND_DES\" visible=\"false\" />"+
		"<LAYERDEF id=\"T0212_DES\" visible=\"false\" />"+
		"<LAYERDEF id=\"T0212_COL\" visible=\"false\" />"+
		"<LAYERDEF id=\"T0212_DOL\" visible=\"false\" />"+
		"<LAYERDEF id=\"T0212_INUNPR500A_2\" visible=\"false\" />"+
		"<LAYERDEF id=\"T0212_INUNPR500A_1\" visible=\"false\" />"+
		"<LAYERDEF id=\"T0212_ALU\" visible=\"false\" />"+
		"<LAYERDEF id=\"T0212_INU\" visible=\"false\" />"+
		"<LAYERDEF id=\"MP_VU\" visible=\"false\" />"+
		"<LAYERDEF id=\"MP_UF\" visible=\"false\" />"+
		"<LAYERDEF id=\"MP_DP\" visible=\"false\" />"+
		"<LAYERDEF id=\"MP_UP\" visible=\"false\" />"+
		"<LAYERDEF id=\"MP_TP\" visible=\"false\" />"+
		"<LAYERDEF id=\"MP_GDP\" visible=\"false\" />"+
		"<LAYERDEF id=\"BusMun\" visible=\"false\" />"+
		"<LAYERDEF id=\"BusParUrb\" visible=\"false\" />"+
		"<LAYERDEF id=\"BusCallUrb_t2m\" visible=\"false\" />"+
		"<LAYERDEF id=\"BusCallUrb\" visible=\"false\" />"+
		"<LAYERDEF id=\"ED\" visible=\"false\" />"+
		"<LAYERDEF id=\"EV\" visible=\"false\" />"+
		"<LAYERDEF id=\"AraFon\" visible=\"false\" />"+
		"</LAYERLIST>"+
		"</PROPERTIES>"+
		( ambito.equalsIgnoreCase("CCAA") ?"":"<LAYER type=\"featureclass\" name=\"selection\">"+
				" <DATASET fromlayer=\""+properties.getProperty(ambito+"_LAYER")+"\" />"+
				" <QUERY where=\""+properties.getProperty(ambito+"_ID_ATTR")+"="+properties.getProperty(ambito+"_"+mapa_idx+"_CODE")+"\" />"+
				" <SIMPLERENDERER>"+
				"<SIMPLEPOLYGONSYMBOL filltransparency=\"0\" boundarycolor=\"50,150, 250\" boundarywidth=\"3\" />"+
				" </SIMPLERENDERER>"+
				"</LAYER>")+
		"<LAYER type=\"acetate\" name=\"Escala\">"+
		"<OBJECT units=\"pixel\">"+
		"<SCALEBAR precision=\"0\" texttransparency=\"0.0\" bartransparency=\"1.0\" fontcolor=\"5,5,5\" coords=\""+scale_left+" 10\" barcolor=\"0,122,180\" fontsize=\"11\" screenlength=\"250\" barwidth=\"5\" mapunits=\"meters\" antialiasing=\"true\" scaleunits=\"kilometers\" overlap=\"False\"/>"+
		"</OBJECT>"+
		"<OBJECT units=\"pixel\">"+
		"<SCALEBAR precision=\"0\" texttransparency=\"0.0\" bartransparency=\"1.0\" fontcolor=\"5,5,5\" coords=\""+scale_left+" 10\" barcolor=\"206,206,206\" fontsize=\"11\" screenlength=\"175\" barwidth=\"5\" mapunits=\"meters\" antialiasing=\"true\" scaleunits=\"kilometers\" overlap=\"False\"/>"+
		"</OBJECT>"+
		"<OBJECT units=\"pixel\">"+
		"<SCALEBAR precision=\"0\" texttransparency=\"0.0\" bartransparency=\"1.0\" fontcolor=\"5,5,5\" coords=\""+scale_left+" 10\" barcolor=\"0,122,180\" fontsize=\"11\" screenlength=\"100\" barwidth=\"5\" mapunits=\"meters\" antialiasing=\"true\" scaleunits=\"kilometers\" overlap=\"False\"/>"+
		"</OBJECT>"+
		"<OBJECT units=\"pixel\">"+
		"<SCALEBAR precision=\"0\" texttransparency=\"0.0\" bartransparency=\"1.0\" fontcolor=\"5,5,5\" coords=\""+scale_left+" 10\" barcolor=\"206,206,206\" fontsize=\"11\" screenlength=\"25\" barwidth=\"5\" mapunits=\"meters\" antialiasing=\"true\" scaleunits=\"kilometers\" overlap=\"False\"/>"+
		"</OBJECT>"+
		"<OBJECT units=\"pixel\">"+
		"<SCALEBAR precision=\"0\" texttransparency=\"1.0\" bartransparency=\"0.0\" fontcolor=\"245,243,244\" coords=\""+scale_left+" 20\" barcolor=\"192,90,0\" fontsize=\"11\" fontstyle=\"bold\" screenlength=\"250\" barwidth=\"0\" mapunits=\"meters\" antialiasing=\"true\" scaleunits=\"kilometers\" outline=\"54,54,54\" overlap=\"False\"/>"+
		"</OBJECT>"+
		"<OBJECT units=\"pixel\">"+
		"<SCALEBAR precision=\"0\" texttransparency=\"1.0\" bartransparency=\"0.0\" fontcolor=\"245,243,244\" coords=\""+scale_left+" 20\" barcolor=\"255,255,255\" fontsize=\"11\" fontstyle=\"bold\" screenlength=\"175\" barwidth=\"0\" mapunits=\"meters\" antialiasing=\"true\" scaleunits=\"kilometers\" outline=\"54,54,54\" overlap=\"False\"/>"+
		"</OBJECT>"+
		"<OBJECT units=\"pixel\">"+
		"<SCALEBAR precision=\"0\" texttransparency=\"1.0\" bartransparency=\"0.0\" fontcolor=\"245,243,244\" coords=\""+scale_left+" 20\" barcolor=\"192,90,90\" fontsize=\"11\" fontstyle=\"bold\" screenlength=\"100\" barwidth=\"0\" mapunits=\"meters\" antialiasing=\"true\" scaleunits=\"kilometers\" outline=\"54,54,54\" overlap=\"False\"/>"+
		"</OBJECT>"+
		"<OBJECT units=\"pixel\">"+
		"<SCALEBAR precision=\"0\" texttransparency=\"1.0\" bartransparency=\"0.0\" fontcolor=\"245,243,244\" coords=\""+scale_left+" 20\" barcolor=\"255,255,255\" fontsize=\"12\" fontstyle=\"bold\" screenlength=\"25\" barwidth=\"0\" mapunits=\"meters\" antialiasing=\"true\" scaleunits=\"kilometers\" outline=\"54,54,54\" overlap=\"False\"/>"+
		"</OBJECT>"+
		"</LAYER>"+
		"</GET_IMAGE>"+
		"</REQUEST>"+
		"</ARCXML>";

		return getArcImsImg(request);
	}
	/**
	 * Realiza una petici�n POST a un servicio Web
	 * @param service_url URL del servicio
	 * @param request petici�n
	 * @return respuesta del servicio
	 */
	protected  String getArcImsImg(String request) {
		try{
			URL url = new URL(properties.getProperty("ARCIMS_URL"));
			HttpURLConnection connection = (HttpURLConnection)url.openConnection();
			connection.setRequestMethod("POST");
			connection.setDoOutput(true);

			//	logger.info(request);

			connection.setRequestProperty("Content-Type", "text/xml"); 
			connection.setRequestProperty("charset", "utf-8");
			connection.setRequestProperty("Content-Length", "" + Integer.toString(request.getBytes().length));
			connection.connect();
			OutputStreamWriter wr = new OutputStreamWriter(connection.getOutputStream());
			wr.write(request);
			wr.flush();

			//read the result from the server
			BufferedReader rd  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			StringBuilder sb = new StringBuilder();
			String line;
			while ((line = rd.readLine()) != null)
			{
				sb.append(line + '\n');
			}

			//System.out.println(sb.toString());
			connection.disconnect();
			String resp = sb.toString();
			try{
				DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
				Document doc = dBuilder.parse(new ByteArrayInputStream(resp.getBytes()));
				doc.getDocumentElement().normalize();
				resp = ((Element)doc.getElementsByTagName("OUTPUT").item(0)).getAttribute("url");
			}
			catch (Exception ex){
				logger.error("Error al procesar el resultado de ArcIMS "+resp, ex);
				return "";
			}
			return resp;
		}
		catch(MalformedURLException ex){
			logger.error("Error en la consulta "+request+ " al servicio ArcIMS", ex);
		}
		catch(IOException ex){
			logger.error("Error en la consulta "+request+ " al servicio ArcIMS", ex);
		}
		return "";
	}

	public static Integer getJobProgress(String jobid){
		return progresos.get(jobid);
	}

	public static String initJob(){
		long jobid = System.currentTimeMillis();
		logger.info("TEMP_DIR="+properties.getProperty("TEMP_DIR"));
		logger.info("TEMP_DIR="+properties.getProperty("TEMP_DIR","/app/jboss/tab2mapWS/maps/temp"));
		File temp_dir = new File(properties.getProperty("TEMP_DIR","/app/jboss/tab2mapWS/maps/temp")+"/"+jobid);
		if(!temp_dir.mkdir()){
			logger.error("No se pudo crear el directorio temporal para generar los mapas "+temp_dir.getAbsolutePath());
			return null;
		}
		return new Long(jobid).toString();
	}

	public static void cancelJob(String jobid){

		progresos.put(jobid,CANCELLED);

		logger.debug(" CANCEL "+progresos.get(jobid));
	}

	public static String adaptBbox( String bbox, double mwidth, double mheight) {
		//var newBBOX = new boundingBox(_minx, _miny, _maxx, _maxy);
		String[] bounds = bbox.split(",");

		double _minx = Double.parseDouble(bounds[0]);
		double _miny = Double.parseDouble(bounds[1]);
		double _maxx = Double.parseDouble(bounds[2]);
		double _maxy = Double.parseDouble(bounds[3]);
		double ratioTamano = mwidth / mheight;
		double diffX = _maxx - _minx;
		double diffY = _maxy - _miny;
		double ratioCoord = diffX / diffY;
		//alert("ratioT" + ratioTamano + "," + ratioCoord)
		if (ratioCoord > ratioTamano) {
			// estirar la Y
			//	alert("estirar la Y")

			double newDiffY = diffX / ratioTamano;

			double gapY = newDiffY - diffY;

			if (gapY > 0) {
				_miny = _miny - (gapY/2);
				_maxy = _maxy + (gapY/2);
			} else {
				_maxy = _miny - (gapY/2);
				_miny = _maxy + (gapY/2);
			}
		} else if (ratioCoord < ratioTamano){
			// estirar la X
			//alert("estirar la X")

			double newDiffX = ratioTamano * diffY;

			double gapX = newDiffX - diffX;
			if (gapX > 0) {
				_minx = _minx - (gapX/2);
				_maxx = _maxx + (gapX/2);
			} else {
				_maxx = _minx - (gapX/2);
				_minx = _maxx + (gapX/2);
			}
		}
		return _minx+","+ _miny+","+ _maxx+","+_maxy;
	}


	public static void main(String[] args) {
		initProperties("C:\\Archivos de programa\\Apache Software Foundation\\Tomcat 6.0\\webapps\\batchMapWS\\WEB-INF\\config\\batchMapWS.properties");
		try{

			//new BatchMap(BatchMap.initJob(),"MIXTO","1_MODIS,1_spot,2_0,2_13","PROV","pdf","vertical","A4").start();
		}
		catch(Exception e){
			e.printStackTrace();
		}
		System.out.println("Fin");
	}
}
