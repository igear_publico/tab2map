package org.apache.fop.servlet;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;

import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;

public class MapaPDFServlet extends FopServlet {
	public FileOutputStream outputFile; 
	public void init(ServletContext context) throws ServletException {
		/*   this..log = new SimpleLog("FOP/Servlet");
        log.setLevel(SimpleLog.LOG_LEVEL_WARN);*/
		this.uriResolver = new ServletContextURIResolver(
				context);
		this.transFactory = TransformerFactory.newInstance();
		this.transFactory.setURIResolver(this.uriResolver);
		//Configure FopFactory as desired
		this.fopFactory = FopFactory.newInstance();
		this.fopFactory.setURIResolver(this.uriResolver);
		configureFopFactory();
	}
	
	public void renderXML(String xml, String xslt, HttpServletResponse response)
	throws FOPException, TransformerException, IOException {

		//Setup sources
		Source xmlSrc = convertString2Source(xml);
		Source xsltSrc = convertString2Source(xslt);

		//Setup the XSL transformation
		Transformer transformer = this.transFactory.newTransformer(xsltSrc);
		transformer.setURIResolver(this.uriResolver);

		//Start transformation and rendering process
		render(xmlSrc, transformer, response);
	}

	protected void render(Source src, Transformer transformer, HttpServletResponse response)
	throws FOPException, TransformerException, IOException {

		FOUserAgent foUserAgent = getFOUserAgent();

		//Setup output
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		//Setup FOP
		Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);

		//Make sure the XSL transformation's result is piped through to FOP
		Result res = new SAXResult(fop.getDefaultHandler());

		//Start the transformation and rendering process
		transformer.transform(src, res);

		//Return the result
		sendPDF(out.toByteArray(), response);
	}

	private void sendPDF(byte[] content, HttpServletResponse response) throws IOException {
		if (response!=null){
			//Send the result back to the client
			response.setContentType("application/pdf");
			response.setContentLength(content.length);
			response.getOutputStream().write(content);
			response.getOutputStream().flush();
		}else{
			outputFile.write(content);
			outputFile.flush();
			outputFile.close();
		}
	}
}
