### Dependencias

Para el funcionamiento de la aplicación TablaAMapa es necesario disponer de:
- un WMS/WFS con soporte al parámetro CQL_FILTER y el formato json (e.g.: geoserver)
- el conjunto de librerías javascript externas y código javascript compartido de IDEARAGON denominado lib (https://gitlab.com/igear_publico/lib)


### Instalación

TablaAMapa se compone de:  
* el cliente Web tab2map: para instalarlo hay que copiar el contenido de src/webClient en la carpeta de publicación del servidor http.  
* el servicio Web tab2mapWS: para instalarlo hay que generar el war con los fuentes proporcionados en src/tbl2mapWS y desplegarlo en el servidor de aplicaciones.

